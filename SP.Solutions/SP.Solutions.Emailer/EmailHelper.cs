﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SP.Solutions.Emailer
{
    public class EmailHelper
    {
        public static void SendEmail(EmailContentEntity emailContent)
        {
            string fromUserEmail = System.Configuration.ConfigurationManager.AppSettings["EmailUser"].ToString();
            string fromUserPassword = System.Configuration.ConfigurationManager.AppSettings["EmailPassword"].ToString();

            try
            {
                MailMessage msg = new MailMessage()
                {
                    Subject = "AP Workflow Daily Notification: Pending Approvals",
                    Body = emailContent.Body,
                    IsBodyHtml = true,
                    From = new MailAddress(fromUserEmail),
                };

                foreach (var email in emailContent.To)
                    msg.To.Add(email);

                SmtpClient SmtpClient = new SmtpClient()
                {
                    Credentials = new System.Net.NetworkCredential(fromUserEmail, fromUserPassword),
                    Host = "smtp.office365.com",
                    Port = 587,
                    EnableSsl = true
                };
                SmtpClient.Send(msg);
            }
            catch (Exception ex)
            {

            }
        }

        public static void SendEmails(Dictionary<string, EmailContentEntity> dictEmails)
        {
            string fromUserEmail = System.Configuration.ConfigurationManager.AppSettings["EmailUser"].ToString();
            string fromUserPassword = System.Configuration.ConfigurationManager.AppSettings["EmailPassword"].ToString();

            foreach (var entEmailEntry in dictEmails)
            {
                try
                {
                    MailMessage msg = new MailMessage()
                    {
                        Subject = "AP Workflow Daily Notification: Pending Approvals",
                        //Body = entEmailEntry.Value.GetEmailBody(MemberTypes.Approvers),
                        IsBodyHtml = true,
                        From = new MailAddress(fromUserEmail),
                    };

                    //foreach (var email in entEmailEntry.Value.ToEmails)
                    //    msg.To.Add(email);

                    SmtpClient SmtpClient = new SmtpClient()
                    {
                        Credentials = new System.Net.NetworkCredential(fromUserEmail, fromUserPassword),
                        Host = "smtp.office365.com",
                        Port = 587,
                        EnableSsl = true
                    };
                    SmtpClient.Send(msg);
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
