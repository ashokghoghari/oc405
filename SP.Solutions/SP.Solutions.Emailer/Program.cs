﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SP.Solutions.Emailer
{
    class Program
    {
        static void Main(string[] args)
        {
            List<RFIEntity> listRFIs = GetAllPendingRFI();

            SendEmailForPendingForDC(listRFIs.Where(x => x.RequestStatus.Equals("Pending for DC 24")).ToList());
            SendEmailForPendingFor24(listRFIs.Where(x => x.RequestStatus.Equals("Pending for 24") && (x.CreatedOn.Subtract(DateTime.Today)).Days > 3).ToList());

            //SendEmailForPendingForDC(listRFIs.Where(x => x.RequestStatus.Equals("Pending for DC 24")).ToList());5040-Design Management
            //SendEmailForPendingFor24(listRFIs.Where(x => x.RequestStatus.Equals("Pending for 24") && (x.CreatedOn.Subtract(DateTime.Today)).Days > 3).ToList());
        }

        private static void SendEmailForPendingFor24(List<RFIEntity> listPendingFor24)
        {
            if (listPendingFor24.Count > 0)
            {
                List<EmailContentEntity> listEmailEntites = new List<EmailContentEntity>();

                Dictionary<int, List<RFIEntity>> listUserData = new Dictionary<int, List<RFIEntity>>();
                foreach (RFIEntity entRFI in listPendingFor24)
                {
                    foreach (var assignedTo in entRFI.AssignedTo)
                    {
                        List<RFIEntity> listCurrentData = null;
                        if (listUserData.ContainsKey(assignedTo.UserID))
                        {
                            listCurrentData = listUserData[assignedTo.UserID];
                            listUserData.Remove(assignedTo.UserID);
                        }
                        else
                            listCurrentData = new List<RFIEntity>();


                        if (listCurrentData.Count(x => x.ID == entRFI.ID) <= 0)
                            listCurrentData.Add(entRFI);

                        listUserData.Add(assignedTo.UserID, listCurrentData);
                    }
                }

                foreach (var userDataItem in listUserData)
                {
                    EmailContentEntity ent = new EmailContentEntity();
                    int userID = userDataItem.Key;
                    List<RFIEntity> listRFIPendingFor24 = userDataItem.Value;
                    //To Do : Prepare Email
                }

                //EmailHelper.SendEmails(listEmailEntites);
            }
        }

        private static void SendEmailForPendingForDC(List<RFIEntity> listRFIPendingForDC24)
        {
            if (listRFIPendingForDC24.Count > 0)
            {
                EmailContentEntity ent = new EmailContentEntity();
                SPGroup group = SSOMServiceSSOMService.SSOMService.GetGroup("5040-Design Staff");
                foreach (SPUser user in group.Users)
                    ent.To.Add(new MailAddress(user.Email));
                ent.Body = GetTable(listRFIPendingForDC24);
            }
        }

        private static string GetTable(List<RFIEntity> listRFIPendingForDC24)
        {
            StringBuilder sbTableHtml = new StringBuilder();
            sbTableHtml.Append("<table><thead>" + RFIEntity.Headers + "</thead><tbody>");
            foreach (RFIEntity entRFI in listRFIPendingForDC24)
            {
                string tableRow = entRFI.ToString();
                sbTableHtml.Append(tableRow);
            }
            sbTableHtml.Append("</tbody></table>");
            return sbTableHtml.ToString();
        }

        private static List<RFIEntity> GetAllPendingRFI()
        {
            string caml = string.Empty;
            //string caml = "<View>"
            //                + "<Query>"
            //                    + "<Where>"
            //                        + "<And>"
            //                            + "<Neq><FieldRef Name = 'IsDeleted'/><Value Type = 'Boolean'>1</Value></Neq>"
            //                            + "<Or><Eq><FieldRef Name = 'ApprovalStatus'/><Value Type = 'Choice'>Pending</Value></Eq><Eq><FieldRef Name = 'ApprovalStatus'/><Value Type = 'Choice'>Partially Approved</Value></Eq></Or>"
            //                        + "</And>"
            //                    + "</Where>"
            //                + "</Query>"
            //              + "</View>";
            SPListItemCollection items = SSOMServiceSSOMService.SSOMService.GetListData("RFI", caml);
            List<RFIEntity> listRFIs = new List<RFIEntity>();
            foreach (SPListItem item in items)
                listRFIs.Add(new RFIEntity(item));
            return listRFIs;
        }
    }
}
