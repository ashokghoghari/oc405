﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SP.Solutions.Emailer
{
   public class EmailContentEntity
    {
        public List<MailAddress> To { get; internal set; }
        public string Body { get; internal set; }
    }
}