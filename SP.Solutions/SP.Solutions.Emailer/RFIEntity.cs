﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace SP.Solutions.Emailer
{
    public class UserEntity
    {
        public UserEntity(int id, string email, string name)
        {
            Email = email;
            Name = name;
            UserID = id;
        }

        public string Email { get; private set; }
        public string Name { get; private set; }
        public int UserID { get; private set; }
    }
    public class RFIEntity
    {
        public RFIEntity(SPListItem item)
        {
            ID = item.ID;
            CreatedOn = (DateTime)item["Created"];
            ModifiedOn = (DateTime)item["Modified"];
            RequestStatus = item["RequestStatus"].ToString();
            SPFieldUserValueCollection assignedToIDList = item["AssignedTo"] as SPFieldUserValueCollection;
            if (assignedToIDList != null && assignedToIDList.Count > 0)
            {
                AssignedTo = new List<UserEntity>();
                foreach (SPFieldUserValue user in assignedToIDList)
                    AssignedTo.Add(new UserEntity(user.User.ID, user.User.Email, user.User.Name));
            }
        }

        public int ID { get; private set; }
        public DateTime CreatedOn { get; private set; }
        public DateTime ModifiedOn { get; private set; }
        public string RequestStatus { get; private set; }
        public List<UserEntity> AssignedTo { get; private set; }

        public override string ToString()
        {
            return base.ToString();
        }

        public static string Headers
        {
            get
            {
                string rowHtml = "<tr>"
                    + GetTH("ID")
                    + GetTH("Request Status")
                    + "</tr>";
                return rowHtml;
            }
        }

        private string GetTD(string val, bool isCenter = false)
        {
            return "<td style='text-align: " + (isCenter ? "center" : "left") + "'>" + (string.IsNullOrEmpty(val) || val.Equals("01/01/0001") || val.Equals("$") ? string.Empty : val) + "</td>";
        }

        private static string GetTH(string val)
        {
            return "<th>" + val + "</th>";
        }
    }
}
