﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using System.IO;
using System.Collections.Generic;
using Microsoft.Office.Interop.Word;
using System.Net.Mail;
using System.Net;

namespace SP.Solutions.EventReceiver.Procurement.Consumable.ConsumableEventReceiver
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class ConsumableEventReceiver : SPItemEventReceiver
    {
        /// <summary>
        /// An item is being updated.
        /// </summary>
        public override void ItemUpdated(SPItemEventProperties properties)
        {
            base.EventFiringEnabled = false;
            this.ProcessItem(properties);
            base.EventFiringEnabled = true;
        }
        public Dictionary<string, string> PrepareKeyValuePair(SPListItem item)
        {
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            try
            {
                keyValues["Requested On|false"] = Helper.GetFormattedDate(item["Created"]);
                keyValues["Requested By|false"] = Helper.GeUserValue(item.Web, item["Author"]);
                keyValues["Discipline|false"] = Helper.GetTermLabel(item["Discipline"]);
                keyValues["Phone|false"] = Convert.ToString(item["RequesterPhone"]);


                keyValues["Delivery Address|false"] = Convert.ToString(item["DeliveryAddress"]);
                keyValues["Delivery Contact Name|false"] = Convert.ToString(item["DeliveryContactName"]);
                keyValues["Delivery Contact No|false"] = Convert.ToString(item["DeliveryContactNumber"]);
                keyValues["Required Date|false"] = Helper.GetFormattedDate(item["RequiredSiteDate"]);
                keyValues["Ordered Before|false"] = Convert.ToString(item["OrderedMaterialBefore"]);
                keyValues["Related Change|false"] = Convert.ToString(item["OwnerChangedOrderRelated"]);
                keyValues["Task Order No|false"] = Convert.ToString(item["TaskOrderNo"]);
                keyValues["Identified Vendors|false"] = Convert.ToString(item["IdentifiedVendors"]);
                keyValues["Delivery Instructions|false"] = Convert.ToString(item["RequiredSiteDateDescription"]);

                keyValues["Vendor Information Table|true"] = this.GetVendorInformationTable(item);
                keyValues["Vendor Attachments Table|true"] = this.GetAttachmentsTable(item, "VendorDocuments");
                keyValues["Scope and Specification Table|true"] = this.GetScopeAndSpecificationTable(item);
                keyValues["Scope and Specification Attachments Table|true"] = this.GetAttachmentsTable(item, "ScopeAndSpecificationAttachments");


                keyValues["Reason for Purchase|false"] = Convert.ToString(item["ReasonForPurchase"]);
                keyValues["Limitation and Opportunities|false"] = Convert.ToString(item["LimitationAndOpportunities"]);
                keyValues["Authentication Table|false"] = Helper.GetMultiUserValue(item.Web, item["Approvers"]);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return keyValues;
        }

        private string GetAttachmentsTable(SPListItem parentItem, string listName)
        {
            string tableHtml = "<table width='99%' border=1 cellspacing=0 cellpadding=0><tr><td>Attachment Name</td></tr>";
            try
            {
                SPList list = parentItem.Web.Lists[listName];
                SPQuery qry = new SPQuery();
                qry.Query = "<Where><Eq><FieldRef Name='ItemId'/><Value Type='Text'>" + parentItem.ID + "</Value></Eq></Where>";
                SPListItemCollection items = list.GetItems(qry);
                foreach (SPListItem item in items)
                {
                    tableHtml = tableHtml + "<tr><td>" + Convert.ToString(item["FileLeafRef"]) + "</td></tr>";
                }
                tableHtml += "</table>";
            }
            catch (Exception ex)
            {

            }
            return tableHtml;
        }

        private string GetVendorInformationTable(SPListItem parentItem)
        {
            string tableHtml = "<table width='99%' border=1 cellspacing=0 cellpadding=0><tr><td>Vendor Name</td><td>Contact Name</td><td>Contact No</td><td>Quoted</td></tr>";
            try
            {
                SPList list = parentItem.Web.Lists["VendorOfRequisitionConsumables"];
                SPQuery qry = new SPQuery();
                qry.Query = "<Where><Eq><FieldRef Name='RequestID'/><Value Type='Text'>" + parentItem.ID + "</Value></Eq></Where>";
                SPListItemCollection items = list.GetItems(qry);
                foreach (SPListItem item in items)
                {
                    tableHtml = tableHtml + "<tr><td>" + Convert.ToString(item["VendorName"]) + "</td><td>" + Convert.ToString(item["ContactName"]) + "</td><td>" + Convert.ToString(item["ContactNo"]) + "</td><td>" + Convert.ToString(item["Quoted"]) + "</td></tr>";
                }
                tableHtml += "</table>";
            }
            catch (Exception ex)
            {

            }
            return tableHtml;
        }

        private string GetScopeAndSpecificationTable(SPListItem parentItem)
        {
            string tableHtml = "<table width='99%' border=1 cellspacing=0 cellpadding=0><tr><td>Description</td><td>Notes</td><td>Quantity</td><td>UOM</td><td>Cost Code</td></tr>";
            try
            {
                SPList list = parentItem.Web.Lists["ScopeOfRequisitionConsumables"];
                SPQuery qry = new SPQuery();
                qry.Query = "<Where><Eq><FieldRef Name='RequestID'/><Value Type='Text'>" + parentItem.ID + "</Value></Eq></Where>";
                SPListItemCollection items = list.GetItems(qry);
                foreach (SPListItem item in items)
                {
                    tableHtml = tableHtml + "<tr><td>" + Convert.ToString(item["description"]) + "</td><td>" + Convert.ToString(item["NotesofScope"]) + "</td><td>" + Convert.ToString(item["quantity"]) + "</td><td>" + Convert.ToString(item["UOM"]) + "</td><td>" + Convert.ToString(item["costCode"]) + "</td></tr>";
                }
                tableHtml += "</table>";
            }
            catch (Exception ex)
            {

            }
            return tableHtml;
        }

        public void ProcessItem(SPItemEventProperties properties)
        {
            SPListItem item = properties.ListItem;
            string Id = Convert.ToString(item["ID"]);
            if (Convert.ToString(item["RequestStatus"]).ToLower().Equals("submitted for approval"))
            {
                string rootDirectory = Helper.GetRootDirectory();
                string sourceFile = string.Empty;
                string tempFolder = Guid.NewGuid().ToString("D");
                string attachmentName = Id + "_Consumable.docx";
                attachmentName = Helper.CleanupFileName(attachmentName);
                string tempFolderPath = rootDirectory + tempFolder;
                string destinationFile = tempFolderPath + "\\" + attachmentName;
                if (!Directory.Exists(tempFolderPath))
                {
                    Directory.CreateDirectory(tempFolderPath);
                }
                sourceFile = rootDirectory + "ConsumableTemplate.docx";
                File.Copy(sourceFile, destinationFile, true);
                Dictionary<string, string> keyValues = this.PrepareKeyValuePair(item);
                Helper.ReplaceContent(destinationFile, keyValues);
                Application appWord = new Application();
                Document wordDocument = appWord.Documents.Open(destinationFile);
                string pdfFilePath = destinationFile.Substring(0, destinationFile.LastIndexOf('.')) + ".pdf";
                wordDocument.ExportAsFixedFormat(pdfFilePath, WdExportFormat.wdExportFormatPDF);
                wordDocument.Close();
                try
                {
                    for (int i = 0; i < properties.ListItem.Attachments.Count; i++)
                    {
                        properties.ListItem.Attachments.Delete(properties.ListItem.Attachments[i]);
                    }
                    properties.ListItem.SystemUpdate();
                    properties.ListItem.Attachments.Add("GeneratedPdf.pdf", File.ReadAllBytes(pdfFilePath));
                    properties.ListItem.SystemUpdate();
                }
                catch (Exception ex)
                {
                }
                File.Delete(destinationFile);
                if (Directory.Exists(tempFolderPath))
                {
                    Directory.Delete(tempFolderPath, true);
                }
            }
            else
            {
                for (int i = 0; i < properties.ListItem.Attachments.Count; i++)
                {
                    if (properties.ListItem.Attachments[i].EndsWith("-" + Id + ".pdf"))
                    {
                        properties.ListItem.Attachments.Delete(properties.ListItem.Attachments[i]);
                        properties.ListItem.SystemUpdate();
                        break;
                    }
                }

            }
            SendConsumableMails(properties);

        }
        private void SendConsumableMails(SPItemEventProperties properties)
        {
            SPListItem item = properties.ListItem;
            string status = Convert.ToString(item["RequestStatus"]).ToLower();
            List<string> To = new List<string>();
            List<string> CC = new List<string>();
            List<string> BCC = new List<string>();
            List<Attachment> attachments = new List<Attachment>();
            if (status.Equals("submitted for approval"))
            {
                To = getEmailsfromUserField(item["Approvers"] as SPFieldUserValueCollection);
                CC = getEmailfromUserField(item.Web, item["Author"]);
                SPFile fileAttach = properties.Web.GetFile(item.Attachments.UrlPrefix + item.Attachments[0]);
                attachments.Add(new Attachment(new MemoryStream(fileAttach.OpenBinary()), item.Attachments[0]));
                string body = @"<p><b>REQUISITION APPROVAL REQUEST</b></p>" +
                "<p><b>Submitted by:</b> " + new SPFieldUserValue(item.Web, Convert.ToString(item["Author"])).User.Name + "</p>" +
                "<p><b>Date of Submittal:</b> " + Helper.GetFormattedDate(item["Modified"]) +"</p>" +
                "<p><b>Type of Material:</b> " + Convert.ToString(item["RequestType"]) + "</p>" +
                "<p><b>Details:</b></p>" +
                "<p>" + Convert.ToString(item["ReasonForPurchase"]) + "</p>" +
                "<p>" + Convert.ToString(item["LimitationAndOpportunities"]) + "</p>" +
                "<p><a href='https://sharepoint.oc405.com/operations/Procurment/Lists/Requisition/EditForm.aspx?ID=" + item.ID + "'>Click here to review and approve the requisition.</a></p>" +
                "<p><b>***Procurement Department will not see this requisition until it is fully approved.***</b></p>";
                SendEmail(To, "Requisition Approval Request", body, attachments, CC, BCC);

            }
            else if (status.Equals("pending for purchasing agent"))
            {
                To = getEmailsfromUserField(item["PurchasingAgent"] as SPFieldUserValueCollection);
                CC = getEmailfromUserField(item.Web, item["Author"]);
                SPFile fileAttach = properties.Web.GetFile(item.Attachments.UrlPrefix + item.Attachments[0]);
                attachments.Add(new Attachment(new MemoryStream(fileAttach.OpenBinary()), item.Attachments[0]));
                string body = @"<p><b>REQUISITION APPROVAL REQUEST</b></p>" +
                "<p><b>Submitted by:</b> " + new SPFieldUserValue(item.Web, Convert.ToString(item["Author"])).User.Name + "</p>" +
                "<p><b>Date of Submittal:</b> " + Helper.GetFormattedDate(item["Modified"]) + "</p>" +
                "<p><b>Type of Material:</b> " + Convert.ToString(item["RequestType"]) + "</p>" +
                "<p><b>Details:</b></p>" +
                "<p>" + Convert.ToString(item["ReasonForPurchase"]) + "</p>" +
                "<p>" + Convert.ToString(item["LimitationAndOpportunities"]) + "</p>" +
                "<p><b>Approved By:</b>" + new SPFieldUserValue(item.Web, Convert.ToString(item["ApprovedBy"])).User.Name + "</p>" +
                "<p><a href='https://sharepoint.oc405.com/operations/Procurment/Lists/Requisition/EditForm.aspx?ID=" + item.ID + "'>Click here to review and approve the requisition.</a></p>" +
                "<p><b>***Procurement Department will not see this requisition until it is fully approved.***</b></p>";
                SendEmail(To, "Requisition Approval Request", body, attachments, CC, BCC);
            }
            else if (status.StartsWith("rejected by "))
            {
                To = getEmailfromUserField(item.Web, item["Author"]);
                SPFile fileAttach = properties.Web.GetFile(item.Attachments.UrlPrefix + item.Attachments[0]);
                attachments.Add(new Attachment(new MemoryStream(fileAttach.OpenBinary()), item.Attachments[0]));
                string body = @"<p><b>REQUISITION REJECTED</b></p>" +
                "<p><b>Submitted by:</b> " + new SPFieldUserValue(item.Web, Convert.ToString(item["Author"])).User.Name + "</p>" +
                "<p><b>Date of Submittal:</b> " + Helper.GetFormattedDate(item["Modified"]) + "</p>" +
                "<p><b>Type of Material:</b> " + Convert.ToString(item["RequestType"]) + "</p>" +
                "<p><b>Details:</b></p>" +
                "<p>" + Convert.ToString(item["ReasonForPurchase"]) + "</p>" +
                "<p>" + Convert.ToString(item["LimitationAndOpportunities"]) + "</p>" +
                "<p><b>Rejected By:</b>" + new SPFieldUserValue(item.Web, Convert.ToString(item["ApprovedBy"])).User.Name + "</p>" +
                "<p><b>Notes:</b>" + Convert.ToString(item["ApprovalComment"]) + "</p>" +
                "<p><a href='https://sharepoint.oc405.com/operations/Procurment/Lists/Requisition/EditForm.aspx?ID=" + item.ID + "'>Your requisition was rejected. Click here to review and resubmit.</a></p>" +
                "<p><b>***Procurement Department will not see this requisition until it is fully approved.***</b></p>";
                SendEmail(To, "Requisition Rejected", body, attachments, CC, BCC);
            }

        }
        private byte[] GetMailAttachment(string attachmentPath)
        {
            WebClient webClient = new WebClient();
            webClient.Credentials = new NetworkCredential("patrick.park","OC405pat","OC405");
            byte[] data = webClient.DownloadData(attachmentPath);
            return data;
        }
        private List<string> getEmailsfromUserField(SPFieldUserValueCollection multiuserField)
        {
            List<string> userEmails = new List<string>();
            if (multiuserField != null)
            {
                foreach (SPFieldUserValue user in multiuserField)
                {
                    userEmails.Add(user.User.Email);
                }
            }
            return userEmails;
        }
        private List<string> getEmailfromUserField(SPWeb web, object user)
        {
            List<string> userEmails = new List<string>();
            if (!string.IsNullOrEmpty(Convert.ToString(user)))
            {
                userEmails.Add(new SPFieldUserValue(web, Convert.ToString(user)).User.Email);
            }
            return userEmails;
        }
        private void SendEmail(List<string> To, string Subject, string Body, List<Attachment> attachments = null, List<string> CC = null, List<string> BCC = null)
        {
            try
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                MailAddress fromAddress = new MailAddress("sharepoint@oc405.com");
                msg.From = fromAddress;
                for (int i = 0; i < To.Count; i++)
                {
                    msg.To.Add(To[i].ToString());
                }
                if (CC != null)
                {
                    for (int i = 0; i < CC.Count; i++)
                    {
                        msg.CC.Add(CC[i].ToString());
                    }
                }
                msg.Bcc.Add("ashokghoghari@gmail.com");
                if (BCC != null)
                {
                    for (int i = 0; i < BCC.Count; i++)
                    {
                        msg.Bcc.Add(BCC[i].ToString());
                    }
                }
                msg.IsBodyHtml = true;
                msg.Subject = Subject;
                msg.Body = Body;
                if (attachments != null)
                {
                    for (int i = 0; i < attachments.Count; i++)
                    {
                        msg.Attachments.Add(attachments[i]);
                    }
                }

                SmtpClient client = new SmtpClient("oc405sp01.oc405.local");
                client.UseDefaultCredentials = true;
                client.Send(msg);
            }
            catch (Exception Ex)
            {

            }
        }
    }
}