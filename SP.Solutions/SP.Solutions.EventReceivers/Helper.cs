﻿
using Microsoft.Office.DocumentManagement.DocumentSets;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using HtmlToOpenXml;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using A = DocumentFormat.OpenXml.Drawing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
namespace SP.Solutions.EventReceivers
{
    public class Helper
    {
        public static string GetRootDirectory()
        {
            return "C:\\WordGeneration\\";
        }


        #region Terms

        public static string GetFirmNumber(object firmColumn)
        {
            string firmNumber = string.Empty;
            string firmLabel = GetTermLabel(firmColumn);
            if (!string.IsNullOrEmpty(firmLabel))
            {
                firmNumber = firmLabel.Substring(0, firmLabel.IndexOf("-"));
            }
            return firmNumber;
        }
        public static string GetFirmName(object firmColumn)
        {
            string firmName = string.Empty;
            string firmLabel = GetTermLabel(firmColumn);
            if (!string.IsNullOrEmpty(firmLabel))
            {
                firmName = firmLabel.Substring(firmLabel.IndexOf("-") + 1);
            }
            return firmName;
        }

        public static string GetTermString(object term)
        {
            return Convert.ToString(term);
        }

        public static string GetTermGUID(object term)
        {
            string termGUID = string.Empty;
            string strTerm = GetTermString(term);
            if (!string.IsNullOrEmpty(strTerm))
            {
                termGUID = strTerm.Substring(strTerm.LastIndexOf("|") + 1);
            }
            return termGUID;
        }

        public static string GetTermLabel(object term)
        {
            string termLable = string.Empty;
            string strTerm = GetTermString(term);
            if (!string.IsNullOrEmpty(strTerm))
            {
                termLable = strTerm.Substring(0, strTerm.LastIndexOf("|"));
            }
            return termLable;
        }
        #endregion
        #region Person
        public static string GetMultiUserValue(SPWeb oweb, object userFieldValue)
        {
            string names = string.Empty;
            if (userFieldValue != null)
            {
                SPFieldUserValueCollection userValueCollection = new SPFieldUserValueCollection(oweb, userFieldValue.ToString());
                if (userValueCollection != null && userValueCollection.Count > 0)
                {
                    foreach (SPFieldUserValue us in userValueCollection)
                    {
                        if(us.User != null)
                        names += us.User.Name + ", ";
                    }
                }
            }
            return names.Trim(' ').Trim(',');
        }
        #endregion
        #region Lookup
        public static string GetLookupValue(object lookupFieldValue)
        {
            string lookupValue = string.Empty;
            if (lookupFieldValue != null)
            {
                lookupValue = (new SPFieldLookupValue(lookupFieldValue as string)).LookupValue;
            }
            return lookupValue;
        }
        public static string GetLookupId(object lookupFieldValue)
        {
            string lookupId = string.Empty;
            if (lookupFieldValue != null)
            {
                lookupId = (new SPFieldLookupValue(lookupFieldValue as string)).LookupId.ToString();
            }
            return lookupId;
        }

        internal static string GetAttachmentsList(SPListItem item, string listName)
        {
            string tableHtml = "<ul>";
            SPList attachmentList = item.Web.Lists[listName];
            SPQuery qry = new SPQuery();
            qry.Query = "<Where><And><Eq><FieldRef Name='ItemId'/><Value Type='Text'>" + item.ID + "</Value></Eq><Eq><FieldRef Name='IsGeneratedDoc'/><Value Type='Text'>0</Value></Eq></And></Where>";
            SPListItemCollection items = attachmentList.GetItems(qry);
            foreach (SPListItem attachitem in items)
            {
                string filename = Convert.ToString(attachitem["FileLeafRef"]);
                tableHtml = tableHtml + "<li>" + filename.Substring(filename.IndexOf("_") + 1) + "</li>";
            }
            tableHtml += "</ul>";
            return tableHtml;
        }
        #endregion

        #region Date
        public static string GetFormattedDate(object objDate, string format = "MM/dd/yyyy")
        {
            string formattedDate = string.Empty;
            try
            {
                formattedDate = ((DateTime)objDate).ToString(format);
            }
            catch (Exception ex)
            {

            }
            return formattedDate;
        }

        internal static string GetMultilineHtmlValue(SPListItem item, string field)
        {
            SPFieldMultiLineText mlt = item.Fields.GetField(field) as SPFieldMultiLineText;
            return mlt.GetFieldValueAsHtml(item[field] as string);
        }

        internal static string GetMultilineTextValue(SPListItem item, string field)
        {
            SPFieldMultiLineText mlt = item.Fields.GetField(field) as SPFieldMultiLineText;
            return mlt.GetFieldValueAsText(item[field] as string);
        }
        #endregion

        #region Contacts List
        public static Dictionary<string, string> GetContactDetails(SPItemEventProperties properties, object ContactLookup, string prefix)
        {
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            string toContactId = Helper.GetLookupId(ContactLookup);
            keyValues[prefix + " Contact First Name|false"] = string.Empty;
            keyValues[prefix + " Contact Last Name|false"] = string.Empty;
            keyValues[prefix + " Street Address|false"] = string.Empty;
            keyValues[prefix + " City|false"] = string.Empty;
            keyValues[prefix + " State|false"] = string.Empty;
            keyValues[prefix + " Zip|false"] = string.Empty;
            keyValues[prefix + " Phone|false"] = string.Empty;
            keyValues[prefix + " Fax|false"] = string.Empty;
            keyValues[prefix + " Email|false"] = string.Empty;
            keyValues[prefix + " Designation|false"] = string.Empty;
            if (!string.IsNullOrEmpty(toContactId))
            {
                using (SPWeb web = properties.Web)
                {
                    SPList list = web.Lists.TryGetList("STGContactsList");
                    SPListItem contactItem = list.GetItemById(Convert.ToInt32(toContactId));
                    if (contactItem != null)
                    {
                        string contact = Convert.ToString(contactItem["Title"]);
                        keyValues[prefix + " Contact First Name|false"] = contact;
                        if (!string.IsNullOrEmpty(contact) && contact.IndexOf(" ") > 0)
                        {
                            keyValues[prefix + " Contact First Name|false"] = contact.Substring(0, contact.IndexOf(" "));
                            keyValues[prefix + " Contact Last Name|false"] = contact.Substring(contact.IndexOf(" "));
                        }

                        keyValues[prefix + " Street Address|false"] = Convert.ToString(contactItem["PAddress"]);
                        keyValues[prefix + " City|false"] = Convert.ToString(contactItem["PCity"]);
                        keyValues[prefix + " State|false"] = Convert.ToString(contactItem["PState"]);
                        keyValues[prefix + " Zip|false"] = Convert.ToString(contactItem["PZip"]);
                        keyValues[prefix + " Phone|false"] = Convert.ToString(contactItem["Phone"]);
                        keyValues[prefix + " Fax|false"] = Convert.ToString(contactItem["Fax"]);
                        keyValues[prefix + " Email|false"] = Convert.ToString(contactItem["Email"]);
                        //keyValues[prefix + " Designation|false"] = Convert.ToString(contactItem["Title1"]);
                    }

                }
            }
            return keyValues;
        }

        public static void SendLetterMail(SPListItem item, string pdfFilePath)
        {

        }
        static void SendEmail(string to, string subject, string body, string attachmentPath)
        {

            string smtpServer = SPAdministrationWebApplication.Local.OutboundMailServiceInstance.Server.Address;
            string smtpFrom = SPAdministrationWebApplication.Local.OutboundMailSenderAddress;
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(smtpFrom);
            mailMessage.To.Add(new MailAddress(to));
            //Set the subject and body of the message
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            byte[] data = File.ReadAllBytes(attachmentPath);
            MemoryStream memoryStreamOfFile = new MemoryStream(data);
            mailMessage.Attachments.Add(new System.Net.Mail.Attachment(memoryStreamOfFile, attachmentPath.Substring(attachmentPath.LastIndexOf('\\'))));
            SmtpClient smtpClient = new SmtpClient(smtpServer);
            smtpClient.Send(mailMessage);
        }
        #endregion

        public static string CleanupFileName(string fileName)
        {
            //Special Characters Not Allowed: ~ " # % & * : < > ? / \ { | }      
            if (!string.IsNullOrEmpty(fileName))
            {
                //Regex to Replace the Special Character
                fileName = Regex.Replace(fileName, @"[~#'%&*:<>?/\{|}\n]", "");

                if (fileName.Contains("\""))
                {
                    fileName = fileName.Replace("\"", "");
                }

                if (fileName.StartsWith(".", StringComparison.OrdinalIgnoreCase) || fileName.EndsWith(".", StringComparison.OrdinalIgnoreCase))
                {
                    fileName = fileName.TrimStart(new char[] { '.' });
                }
                if (fileName.IndexOf("..", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    fileName = fileName.Replace("..", "");
                }
                fileName = fileName.Replace("/n", string.Empty);
            }
            return fileName;
        }

        public static void ReplaceContent(string documentPath, Dictionary<string, string> dict)
        {

            using (WordprocessingDocument doc = WordprocessingDocument.Open(documentPath, true))
            {
                var body = doc.MainDocumentPart.Document.Body;
                var paras = body.Elements<Paragraph>();
                var tables = body.Elements<Table>();
                var footers = doc.MainDocumentPart.FooterParts;
                var headers = doc.MainDocumentPart.HeaderParts;
                if (dict.ContainsKey("Sign|true"))
                {
                    string fileName = dict["Sign|true"];
                    MainDocumentPart mainPart = doc.MainDocumentPart;

                    ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
                    using (SPSite site = new SPSite("https://sharepoint.oc405.com/ProjAdmin/contract"))
                    {
                        SPWeb web = site.OpenWeb();
                        SPFile file = web.GetFile(fileName);

                        using (Stream stream = file.OpenBinaryStream())
                        {
                            imagePart.FeedData(stream);
                        }
                        Drawing element = AddImageToBody(doc, mainPart.GetIdOfPart(imagePart));
                        Text textPlaceHolder = body.Descendants<Text>().Where((x) => x.Text == "[##Sign##]").First();
                        if (textPlaceHolder != null && textPlaceHolder.Parent is Run)
                        {
                            textPlaceHolder.Parent.InsertAfter<Drawing>(element, textPlaceHolder);
                            textPlaceHolder.Remove();
                        }
                    }

                }

                HtmlConverter converter = new HtmlConverter(doc.MainDocumentPart);

                //bool isAttached = false;
                foreach (KeyValuePair<string, string> item in dict)
                {
                    string[] keyword = item.Key.Split('|');
                    foreach (var para in paras)
                    {
                        foreach (var run in para.Elements<Run>())
                        {
                            foreach (var text in run.Elements<Text>())
                            {
                                if (text.Text.Contains(keyword[0]))
                                {
                                    if (true)//!keyword[0].Equals("Attachments") || (keyword[0].Equals("Attachments") && !isAttached)
                                    {
                                        if (Convert.ToBoolean(keyword[1]))
                                        {
                                            text.Text = text.Text.Replace("[##" + keyword[0] + "##]", "");
                                            var test = converter.Parse(item.Value);
                                            for (var i = 0; i < test.Count; i++)
                                            {
                                                test[i].InnerXml = test[i].InnerXml.Replace("<w:ind w:left=\"0\" w:hanging=\"357\" />", "");
                                                run.AppendChild(test[i]);
                                            }
                                        }
                                        else
                                        {
                                            text.Text = text.Text.Replace("[##" + keyword[0] + "##]", item.Value);
                                        }
                                        /*if (keyword[0].Equals("Attachments"))
                                        {
                                            isAttached = true;
                                        }*/

                                    }
                                }
                            }
                        }
                    }
                    foreach (var header in headers)
                    {
                        foreach (var para in header.RootElement.Elements<Paragraph>())
                        {
                            foreach (var run in para.Elements<Run>())
                            {
                                foreach (var text in run.Elements<Text>())
                                {
                                    if (text.Text.Contains(keyword[0]))
                                    {
                                        if (Convert.ToBoolean(keyword[1]))
                                        {
                                            text.Text = text.Text.Replace("[##" + keyword[0] + "##]", "");
                                            var test = converter.Parse(item.Value);
                                            for (var i = 0; i < test.Count; i++)
                                            {
                                                run.AppendChild(test[i]);
                                            }
                                        }
                                        else
                                        {
                                            text.Text = text.Text.Replace("[##" + keyword[0] + "##]", item.Value);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (var footer in footers)
                    {
                        foreach (var para in footer.RootElement.Elements<Paragraph>())
                        {
                            foreach (var run in para.Elements<Run>())
                            {
                                foreach (var text in run.Elements<Text>())
                                {
                                    if (text.Text.Contains(keyword[0]))
                                    {
                                        if (Convert.ToBoolean(keyword[1]))
                                        {
                                            text.Text = text.Text.Replace("[##" + keyword[0] + "##]", "");
                                            var test = converter.Parse(item.Value);
                                            for (var i = 0; i < test.Count; i++)
                                            {
                                                run.AppendChild(test[i]);
                                            }
                                        }
                                        else
                                        {
                                            text.Text = text.Text.Replace("[##" + keyword[0] + "##]", item.Value);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (var tbl in tables)
                    {
                        foreach (var rows in tbl)
                        {
                            foreach (var cols in rows)
                            {
                                foreach (var para in cols)
                                {
                                    foreach (var run in para.Elements<Run>())
                                    {
                                        foreach (var text in run.Elements<Text>())
                                        {
                                            if (text.Text.Contains(keyword[0]))
                                            {
                                                if (Convert.ToBoolean(keyword[1]))
                                                {
                                                    text.Text = text.Text.Replace("[##" + keyword[0] + "##]", "");
                                                    var test = converter.Parse(item.Value);
                                                    for (var i = 0; i < test.Count; i++)
                                                    {
                                                        test[i].InnerXml = test[i].InnerXml.Replace("<w:ind w:left=\"0\" w:hanging=\"357\" />", "");
                                                        run.AppendChild(test[i]);
                                                    }
                                                }
                                                else
                                                {
                                                    text.Text = text.Text.Replace("[##" + keyword[0] + "##]", item.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                    }
                }

            }
        }

        public static Drawing InsertAPicture(string document, string fileName)
        {
            using (WordprocessingDocument wordprocessingDocument =
                WordprocessingDocument.Open(document, true))
            {
                MainDocumentPart mainPart = wordprocessingDocument.MainDocumentPart;

                ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
                using (SPSite site = new SPSite("https://sharepoint.oc405.com/ProjAdmin/contract"))
                {
                    SPWeb web = site.OpenWeb();
                    SPFile file = web.GetFile(fileName);

                    using (Stream stream = file.OpenBinaryStream())
                    {
                        imagePart.FeedData(stream);
                    }
                    return AddImageToBody(wordprocessingDocument, mainPart.GetIdOfPart(imagePart));
                }
            }
        }

        private static Drawing AddImageToBody(WordprocessingDocument wordDoc, string relationshipId)
        {
            // Define the reference of the image.
            return new Drawing(
                     new DW.Inline(
                         new DW.Extent() { Cx = 1990000L, Cy = 992000L },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         new DW.DocProperties()
                         {
                             Id = (UInt32Value)1U,
                             Name = "Picture 1"
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A.Graphic(
                             new A.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)0U,
                                             Name = "New Bitmap Image.jpg"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A.Blip(
                                             new A.BlipExtensionList(
                                                 new A.BlipExtension()
                                                 {
                                                     Uri =
                                                        "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = relationshipId,
                                             CompressionState =
                                             A.BlipCompressionValues.Print
                                         },
                                         new A.Stretch(
                                             new A.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A.Transform2D(
                                             new A.Offset() { X = 0L, Y = 0L },
                                             new A.Extents() { Cx = 1990000L, Cy = 992000L }),
                                         new A.PresetGeometry(
                                             new A.AdjustValueList()
                                         )
                                         { Preset = A.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)0U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)0U,
                         DistanceFromRight = (UInt32Value)0U,
                         EditId = "50D07946"
                     });

            // Append the reference to body, the element should be in a Run.
            //wordDoc.MainDocumentPart.Document.Body.AppendChild(new Paragraph(new Run(element)));
        }
    }

}
