﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Office.Interop.Word;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace SP.Solutions.EventReceivers.LetterEventReceiver
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class LetterEventReceiver : SPItemEventReceiver
    {
        #region Helper Methods

        //public Dictionary<string, string> PrepareKeyValuePair(SPListItem item)
        //{
        //    Dictionary<string, string> keyValues = new Dictionary<string, string>();
        //    //SPListItem item = properties.ListItem;
        //    try
        //    {
        //        string letterNumber = Convert.ToString(item["LetterNo"]);
        //        if (!string.IsNullOrEmpty(letterNumber))
        //        {
        //            keyValues.Add("Letter Number|false", letterNumber);
        //        }

        //        keyValues.Add("To Name|false", Helper.GetLookupValue(item["To"]));
        //        keyValues.Add("From Name|false", Helper.GetLookupValue(item["From"]));

        //        keyValues.Add("Subject|false", Convert.ToString(item["Subject"]));
        //        keyValues.Add("Contract|false", Convert.ToString(item["Contract"]));
        //        keyValues.Add("Reference|false", Convert.ToString(item["Reference"]));
        //        keyValues.Add("Letter Date|false", Helper.GetFormattedDate(item["LetterDate"]));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return keyValues;
        //}
        public Dictionary<string, string> PrepareKeyValuePair(SPListItem item, SPListItem subItem)
        {
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            try
            {
                string bodyHTML = Helper.GetMultilineHtmlValue(item, "LetterBody");
                bodyHTML = bodyHTML.Replace("<br></", "</");
                bodyHTML = bodyHTML.Replace("<br/></", "</");
                bodyHTML = bodyHTML.Replace("<br/></div>", "</div>");
                bodyHTML = bodyHTML.Replace("<br/></li>", "</li>");
                bodyHTML = bodyHTML.Replace("<br/></span>", "</span>");

                string letterNumber = Convert.ToString(item["LetterNo"]);
                if (!string.IsNullOrEmpty(letterNumber))
                    keyValues.Add("Letter Number|false", letterNumber);
                keyValues.Add("Letter Date|false", Helper.GetFormattedDate(item["LetterDate"], "MMMM d, yyyy"));
                keyValues.Add("Letter Date YYYYMMDD|false", Helper.GetFormattedDate(item["LetterDate"], "yyyyMMdd"));
                keyValues["Body|true"] = bodyHTML;
                keyValues["Attachments|true"] = Helper.GetAttachmentsList(item, "LetterLogDoc");
                keyValues["Revision|false"] = Convert.ToString(item["Revision"]);
                keyValues["CC Name|false"] = Helper.GetMultiUserValue(item.Web, item["CC"]);
                keyValues["Subject|false"] = Convert.ToString(item["Title"]);
                keyValues["Contract|false"] = Convert.ToString(item["Contract"]);
                keyValues["Reference|false"] = Helper.GetMultilineTextValue(item, "Reference");

                SPList list = item.Web.Lists["Signature"];
                SPListItem fromItem = list.GetItemById(Convert.ToInt32(Helper.GetLookupId(item["FromName"])));
                string fileUrl = item.Web.Url + "/" + fromItem.File.Url;
                keyValues["Sign|true"] = fileUrl;
                keyValues["From Name|false"] = Convert.ToString(fromItem["UserName"]);
                keyValues["From Role|false"] = Convert.ToString(fromItem["Title"]);
                if (Convert.ToString(item["LetterType"]).ToLower().Equals("designer"))
                {
                    keyValues["To Name|false"] = "Scott Lucas, PE";
                    keyValues["Dear Name|false"] = "Lucas";
                    keyValues["Role|false"] = "Design Manager";
                    keyValues["Firm|false"] = "Pacific Infrastructure 405 Designers";
                    keyValues["Address1|false"] = "3100 Lake Center Drive, Ste 300";
                    keyValues["Address2|false"] = "Santa Ana, CA 92704";
                }
                else
                {
                    keyValues["To Name|false"] = "Jeff Mills, PE";
                    keyValues["Dear Name|false"] = "Mills";
                    keyValues["Role|false"] = "Program Manager, Highway Programs";
                    keyValues["Firm|false"] = "Orange Country Transportation Authority";
                    keyValues["Address1|false"] = "3100 Lake Center Drive, Ste 300";
                    keyValues["Address2|false"] = "Santa Ana, CA 92704";
                    keyValues["Contract|false"] = "C-5-3843 | 405 Improvement Design-Build Project";
                }



                /*string to = Helper.GetLookupValue(item["To"]);
                keyValues.Add("To Name|false", to);
                keyValues.Add("Dear Name|false", to);
                if (subItem != null)
                {
                    keyValues.Add("Address1|false", Convert.ToString(subItem["Address_x0020_1"]));
                    keyValues.Add("Address2|false", Convert.ToString(subItem["Address_x0020_2"]));
                    keyValues.Add("City|false", Convert.ToString(subItem["City"]));
                    keyValues.Add("State|false", Convert.ToString(subItem["State"]));
                    keyValues.Add("Zip|false", Convert.ToString(subItem["WorkZip"]));
                }*/
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return keyValues;
        }

        #endregion


        /// <summary>
        /// An item was added.
        /// </summary>
        public override void ItemAdded(SPItemEventProperties properties)
        {
            base.ItemAdded(properties);
            if (properties.ListTitle.Equals("Letter Log"))
            {
                base.EventFiringEnabled = false;
                this.ProcessItem(properties);
                base.EventFiringEnabled = true;
            }
        }
        public void ProcessItem(SPItemEventProperties properties)
        {
            SPListItem item = properties.ListItem;
            string Id = Convert.ToString(item["ID"]);
            if (Convert.ToString(item["InOrOut"]).ToLower().Equals("out"))
            {
                string rootDirectory = Helper.GetRootDirectory();
                string sourceFile = string.Empty;
                string tempFolder = Guid.NewGuid().ToString("D");
                string subjectRequest = Convert.ToString(item["Title"]);
                string attachmentName = "Letter-" + subjectRequest + "-" + Id + ".docx";
                attachmentName = Helper.CleanupFileName(attachmentName);
                string tempFolderPath = rootDirectory + tempFolder;
                string destinationFile = tempFolderPath + "\\" + attachmentName;
                if (!Directory.Exists(tempFolderPath))
                {
                    Directory.CreateDirectory(tempFolderPath);
                }
                sourceFile = rootDirectory + "LetterTemplate.docx";
                File.Copy(sourceFile, destinationFile, true);

                var subItems = item["DistributionList"] as SPFieldLookupValueCollection;
                SPListItem subItem = null;
                /*foreach (var nextItem in subItems)
                {
                    SPList spSubList = properties.Web.Lists["LetterLog Contacts"];
                    subItem = spSubList.Items.GetItemById(nextItem.LookupId);
                    break;
                }*/

                Dictionary<string, string> keyValues = this.PrepareKeyValuePair(item, subItem);
                Helper.ReplaceContent(destinationFile, keyValues);
                Application appWord = new Application();
                Document wordDocument = appWord.Documents.Open(destinationFile);
                string pdfFilePath = destinationFile.Substring(0, destinationFile.LastIndexOf('.')) + ".pdf";
                wordDocument.ExportAsFixedFormat(pdfFilePath, WdExportFormat.wdExportFormatPDF);
                wordDocument.Close();
                try
                {
                    /*for (int i = 0; i < properties.ListItem.Attachments.Count; i++)
                    {
                        if (properties.ListItem.Attachments[i].EndsWith("-" + Id + ".pdf"))
                        {
                            properties.ListItem.Attachments.Delete(properties.ListItem.Attachments[i]);
                            properties.ListItem.SystemUpdate();
                            break;
                        }
                    }*/
                    string title = "Letter-" + subjectRequest + "-" + DateTime.Now.ToLongDateString() + "-" + Id;

                    attachmentName = title + ".pdf";
                    attachmentName = Helper.CleanupFileName(attachmentName);
                    string finalPdfPath = pdfFilePath;
                    if (Convert.ToString(item["Status"]).ToLower().Equals("submitted"))
                    {
                        string destinationFolder = tempFolderPath + "\\" + DateTime.Now.ToString("yyyyMMddhhmmss");
                        finalPdfPath = destinationFolder + "\\" + "Final.pdf";
                        Directory.CreateDirectory(destinationFolder);
                        List<string> files = new List<string>();
                        files.Add(pdfFilePath);
                        SPList docList = item.Web.Lists.TryGetList("LetterLogDoc");
                        SPQuery qry = new SPQuery();
                        qry.Query = "<Where><And><Eq><FieldRef Name='ItemId'/><Value Type='Text'>" + item.ID + "</Value></Eq><Eq><FieldRef Name='IsGeneratedDoc'/><Value Type='Text'>0</Value></Eq></And></Where>";
                        SPListItemCollection collListItems = docList.GetItems(qry);
                        if (collListItems.Count > 0)
                        {
                            foreach(SPListItem docItem in collListItems)
                            {
                                string attachment = docItem.File.Name;
                                if (attachment.ToLower().EndsWith(".pdf"))
                                {
                                    SPFile file = item.Web.GetFile(docItem.File.ServerRelativeUrl);
                                    string filePath = destinationFolder + "\\" + attachment;
                                    byte[] binFile = file.OpenBinary();
                                    System.IO.FileStream fs = System.IO.File.Create(filePath);
                                    fs.Write(binFile, 0, binFile.Length);
                                    fs.Close();
                                    files.Add(filePath);
                                }
                            }
                        }
                        

                        MergePDFs(finalPdfPath, files);
                        Helper.SendLetterMail(item, finalPdfPath);
                    }
                    AddorUpdateGeneratedLetter(properties.ListItem, attachmentName, finalPdfPath);
                    /*.ListItem.Attachments.Add(attachmentName, File.ReadAllBytes(pdfFilePath));
                    properties.ListItem.SystemUpdate();*/
                }
                catch (Exception ex)
                {
                }

                File.Delete(destinationFile);
                if (Directory.Exists(tempFolderPath))
                {
                    Directory.Delete(tempFolderPath, true);
                }
            }
            else
            {
                for (int i = 0; i < properties.ListItem.Attachments.Count; i++)
                {
                    if (properties.ListItem.Attachments[i].EndsWith("-" + Id + ".pdf"))
                    {
                        properties.ListItem.Attachments.Delete(properties.ListItem.Attachments[i]);
                        properties.ListItem.SystemUpdate();
                        break;
                    }
                }

            }

        }
        public static void MergePDFs(string targetPath, List<string> pdfs)
        {
            using (PdfDocument targetDoc = new PdfDocument())
            {
                foreach (string pdf in pdfs)
                {
                    using (PdfDocument pdfDoc = PdfReader.Open(pdf, PdfDocumentOpenMode.Import))
                    {
                        for (int i = 0; i < pdfDoc.PageCount; i++)
                        {
                            targetDoc.AddPage(pdfDoc.Pages[i]);
                        }
                    }
                }
                targetDoc.Save(targetPath);
            }
        }
        private void AddorUpdateGeneratedLetter(SPListItem listItem, string attachmentName, string pdfFilePath)
        {

            SPList docList = listItem.Web.Lists.TryGetList("LetterLogDoc");
            SPQuery qry = new SPQuery();
            qry.Query = "<Where><And><Eq><FieldRef Name='ItemId'/><Value Type='Text'>" + listItem.ID + "</Value></Eq><Eq><FieldRef Name='IsGeneratedDoc'/><Value Type='Text'>1</Value></Eq></And></Where>";
            SPListItemCollection collListItems = docList.GetItems(qry);
            if (collListItems.Count > 0)
            {
                collListItems[0].Delete();
            }
            SPFile file = docList.RootFolder.Files.Add(attachmentName, File.ReadAllBytes(pdfFilePath));
            file.Item["IsGeneratedDoc"] = true;
            file.Item["ItemId"] = listItem.ID;
            file.Item.Update();
        }

        /// <summary>
        /// An item was updated.
        /// </summary>
        public override void ItemUpdated(SPItemEventProperties properties)
        {
            base.ItemUpdated(properties);


            if (properties.ListTitle.Equals("Letter Log"))
            {
                base.EventFiringEnabled = false;
                this.ProcessItem(properties);
                base.EventFiringEnabled = true;
                /*
                base.EventFiringEnabled = false;
                    SPListItem item = properties.ListItem;
                    string rootDirectory = Helper.GetRootDirectory();
                    string sourceFile = string.Empty;
                    string tempFolder = Guid.NewGuid().ToString("D");
                    string subjectRequest = Convert.ToString(item["Subject"]);
                    string Id = Convert.ToString(item["ID"]);
                    string attachmentName = "Letter-" + subjectRequest + "-" + Id + ".docx";
                    attachmentName = Helper.CleanupFileName(attachmentName);
                    string tempFolderPath = rootDirectory + tempFolder;
                    string destinationFile = tempFolderPath + "\\" + attachmentName;
                    if (!Directory.Exists(tempFolderPath))
                    {
                        Directory.CreateDirectory(tempFolderPath);
                    }
                    sourceFile = rootDirectory + "LetterTemplate.docx";
                    File.Copy(sourceFile, destinationFile, true);

                    var subItems = item["DistributionList"] as SPFieldLookupValueCollection;
                    SPListItem subItem = null;
                    foreach (var nextItem in subItems)
                    {
                        SPList spSubList = properties.Web.Lists["LLDistribution"];
                        subItem = spSubList.Items.GetItemById(nextItem.LookupId);
                        break;
                    }

                    Dictionary<string, string> keyValues = this.PrepareKeyValuePair(item, subItem);
                    Helper.ReplaceContent(destinationFile, keyValues);
                    Application appWord = new Application();
                    Document wordDocument = appWord.Documents.Open(destinationFile);
                    string pdfFilePath = destinationFile.Substring(0, destinationFile.LastIndexOf('.')) + ".pdf";
                    wordDocument.ExportAsFixedFormat(pdfFilePath, WdExportFormat.wdExportFormatPDF);
                    try
                    {
                        for (int i = 0; i < properties.ListItem.Attachments.Count; i++)
                        {
                            if (properties.ListItem.Attachments[i].EndsWith("-" + Id + ".pdf"))
                            {
                                properties.ListItem.Attachments.Delete(properties.ListItem.Attachments[i]);
                                properties.ListItem.SystemUpdate();
                                break;
                            }
                        }
                        string title = title = "Letter-" + subjectRequest + "-" + DateTime.Now.ToLongDateString() + "-" + Id;

                        attachmentName = title + ".pdf";
                        attachmentName = Helper.CleanupFileName(attachmentName);
                        properties.ListItem.Attachments.Add(attachmentName, File.ReadAllBytes(pdfFilePath));
                        properties.ListItem.SystemUpdate();
                    }
                    catch (Exception ex)
                    {
                    }
                    File.Delete(destinationFile);
                    if (Directory.Exists(tempFolderPath))
                    {
                        Directory.Delete(tempFolderPath, true);
                    }

                    base.EventFiringEnabled = true;*/

            }

        }
    }
}