﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SP.Solutions.PODEmailNotificationSchedular
{
    static class Program
    {
        public static void Main(string[] args)
        {
            var url = ConfigurationManager.AppSettings.Get("WebServiceUrl");
            string siteUrl = ConfigurationManager.AppSettings.Get("SiteUrl");
            
            var json = new WebClient().DownloadString(url);

            RootObject UserList = JsonConvert.DeserializeObject<RootObject>(json);

            var table = "<h2>POD Schedule</h2><table border=1>";
            table += "<tr style='background:#337ab7;color: white;'>";
            table += "<th>REQUEST</th>";
            table += "<th>ON-SITE TIME/DATE</th>";
            table += "<th>STATUS</th>";
            table += "<th>LOCATION/ADDRESS</th>";
            table += "<th>ORIGINATOR</th>";
            table += "<th>DETAILS</th>";
            table += "</tr>";

            var data = UserList.data;
            for (int i = 0; i < data.Count; i++)
            {
                table += "<tr>";
                table += "<td>" + data[i].Type + "</td>";
                table += "<td>" + data[i].Time + "</td>";
                table += "<td>" + data[i].Status + "</td>";
                table += "<td><a href='https://www.google.com/maps/?q=" + data[i].Lat + "," + data[i].Long + "' target='_blank'>" + data[i].Address + "</a></td>";
                table += "<td>" + data[i].CreatedBy + "</td>";
                table += "<td><a href='" + siteUrl + "/Lists/" + data[i].ListInternalName + "/DispForm.aspx?ID=" + data[i].Id + "'>View</a></td>";
                table += "</tr>";
            }
            table += "</table>";

            SendMail(siteUrl, table);
            
        }

        private static void SendMail(string siteUrl, string table)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    SPWeb web = site.OpenWeb();
                    string emails = "";
                    if (!ConfigurationManager.AppSettings.Get("IsDev").ToLower().Equals("true"))
                    {
                        string groupTitle = ConfigurationManager.AppSettings.Get("SPGroupTitle");
                        SPUserCollection users = web.SiteGroups[groupTitle].Users;
                        foreach (SPUser user in users)
                        {
                            emails += user.Email + ",";
                        }
                        emails = emails.Trim(',');
                    }
                    else
                    {
                        emails = ConfigurationManager.AppSettings.Get("Emails");
                    }
                    string subject = ConfigurationManager.AppSettings.Get("Subject");
                    StringDictionary headers = new StringDictionary();
                    headers.Add("content-type", "text/html");
                    headers.Add("from", "sharepoint@oc405.com");
                    headers.Add("sender", "sharepoint@oc405.com");
                    headers.Add("replyto", "no-reply@oc405.com");
                    headers.Add("to", ConfigurationManager.AppSettings.Get("ToEmail"));
                    headers.Add("bcc", emails);
                    headers.Add("subject", ConfigurationManager.AppSettings.Get("Subject"));
                    
                    SPUtility.SendEmail(web, headers, table);
                    //SPUtility.SendEmail(web, ConfigurationManager.AppSettings.Get("FromName"), ConfigurationManager.AppSettings.Get("ToEmail"), "",emails, subject, table);
                }
            });
        }

        public class Datum
        {
            public string Type { get; set; }
            public string ListInternalName { get; set; }
            public string Address { get; set; }
            public string Lat { get; set; }
            public string Long { get; set; }
            public string ListId { get; set; }
            public string Id { get; set; }
            public string Status { get; set; }
            public string CreatedBy { get; set; }
            public string Time { get; set; }
        }

        public class RootObject
        {
            public List<Datum> data { get; set; }
        }
    }
}
