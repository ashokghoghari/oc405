﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;

namespace SP.Solutions.EventReceivers.Safety.SafetyEventReceiver
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class SafetyEventReceiver : SPItemEventReceiver
    {
        /// <summary>
        /// An item is being deleted.
        /// </summary>
        public override void ItemDeleting(SPItemEventProperties properties)
        {
            if (properties.ListItem.Folder != null && properties.ListItem.Folder.ParentFolder.Name.Equals(properties.List.RootFolder.Name))
            {
                bool isMemberOfGroup = false;
                foreach(SPGroup group in properties.Web.CurrentUser.Groups)
                {
                    if(group.Name == "Safety Admin")
                    {
                        isMemberOfGroup = true;
                        base.ItemDeleting(properties);
                        break;
                    }
                }
                if (!isMemberOfGroup)
                {
                    properties.Status = SPEventReceiverStatus.CancelWithError;
                    properties.ErrorMessage = "Root Folder Deletion Not allowed in this library";
                }
            }
            else
            {
                base.ItemDeleting(properties);
            }
        }
        

        /// <summary>
        /// An item is being added
        /// </summary>
        public override void ItemAdding(SPItemEventProperties properties)
        {
            if (properties.ListItem.Folder != null && properties.ListItem.Folder.ParentFolder.Name.Equals(properties.List.RootFolder.Name))
            {
                bool isMemberOfGroup = false;
                foreach (SPGroup group in properties.Web.CurrentUser.Groups)
                {
                    if (group.Name == "Safety Admin")
                    {
                        isMemberOfGroup = true;
                        base.ItemAdding(properties);
                        break;
                    }
                }
                if (!isMemberOfGroup)
                {
                    properties.Status = SPEventReceiverStatus.CancelWithError;
                    properties.ErrorMessage = "Root Folder Move Not allowed in this library";
                }
            }
            else
            {
                base.ItemAdding(properties);
            }
        }
    }
}