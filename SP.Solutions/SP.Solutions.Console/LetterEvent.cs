﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Solutions.Console
{
    public class LetterEventReceiver
    {
        #region Helper Methods

        public Dictionary<string, string> PrepareKeyValuePair(SPListItem item, SPListItem subItem)
        {
            Dictionary<string, string> keyValues = new Dictionary<string, string>();            
            try
            {
                string letterNumber = Convert.ToString(item["LetterNo"]);
                if (!string.IsNullOrEmpty(letterNumber))
                    keyValues.Add("Letter Number|false", letterNumber);
                string to = Helper.GetLookupValue(item["To"]);
                keyValues.Add("To Name|false", to);
                keyValues.Add("CC Name|false", Helper.GetLookupValue(item["CC"]));
                keyValues.Add("From Name|false", Helper.GetLookupValue(item["From"]));
                keyValues.Add("Subject|false", Convert.ToString(item["Subject"]));
                keyValues.Add("Contract|false", Convert.ToString(item["Contract"]));
                keyValues.Add("Reference|false", Helper.GetMultilineTextValue(item, "Reference"));
                keyValues.Add("Letter Date|false", Helper.GetFormattedDate(item["LetterDate"]));
                keyValues.Add("Body|true", Helper.GetMultilineHtmlValue(item, "LetterBody"));

                if (subItem != null)
                {
                    keyValues.Add("Address1|false", Convert.ToString(subItem["Address_x0020_1"]));
                    keyValues.Add("Address2|false", Convert.ToString(subItem["Address_x0020_2"]));
                    keyValues.Add("City|false", Convert.ToString(subItem["City"]));
                    keyValues.Add("State|false", Convert.ToString(subItem["State"]));
                    keyValues.Add("Zip|false", Convert.ToString(subItem["WorkZip"]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return keyValues;
        }


        #endregion

        private string GetDocIdAndRevison(SPItemEventProperties properties)
        {
            string selectedTransmittalNumber = Convert.ToString(properties.ListItem["SelectedNumberId"]);
            int docId = 1000;
            int revison = 0;
            try
            {
                if (!string.IsNullOrEmpty(selectedTransmittalNumber))
                {
                    using (SPWeb web = properties.Web)
                    {
                        SPListItem listItem = properties.List.GetItemById(Convert.ToInt32(selectedTransmittalNumber));
                        if (listItem != null)
                        {
                            docId = Convert.ToInt32(listItem["DocId"]);
                            revison = Convert.ToInt32(listItem["RevisionNumber"]) + 1;
                        }
                    }
                }
                else
                {
                    SPQuery qry = new SPQuery();
                    qry.Query = string.Concat(
                              "<Where><Eq>",
                                 "<FieldRef Name='ToFirmNumber'/>",
                                 "<Value Type='Text'>", Helper.GetTermLabel(properties.ListItem["ToFirmNumber"]), "</Value>",
                              "</Eq></Where>",
                              "<OrderBy>",
                                 "<FieldRef Name='DocId' Ascending='False' />",
                              "</OrderBy>");
                    qry.RowLimit = 1;
                    qry.ViewFields = "<FieldRef Name='DocId'/>";
                    SPListItemCollection listItems = properties.List.GetItems(qry);
                    if (listItems.Count == 1)
                    {
                        docId = Convert.ToInt32(listItems[0]["DocId"]) + 1;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return docId + "." + revison;
        }

        private string GenerateLetterNumber(string firmNumber, string inOut, int docId, int revision)
        {
            return firmNumber + "-" + inOut + "-" + "L" + docId + "." + revision;
        }

        /// <summary>
        /// An item was updated.
        /// </summary>
        public void ItemUpdated()
        {
            //base.ItemUpdated(properties);
            //if (properties.ListTitle.Equals("LetterLog"))
            {

                SPSite spSite = new SPSite("https://sharepoint.oc405.com/ProjAdmin/contract");
                SPWeb web = spSite.OpenWeb();
                SPList spList = web.Lists["Letter Log"];
                SPListItem item = spList.Items.GetItemById(1);

                var subItems = item["DistributionList"] as SPFieldLookupValueCollection;
                SPListItem subItem = spList.Items.GetItemById(1);
                foreach (var nextItem in subItems)
                {
                    SPList spSubList = web.Lists["LLDistribution"]; 
                    subItem = spSubList.Items.GetItemById(nextItem.LookupId); 
                }

                //SPQuery qry = new SPQuery();
                //qry.Query = "<Query><Eq><FieldRef Name='DistributionList' Lookup /></Eq><Query>";
                //SPListItemCollection items = spList.GetItems(qry);

                //SPList spListSub = web.Lists["Letter Log"];
                //SPListItem subItem = spList.Items.GetItemById(1);


                //base.EventFiringEnabled = false;
                //SPListItem item = properties.ListItem;
                string rootDirectory = Helper.GetRootDirectory();
                string sourceFile = string.Empty;
                string tempFolder = Guid.NewGuid().ToString("D");
                string subjectRequest = Convert.ToString(item["Subject"]);
                string Id = Convert.ToString(item["ID"]);
                string attachmentName = "Letter-" + subjectRequest + "-" + Id + ".docx";
                attachmentName = Helper.CleanupFileName(attachmentName);
                string tempFolderPath = rootDirectory + tempFolder;
                string destinationFile = tempFolderPath + "\\" + attachmentName;
                if (!Directory.Exists(tempFolderPath))
                {
                    Directory.CreateDirectory(tempFolderPath);
                }
                sourceFile = rootDirectory + "LetterTemplate.docx";
                File.Copy(sourceFile, destinationFile, true);

                Dictionary<string, string> keyValues = PrepareKeyValuePair(item, subItem);
                Helper.ReplaceContent(destinationFile, keyValues);

                System.Console.ReadLine();
                //try
                //{
                //    for (int i = 0; i < item.Attachments.Count; i++)
                //    {
                //        if (item.Attachments[i].EndsWith("-" + Id + ".docx"))
                //        {
                //            item.Attachments.Delete(item.Attachments[i]);
                //            item.SystemUpdate();
                //            break;
                //        }
                //    }
                //    string title = "Letter-" + subjectRequest + "-" + DateTime.Now.ToLongDateString() + "-" + Id;

                //    attachmentName = title + ".docx";
                //    attachmentName = Helper.CleanupFileName(attachmentName);
                //    item.Attachments.Add(attachmentName, File.ReadAllBytes(destinationFile));
                //    item.SystemUpdate();
                //}
                //catch (Exception ex)
                //{
                //}
                //File.Delete(destinationFile);
                //if (Directory.Exists(tempFolderPath))
                //{
                //    Directory.Delete(tempFolderPath, true);
                //}

                //base.EventFiringEnabled = true;
                //}
            }
        }
    }
}