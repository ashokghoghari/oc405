﻿using Microsoft.Office.DocumentManagement.DocumentSets;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Taxonomy;
using SP.Solutions.EventReceivers.LetterEventReceiver;
using SP.Solutions.Webpart.BatchDocumentUpload;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using HtmlToOpenXml;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using A = DocumentFormat.OpenXml.Drawing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using SPClient = Microsoft.SharePoint.Client;
using Syncfusion.Pdf.Parsing;
using word =  Microsoft.Office.Interop.Word;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace SP.Solutions.Console
{
    class Program
    {
        enum SPColumnTypes
        {
            Date,
            ManagedMetaData,
            String
        }

        class SPColumnInfo
        {
            public SPColumnInfo()
            {
                SPColumnType = SPColumnTypes.String;
            }
            public string SPColumnName { get; set; }

            public SPColumnTypes SPColumnType { get; set; }
            public string TermStoreGroupName { get; set; }
            public bool IsAlloedMultipleTermValues { get; set; }
        }
        public static Drawing InsertAPicture(string document, string fileName)
        {
            using (WordprocessingDocument wordprocessingDocument =
                WordprocessingDocument.Open(document, true))
            {
                MainDocumentPart mainPart = wordprocessingDocument.MainDocumentPart;

                ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
                using(SPSite site = new SPSite("https://sharepoint.oc405.com/ProjAdmin/contract"))
                {
                    SPWeb web = site.OpenWeb();
                    SPFile file = web.GetFile(fileName);
                    
                    using (Stream stream = file.OpenBinaryStream())
                    {
                        imagePart.FeedData(stream);
                    }
                    return AddImageToBody(wordprocessingDocument, mainPart.GetIdOfPart(imagePart));
                }
            }
        }

        private static Drawing AddImageToBody(WordprocessingDocument wordDoc, string relationshipId)
        {
            // Define the reference of the image.
            return new Drawing(
                     new DW.Inline(
                         new DW.Extent() { Cx = 1990000L, Cy = 992000L },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         new DW.DocProperties()
                         {
                             Id = (UInt32Value)1U,
                             Name = "Picture 1"
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A.Graphic(
                             new A.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)0U,
                                             Name = "New Bitmap Image.jpg"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A.Blip(
                                             new A.BlipExtensionList(
                                                 new A.BlipExtension()
                                                 {
                                                     Uri =
                                                        "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = relationshipId,
                                             CompressionState =
                                             A.BlipCompressionValues.Print
                                         },
                                         new A.Stretch(
                                             new A.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A.Transform2D(
                                             new A.Offset() { X = 0L, Y = 0L },
                                             new A.Extents() { Cx = 1990000L, Cy = 992000L }),
                                         new A.PresetGeometry(
                                             new A.AdjustValueList()
                                         )
                                         { Preset = A.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)0U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)0U,
                         DistanceFromRight = (UInt32Value)0U,
                         EditId = "50D07946"
                     });

            // Append the reference to body, the element should be in a Run.
            //wordDoc.MainDocumentPart.Document.Body.AppendChild(new Paragraph(new Run(element)));
        }

        public static void MergePDFs(string targetPath, List<string> pdfs)
        {
            using (PdfDocument targetDoc = new PdfDocument())
            {
                foreach (string pdf in pdfs)
                {
                    using (PdfDocument pdfDoc = PdfReader.Open(pdf, PdfDocumentOpenMode.Import))
                    {
                        for (int i = 0; i < pdfDoc.PageCount; i++)
                        {
                            targetDoc.AddPage(pdfDoc.Pages[i]);
                        }
                    }
                }
                targetDoc.Save(targetPath);
            }
        }

        static void Main(string[] args)
        {
            List<string> pdfs = new List<string>();
            pdfs.Add("D:\\1.pdf");
            pdfs.Add("D:\\2.pdf");
            MergePDFs("D:\\3.pdf", pdfs);
            /*string destinationFile = "C:\\WordGeneration\\test.docx";
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            keyValues["Delivery Address|false"] = "Temp Content";
            Helper.ReplaceContent(destinationFile, keyValues);
            word.Application appWord = new word.Application();
            word.Document wordDocument = appWord.Documents.Open(destinationFile);
            string pdfFilePath = destinationFile.Substring(0, destinationFile.LastIndexOf('.')) + ".pdf";
            wordDocument.ExportAsFixedFormat(pdfFilePath, word.WdExportFormat.wdExportFormatPDF);
            wordDocument.Close();*/
            /*List<string> to = new List<string>();
            to.Add("ashokghoghari@gmail.com");
            List<string> cc = new List<string>();
            List<string> bcc = new List<string>();
            string subject = "Test Subject";
            string body = @"<p><b>REQUISITION APPROVAL REQUEST</b></p>"+
            "<p><b>Submitted by:</b> " +  + "</p>"+
            "<p><b>Date of Submittal:</b> Friday, July 12, 2019</p>"+
            "<p><b>Type of Material:</b> Consumables</p>"+
            "<p><b>Details:</b></p>"+
            "<p>Test Briefly describe the work to be performed with these materials/supplies</p>"+
            "<p>Test What limitations/opportunities should be taken into consideration for this purchase</p>"+
            "<p><a href='#'>Click here to review and approve the requisition.</a></p>"+
            "<p><b>***Procurement Department will not see this requisition until it is fully approved.***</b></p>";
            SendEmail(to,subject,body,null,cc,bcc);*/

            //using (SPSite site = new SPSite("https://sharepoint.oc405.com"))
            //{
            //    SPWeb web = site.OpenWeb();
            //    SPContentType spct = web.AvailableContentTypes["Master Document Set"];
            //    System.Console.ReadKey();
            //}


            //using (SPSite spSite = new SPSite("https://sharepoint.oc405.com/DControl/ProjDocs"))
            //{
            //    using (SPWeb web = spSite.OpenWeb())
            //    {
            //        TaxonomySession taxonomySession = new TaxonomySession(spSite);
            //        TermStore termStore = taxonomySession.TermStores["Managed Metadata Service"];
            //        Group group = termStore.Groups["OC405 Codes"];
            //        TermSet termSet = group.TermSets["Contract Section"];
            //        Term term = termSet.Terms["01 - Contract Components"];
            //    }
            //}
            /*BatchDocumentUploadUserControl p = new BatchDocumentUploadUserControl();
            p.ExecuteFromProgram();*/

            //Program p = new Program();
            //p.ExecuteFromProgram();

            /*LetterEventReceiver dr = new LetterEventReceiver();
            dr.ItemUpdated();*/
            /*string attachmentPath = @"C:\WordGeneration\2.docx";
            string smtpServer = SPAdministrationWebApplication.Local.OutboundMailServiceInstance.Server.Address;
            string smtpFrom = SPAdministrationWebApplication.Local.OutboundMailSenderAddress;
            MailAddressCollection to = new MailAddressCollection();
            to.Add(new MailAddress("p.park@oc405.com"));
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(smtpFrom);
            mailMessage.To.Add(new MailAddress("p.park@oc405.com"));
            //Set the subject and body of the message
            mailMessage.Subject = "Letter";
            mailMessage.Body = "PFA...";
            byte[] data = File.ReadAllBytes(attachmentPath);
            MemoryStream memoryStreamOfFile = new MemoryStream(data);
            mailMessage.Attachments.Add(new System.Net.Mail.Attachment(memoryStreamOfFile, attachmentPath.Substring(attachmentPath.LastIndexOf('\\'))));
            SmtpClient smtpClient = new SmtpClient(smtpServer);
            smtpClient.Send(mailMessage);*/
            /*string sourceFile = @"D:\test\123.docx";
            string destFile = @"D:\test\dest.docx";
            if (File.Exists(destFile))
            {
                File.Delete(destFile);
            }
            File.Copy(sourceFile, destFile);
            string fileName = @"https://sharepoint.oc405.com/ProjAdmin/contract/Signature/Azzam%20Saad.jpg";
            Drawing element = InsertAPicture(destFile, fileName);
            using (WordprocessingDocument doc = WordprocessingDocument.Open(destFile, true))
            {
                Text textPlaceHolder = doc.MainDocumentPart.Document.Body.Descendants<Text>().Where((x) => x.Text == "[##Sign##]").First();
                textPlaceHolder.Parent.InsertAfter<Drawing>(element, textPlaceHolder);
                textPlaceHolder.Remove();
            }*/
            /*using (SPSite site = new SPSite("https://sharepoint.oc405.com/ProjAdmin/contract"))
            {
                SPWeb web = site.OpenWeb();
                SPList docList = web.Lists.TryGetList("LetterLogDoc");
                SPQuery qry = new SPQuery();
                qry.Query = "<Where><And><Eq><FieldRef Name='ItemId'/><Value Type='Text'>15</Value></Eq><Eq><FieldRef Name='IsGeneratedDoc'/><Value Type='Text'>1</Value></Eq></And></Where>";
                //<And><Eq><FieldRef Name='ItemId'/><Value Type='Text'>17</Value></Eq<Eq><FieldRef Name='IsGeneratedDoc'/><Value Type='Boolean'>1</Value></Eq></And>
                SPListItemCollection collListItems = docList.GetItems(qry);
                if (collListItems.Count > 0)
                {
                    collListItems[0].Delete();
                }
                SPFile file = docList.RootFolder.Files.Add("test1.txt", File.ReadAllBytes("C:\\test.txt"));
                file.Item["IsGeneratedDoc"] = true;
                file.Item["ItemId"] = 15;
                file.Item.Update();
                docList.GetItems();
            }*/
            /*
            SPSecurity.RunWithElevatedPrivileges(delegate() {
                using(SPSite site = new SPSite("https://sharepoint.oc405.com/pp"))
                {
                    SPWeb web = site.OpenWeb();
                    SPList sourceList = web.Lists.TryGetList("AESCOSource");
                    SPList targetList = web.Lists.TryGetList("AESCOTarget");
                    SPListItem sourceItem = sourceList.GetItemById(1);
                    SPFile file = sourceItem.File;
                    if (file != null)
                    {

                        // retrieve the file as a byte array byte[] bArray = file.OpenBinary(); 
                        string filePath = Path.Combine("C:\\PDFFlattening","old_" +  file.Name);
                        //open the file stream and write the file 
                        using (FileStream filestream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                        {
                            byte[] bArray = file.OpenBinary();
                            filestream.Write(bArray, 0, bArray.Length);
                        }
                        PdfLoadedDocument doc = new PdfLoadedDocument(filePath);
                        PdfLoadedForm form = doc.Form;
                        doc.Form.Flatten = true;
                        //doc.Security.OwnerPassword = "owner";
                        doc.Security.Permissions = Syncfusion.Pdf.Security.PdfPermissionsFlags.Default;
                        filePath = Path.Combine("C:\\PDFFlattening", file.Name);
                        doc.Save(filePath);
                        doc.Close();

                        SPFile spfile = targetList.RootFolder.Files.Add(filePath.Substring(filePath.LastIndexOf("\\") + 1), File.ReadAllBytes(filePath));
                        spfile.Item["Title"] = "Uploaded";
                        spfile.Item.Update();
                    }
                }
            });*/

        }
        private static void SendEmail(List<string> To, string Subject, string Body, AttachmentCollection attachments = null, List<string> CC = null, List<string> BCC = null)
        {
            try
            {
                MailMessage msg = new MailMessage();
                MailAddress fromAddress = new MailAddress("sharepoint@oc405.com");
                msg.From = fromAddress;
                for (int i = 0; i < To.Count; i++)
                {
                    msg.To.Add(To[i].ToString());
                }
                if (CC != null)
                {
                    for (int i = 0; i < CC.Count; i++)
                    {
                        msg.CC.Add(CC[i].ToString());
                    }
                }
                if(BCC != null)
                {
                    for (int i = 0; i < BCC.Count; i++)
                    {
                        msg.Bcc.Add(BCC[i].ToString());
                    }
                }
                msg.IsBodyHtml = true;
                msg.Subject = Subject;
                msg.Body = Body;
                if(attachments != null) {
                    for (int i = 0; i < attachments.Count; i++)
                    {
                        msg.Attachments.Add(attachments[i]);
                    }
                }

                SmtpClient client = new SmtpClient("oc405sp01.oc405.local");
                client.UseDefaultCredentials = true;
                client.Send(msg);
            }
            catch (Exception Ex)
            {

            }
        
    }
    private void AddorUpdateGeneratedLetter(SPListItem listItem, string attachmentName, string pdfFilePath)
        {

            SPList docList = listItem.Web.Lists.TryGetList("LetterLogDoc");
            SPQuery qry = new SPQuery();
            qry.ViewXml = "<Where><And><Eq><FieldRef Name='IsGeneratedDoc'/><Value Type='Boolean'>1</Value></Eq><Eq><FieldRef Name='ItemId'/><Value Type='Number'" + listItem.ID + "</Value></Eq></And></Where>";
            SPListItemCollection collListItems = docList.GetItems(qry);
            if (collListItems.Count > 0)
            {
                collListItems[0].Delete();
            }
            docList.GetItems();
        }
        static Dictionary<string, SPColumnInfo> _mappingInfo = null;
        static Group _groups = null;
        public static string folderPath = "C:\\UploadFiles\\";
        public static string completeFolderName = "Completed\\";
        public static string logFolderName = "Logs\\";
        private static bool runAsync = true;
        private static string outLogPath;
        private static string siteURL = "https://sharepoint.oc405.com/DControl/ProjDocs";

        public void ExecuteFromProgram()
        {
            runAsync = false;
            this.Page_Load(null, null);
            this.btnSync_Click(null, null);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _mappingInfo = new Dictionary<string, SPColumnInfo>();
            _mappingInfo.Add("3rd Party", new SPColumnInfo() { SPColumnName = "_x0033_rd_x0020_Party_x0020_Contractor", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Sub Contractors" });
            _mappingInfo.Add("3rd Party Document #", new SPColumnInfo() { SPColumnName = "_x0033_rdPartyDocumentNo" });
            _mappingInfo.Add("AconexReferenceNo", new SPColumnInfo() { SPColumnName = "AconexReferenceNo" });
            _mappingInfo.Add("Buy America", new SPColumnInfo() { SPColumnName = "BuyAmerica" });
            _mappingInfo.Add("Change Order", new SPColumnInfo() { SPColumnName = "ChangeOrder" });
            _mappingInfo.Add("Conflict ID Number", new SPColumnInfo() { SPColumnName = "ConflictIDNumber" });
            _mappingInfo.Add("Contract Section", new SPColumnInfo() { SPColumnName = "ContractSection", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "TP Section" });
            _mappingInfo.Add("Correspondence", new SPColumnInfo() { SPColumnName = "Correspondence", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Letters" });
            _mappingInfo.Add("CT Categories", new SPColumnInfo() { SPColumnName = "CTCategories", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "CT Categories" });
            _mappingInfo.Add("Date Due Back", new SPColumnInfo() { SPColumnName = "DateDueBack", SPColumnType = SPColumnTypes.Date });
            _mappingInfo.Add("Date Received Back", new SPColumnInfo() { SPColumnName = "DateReceivedBack", SPColumnType = SPColumnTypes.Date });
            _mappingInfo.Add("Date Sent", new SPColumnInfo() { SPColumnName = "DateSent", SPColumnType = SPColumnTypes.Date });
            _mappingInfo.Add("DBE", new SPColumnInfo() { SPColumnName = "DBE" });
            _mappingInfo.Add("Departments", new SPColumnInfo() { SPColumnName = "Departments", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Departments" });
            _mappingInfo.Add("Deviation #", new SPColumnInfo() { SPColumnName = "DeviationNo" });
            _mappingInfo.Add("Directive Letter #", new SPColumnInfo() { SPColumnName = "DirectiveLetterNo" });
            _mappingInfo.Add("Discipline", new SPColumnInfo() { SPColumnName = "Discipline", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Discipline", IsAlloedMultipleTermValues = true });
            _mappingInfo.Add("Doc Type", new SPColumnInfo() { SPColumnName = "DocType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Document Type" });
            _mappingInfo.Add("DocumentAttribute", new SPColumnInfo() { SPColumnName = "DocumentAttribute", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "DocumentAttribute" });
            _mappingInfo.Add("Document #", new SPColumnInfo() { SPColumnName = "DocumentNo" });
            _mappingInfo.Add("DocumentSet", new SPColumnInfo() { SPColumnName = "DocumentSet" });
            _mappingInfo.Add("DocumentSetLabel", new SPColumnInfo() { SPColumnName = "DocumentSetLabel" });
            _mappingInfo.Add("Document Set Type", new SPColumnInfo() { SPColumnName = "DocumentSetType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Document Set Type" });
            _mappingInfo.Add("Document Status", new SPColumnInfo() { SPColumnName = "DocumentStatus", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Document Status" });
            _mappingInfo.Add("Document Title", new SPColumnInfo() { SPColumnName = "DocumentTitle" });
            _mappingInfo.Add("Durations", new SPColumnInfo() { SPColumnName = "Durations" });
            _mappingInfo.Add("ECR", new SPColumnInfo() { SPColumnName = "ECR", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "ECR" });
            _mappingInfo.Add("Inspector Name", new SPColumnInfo() { SPColumnName = "InspectorName", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Inspector Name" });
            _mappingInfo.Add("Material Type", new SPColumnInfo() { SPColumnName = "MaterialType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Material Type" });
            _mappingInfo.Add("Material Testing", new SPColumnInfo() { SPColumnName = "MaterialTesting", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Material Testing" });
            _mappingInfo.Add("NCR Disposition", new SPColumnInfo() { SPColumnName = "NCRDisposition", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "NCR Disposition" });
            _mappingInfo.Add("NCR #", new SPColumnInfo() { SPColumnName = "NCRNo" });
            _mappingInfo.Add("NCR Type", new SPColumnInfo() { SPColumnName = "NCRType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "NCR Type" });
            _mappingInfo.Add("NDC #", new SPColumnInfo() { SPColumnName = "NDCNo" });
            _mappingInfo.Add("Permissions", new SPColumnInfo() { SPColumnName = "Permissions", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Permissions" });
            _mappingInfo.Add("Permits", new SPColumnInfo() { SPColumnName = "Permits" });
            _mappingInfo.Add("Permit Type", new SPColumnInfo() { SPColumnName = "PermitType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Permit Type" });
            _mappingInfo.Add("Potential Change Order", new SPColumnInfo() { SPColumnName = "PotentialChangeOrder" });
            _mappingInfo.Add("Quality Doc #", new SPColumnInfo() { SPColumnName = "QualityDocNo" });
            _mappingInfo.Add("Quality Docs Type", new SPColumnInfo() { SPColumnName = "QualityDocsType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Quality Document" });
            _mappingInfo.Add("Responsible Person", new SPColumnInfo() { SPColumnName = "ResponsiblePerson" });
            _mappingInfo.Add("Revision #", new SPColumnInfo() { SPColumnName = "RevisionNo" });
            _mappingInfo.Add("RFI #", new SPColumnInfo() { SPColumnName = "RFINo" });
            _mappingInfo.Add("RFI Response", new SPColumnInfo() { SPColumnName = "RFIResponse" });
            _mappingInfo.Add("Safety", new SPColumnInfo() { SPColumnName = "Safety" });
            _mappingInfo.Add("Specification Section", new SPColumnInfo() { SPColumnName = "SpecificationSection", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "CT Specifications" });
            _mappingInfo.Add("Task Order", new SPColumnInfo() { SPColumnName = "TaskOrder" });
            _mappingInfo.Add("TP Section", new SPColumnInfo() { SPColumnName = "TPSection", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "TP Section" });
            _mappingInfo.Add("Utility", new SPColumnInfo() { SPColumnName = "StrUtility"/*, SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Utility" */});
            _mappingInfo.Add("Unit ID", new SPColumnInfo() { SPColumnName = "UnitID", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "UNIT ID" });
            if (runAsync)
            {
                siteURL = SPContext.Current.Web.Url;
            }
            using (SPSite spSite = new SPSite(siteURL))
            {
                using (SPWeb web = spSite.OpenWeb())
                {
                    TaxonomySession taxonomySession = new TaxonomySession(spSite);
                    TermStore termStore = taxonomySession.TermStores["Managed Metadata Service"];
                    _groups = termStore.Groups["OC405 Codes"];
                }
            }
        }

        public static DataTable ReadCSV(string csvPath, string outCSVPath)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[50] {
                new DataColumn("DocumentAttribute", typeof(string)),
                 new DataColumn("Document Set Type", typeof(string)),
                 new DataColumn("Discipline", typeof(string)),
                 new DataColumn("DocumentSet", typeof(string)),
                 new DataColumn("DocumentSetLabel", typeof(string)),
                 new DataColumn("AconexReferenceNo", typeof(string)),
                 new DataColumn("Document #", typeof(string)),
                 new DataColumn("Document Title", typeof(string)),
                 new DataColumn("Doc Type", typeof(string)),
                 new DataColumn("Revision #", typeof(string)),
                 new DataColumn("Durations", typeof(string)),
                 new DataColumn("Date Sent", typeof(string)),
                 new DataColumn("Date Due Back", typeof(string)),
                 new DataColumn("Date Received Back", typeof(string)),
                 new DataColumn("Document Status", typeof(string)),
                 new DataColumn("Correspondence", typeof(string)),
                 new DataColumn("Contract Section", typeof(string)),
                 new DataColumn("TP Section", typeof(string)),
                 new DataColumn("Change Order", typeof(string)),
                 new DataColumn("Deviation #", typeof(string)),
                 new DataColumn("Potential Change Order", typeof(string)),
                 new DataColumn("Directive Letter #", typeof(string)),
                 new DataColumn("Task Order", typeof(string)),
                 new DataColumn("DBE", typeof(string)),
                 new DataColumn("Utility", typeof(string)),
                 new DataColumn("Conflict ID Number", typeof(string)),
                 new DataColumn("Unit ID", typeof(string)),
                 new DataColumn("Departments", typeof(string)),
                 new DataColumn("Responsible Person", typeof(string)),
                 new DataColumn("Inspector Name", typeof(string)),
                 new DataColumn("Specification Section", typeof(string)),
                 new DataColumn("3rd Party Document #", typeof(string)),
                 new DataColumn("3rd Party", typeof(string)),
                 new DataColumn("CT Categories", typeof(string)),
                 new DataColumn("ECR", typeof(string)),
                 new DataColumn("Material Type", typeof(string)),
                 new DataColumn("Material Testing", typeof(string)),
                 new DataColumn("Buy America", typeof(string)),
                 new DataColumn("Permits", typeof(string)),
                 new DataColumn("Permit Type", typeof(string)),
                 new DataColumn("Quality Docs Type", typeof(string)),
                 new DataColumn("Quality Doc #", typeof(string)),
                 new DataColumn("Safety", typeof(string)),
                 new DataColumn("RFI #", typeof(string)),
                 new DataColumn("RFI Response", typeof(string)),
                 new DataColumn("NCR #", typeof(string)),
                 new DataColumn("NCR Disposition", typeof(string)),
                 new DataColumn("NCR Type", typeof(string)),
                 new DataColumn("NDC #", typeof(string)),
                 new DataColumn("Permissions", typeof(string))
            });

            //Read the contents of CSV file.  
            string csvData = File.ReadAllText(csvPath);

            //Execute a loop over the rows.  
            string[] rows = csvData.Split('\n');
            bool isHeader = true;
            foreach (string row in rows)
            {
                if (!string.IsNullOrEmpty(row) && !isHeader)
                {
                    dt.Rows.Add();
                    int i = 0;

                    //Execute a loop over the columns.  
                    foreach (string cell in row.Split(','))
                    {
                        if (i < dt.Columns.Count)
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    isHeader = false;
                }
            }
            //File.Move(csvPath, outCSVPath);
            return dt;
        }

        static SPFolder VerifyDocumentSetOld(SPWeb web, DataRow row)
        {
            string documentSetName = Convert.ToString(row["DocumentSetLabel"]);
            File.AppendAllText(outLogPath, string.Format("===================================================={0}==========================================================", documentSetName));
            SPFolder folder = null;
            try
            {
                SPList sharedDocs = web.Lists["Project Documents"];
                if (sharedDocs != null)
                {
                    
                    SPQuery objQuery = new SPQuery();
                    objQuery.Query = @"<Where><And>
                                <Eq><FieldRef Name='FSObjType'/><Value Type='Integer'>1</Value></Eq>
                                <Eq><FieldRef Name='FileLeafRef' /><Value Type='Text'>" + documentSetName + "</Value></Eq></And></Where>";
                    SPListItemCollection lst = sharedDocs.GetItems(objQuery);
                    if (lst.Count > 0)
                    {
                        folder = lst[0].Folder;
                    }
                    else
                    {
                        SPContentType docSetCT = sharedDocs.ContentTypes["Master Document Set"];
                        if (docSetCT != null)
                        {
                            Hashtable props = GetColumnProps(row, out Dictionary<string, SPColumnInfo> propsManagedData);
                            DocumentSet docSet = DocumentSet.Create(sharedDocs.RootFolder, documentSetName, docSetCT.Id, props, true);
                            DocumentSet MyDocSet = DocumentSet.GetDocumentSet(sharedDocs.GetItemById(docSet.Item.ID).Folder);
                            foreach (var metaDataEntry in propsManagedData.Where(x => row[x.Key] != null && !string.IsNullOrEmpty(row[x.Key].ToString())))
                            {
                                try
                                {
                                    TermSet termSet = _groups.TermSets[metaDataEntry.Value.TermStoreGroupName];
                                    Term term = termSet.Terms[Convert.ToString(row[metaDataEntry.Key]).Trim()];

                                    //DocumentLibrary.Fields
                                    TaxonomyField taxonomyField = MyDocSet.ParentList.Fields[metaDataEntry.Value.SPColumnName] as TaxonomyField;
                                    TaxonomyFieldValue taxonomyFieldValue = new TaxonomyFieldValue(taxonomyField);
                                    taxonomyFieldValue.TermGuid = term.Id.ToString();
                                    taxonomyFieldValue.Label = term.Name;

                                    if (metaDataEntry.Value.IsAlloedMultipleTermValues)
                                    {
                                        TaxonomyFieldValueCollection values = new TaxonomyFieldValueCollection(taxonomyField);
                                        values.Add(taxonomyFieldValue);
                                        MyDocSet.Item[metaDataEntry.Value.SPColumnName] = values;
                                    }
                                    else
                                        MyDocSet.Item[metaDataEntry.Value.SPColumnName] = taxonomyFieldValue;
                                }
                                catch (Exception ex)
                                {

                                }

                            }
                            MyDocSet.Item.Update();
                            folder = MyDocSet.Folder;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(outLogPath, string.Format("Not able to create DocumentSet : {0}", documentSetName));
                File.AppendAllText(outLogPath, ex.Message);
                File.AppendAllText(outLogPath, ex.StackTrace);

            }
            return folder;
        }

        private static void Updateprops(DataRow row, SPListItem item)
        {

            foreach (KeyValuePair<string, SPColumnInfo> entry in _mappingInfo)
            {
                switch (entry.Value.SPColumnType)
                {
                    case SPColumnTypes.Date:
                        if (row[entry.Key] != null && !string.IsNullOrEmpty(row[entry.Key].ToString()))
                        {
                            try
                            {
                                //DateTime dt;
                                bool success = DateTime.TryParse(row[entry.Key].ToString(), out DateTime dt);
                                if (success)
                                    item[entry.Value.SPColumnName] = dt;
                            }
                            catch (Exception ex)
                            { }

                        }
                        break;
                    case SPColumnTypes.ManagedMetaData:
                        break;
                    case SPColumnTypes.String:
                    default:
                        item[entry.Value.SPColumnName] = Convert.ToString(row[entry.Key]);
                        break;
                }

            }
        }

        static SPFolder VerifyDocumentSet(SPWeb web, DataRow row)
        {
            SPFolder folder = null;
            SPList sharedDocs = web.Lists["Project Documents"];
            if (sharedDocs != null)
            {
                folder = sharedDocs.RootFolder;
            }
            return folder;
        }
        private static Hashtable GetColumnProps(DataRow row, out Dictionary<string, SPColumnInfo> propsManagedMetaData)
        {
            Hashtable props = new Hashtable();
            propsManagedMetaData = new Dictionary<string, SPColumnInfo>();

            foreach (KeyValuePair<string, SPColumnInfo> entry in _mappingInfo)
            {
                switch (entry.Value.SPColumnType)
                {
                    case SPColumnTypes.Date:
                        if (row[entry.Key] != null && !string.IsNullOrEmpty(row[entry.Key].ToString()))
                        {
                            try
                            {
                                //DateTime dt;
                                bool success = DateTime.TryParse(row[entry.Key].ToString(), out DateTime dt);
                                if (success)
                                    props.Add(entry.Value.SPColumnName, dt);
                            }
                            catch (Exception ex)
                            { }

                        }
                        break;
                    case SPColumnTypes.ManagedMetaData:
                        propsManagedMetaData.Add(entry.Key, entry.Value);

                        /*if (row[entry.Key] != null && !string.IsNullOrEmpty(row[entry.Key].ToString()))
                        {
                            TermSet termSet = _groups.TermSets[entry.Value.TermStoreGroupName];
                            Term term = termSet.Terms[Convert.ToString(row[entry.Key])];
                            TaxonomyFieldValue taxonomyFieldValue = new TaxonomyFieldValue(term.Id.ToString());
                            taxonomyFieldValue.TermGuid = term.Id.ToString();
                            taxonomyFieldValue.Label = term.Name;

                            props.Add(entry.Value.SPColumnName, Convert.ToString(row[entry.Key]));
                        }*/
                        break;
                    case SPColumnTypes.String:
                    default:
                        props.Add(entry.Value.SPColumnName, Convert.ToString(row[entry.Key]));
                        break;
                }

            }

            return props;
        }
        static void AsyncMethod(string spSiteURL, string csvFilePath, string outCSVPath)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                DateTime StartTime = DateTime.Now;
                using (SPSite spSite = new SPSite(spSiteURL))
                {
                    using (SPWeb web = spSite.OpenWeb())
                    {
                        var outLog = new StringBuilder();
                        DataTable dt = ReadCSV(csvFilePath, outCSVPath);
                        var grouped = from table in dt.AsEnumerable()
                                      group table by new { documentSetName = table["DocumentSet"] } into groupby
                                      select new
                                      {
                                          Value = groupby.Key,
                                          ColumnValues = groupby
                                      };
                        foreach (var key in grouped)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(key.Value.documentSetName)))
                            {
                                SPFolder docSetFolder = null;
                                if (key.ColumnValues.Count() > 0)
                                {
                                    //docSetFolder = VerifyDocumentSet(web, key.ColumnValues.FirstOrDefault());
                                    docSetFolder = VerifyDocumentSetOld(web, key.ColumnValues.FirstOrDefault());
                                }
                                if (docSetFolder != null)
                                {
                                    foreach (DataRow row in key.ColumnValues)
                                    {
                                        string filestatus = "File Not Found";
                                        string msg = "Success";
                                        string overWriteMessage = string.Empty;
                                        try
                                        {
                                            string[] dirs = Directory.GetFiles(folderPath, row["Document #"] + "*");
                                            if (dirs.Length > 0)
                                            {
                                                string fileURL = dirs[0];
                                                string fileName = fileURL.Substring(fileURL.LastIndexOf("\\") + 1);

                                                Hashtable props = GetColumnProps(row, out Dictionary<string, SPColumnInfo> propsManagedData);
                                                SPFile newFile = null;
                                                
                                                try
                                                {
                                                    newFile = docSetFolder.Files.Add(fileName, File.ReadAllBytes(fileURL), props, false);
                                                }
                                                catch(Exception ex)
                                                {
                                                    newFile = docSetFolder.Files.Add(fileName, File.ReadAllBytes(fileURL), props, true);
                                                    if(newFile != null)
                                                    {
                                                        overWriteMessage = "\n File is overWritten...";
                                                    }
                                                }
                                                

                                                foreach (var metaDataEntry in propsManagedData.Where(x => row[x.Key] != null && !string.IsNullOrEmpty(row[x.Key].ToString())))
                                                {
                                                    try
                                                    {
                                                        TermSet termSet = _groups.TermSets[metaDataEntry.Value.TermStoreGroupName];
                                                        Term term = termSet.Terms[Convert.ToString(row[metaDataEntry.Key]).Trim()];

                                                        TaxonomyField taxonomyField = newFile.DocumentLibrary.Fields[metaDataEntry.Value.SPColumnName] as TaxonomyField;
                                                        TaxonomyFieldValue taxonomyFieldValue = new TaxonomyFieldValue(taxonomyField);
                                                        taxonomyFieldValue.TermGuid = term.Id.ToString();
                                                        taxonomyFieldValue.Label = term.Name;

                                                        if (metaDataEntry.Value.IsAlloedMultipleTermValues)
                                                        {
                                                            TaxonomyFieldValueCollection values = new TaxonomyFieldValueCollection(taxonomyField);
                                                            values.Add(taxonomyFieldValue);
                                                            newFile.Item[metaDataEntry.Value.SPColumnName] = values;
                                                        }
                                                        else
                                                            newFile.Item[metaDataEntry.Value.SPColumnName] = taxonomyFieldValue;
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                }
                                                newFile.Item.Update();


                                                filestatus = "File Found";
                                                if (File.Exists(folderPath + completeFolderName + fileName))
                                                    File.Delete(folderPath + completeFolderName + fileName);
                                                File.Move(fileURL, folderPath + completeFolderName + fileName);
                                            }
                                            File.AppendAllText(outLogPath, string.Format("{0} ( {1} ) {2}\n",
                                                    Convert.ToString(row["Document #"]),
                                                    filestatus,
                                                    overWriteMessage));
                                        }
                                        catch (Exception ex)
                                        {
                                            File.AppendAllText(outLogPath, string.Format("{0} ( Error ) {1}\n",
                                                     Convert.ToString(row["Document #"]),
                                                     ex.Message));
                                        }
                                    }
                                }

                            }

                        }
                        DateTime EndTime = DateTime.Now;
                        TimeSpan span = EndTime - StartTime;
                        double totalMinutes = span.TotalMinutes;
                        outLog.AppendLine("Time Taken (In Minutes) : " + totalMinutes);
                        File.AppendAllText(outLogPath, "Time Taken (In Minutes) : " + totalMinutes);
                        /* DateTime EndTime = DateTime.Now;
                         TimeSpan span = EndTime - StartTime;
                         double totalMinutes = span.TotalMinutes;
                         outLog.AppendLine("Time Taken (In Minutes) : " + totalMinutes);
                         File.WriteAllText(outLogPath, outLog.ToString());
                         SPList list = web.Lists.TryGetList("Aconex Import Logs");
                         if (list != null)
                         {
                             list.RootFolder.Files.Add(outLogPath.Substring(outLogPath.LastIndexOf("\\") + 1), File.ReadAllBytes(outLogPath));
                         }*/
                    }
                }
            });
        }

        static void ProcessCSV(string csvFilePath, string outCSVPath)
        {
             
                AsyncMethod(siteURL, csvFilePath, outCSVPath);
        }

        protected void btnSync_Click(object sender, EventArgs e)
        {
            string[] csvFiles = Directory.GetFiles(folderPath, "*.csv");
            if (csvFiles.Length > 0)
            {
                string csvFilePath = csvFiles[0];
                string csvFileName = csvFilePath.Substring(csvFilePath.LastIndexOf("\\") + 1);
                string outCSVFileName = csvFileName.Substring(0, csvFileName.LastIndexOf(".")) + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";
                string outLogFileName = csvFileName.Substring(0, csvFileName.LastIndexOf(".")) + DateTime.Now.ToString("yyyyMMddhhmmss") + ".log";
                if (!Directory.Exists(folderPath + completeFolderName))
                {
                    Directory.CreateDirectory(folderPath + completeFolderName);
                }
                if (!Directory.Exists(folderPath + logFolderName))
                {
                    Directory.CreateDirectory(folderPath + logFolderName);
                }
                outLogPath = folderPath + logFolderName + outLogFileName;
                ProcessCSV(csvFilePath, folderPath + completeFolderName + outCSVFileName);
            }
        }
    }
}
