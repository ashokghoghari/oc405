﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Solutions.Emailer.SSOMServiceSSOMService
{
    public class SSOMService
    {
        private static SPSite GetProxy()
        {
            return new SPSite("https://sharepoint.oc405.com/planspecs/rfis");
        }

        //public static void GetListDate()
        //{
        //    using (var site = new SPSite("https://sharepoint.oc405.com/planspecs/rfis"))
        //    {
        //        var web = site.RootWeb;
        //        Console.WriteLine(web.Title);
        //        var lists = web.Lists;

        //        foreach (SPList list in lists)
        //        {
        //            Console.WriteLine("\t" + list.Title);
        //        }
        //    }
        //}

        public static SPListItemCollection GetListData(string listName, string caml)
        {
            using (var site = GetProxy())
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList list = web.Lists[listName] as SPList;
                    if (null != list)
                    {
                        SPQuery qry = new SPQuery();
                        qry.Query = caml;// "<Query><OrderBy><FieldRef Name='FirstName' /></OrderBy><Query>";
                        //qry.ViewAttributes = "<FieldRef Name='FirstName' />";
                        SPListItemCollection items = list.GetItems(qry);
                        return items;
                    }
                }
            }
            return null;
        }

        public static SPGroup GetGroup(string groupName)
        {
            using (SPSite site = GetProxy())
            {
                using (SPWeb web = site.OpenWeb())
                {
                    return web.Groups[groupName];                    
                }
            }
        }
    }
}
