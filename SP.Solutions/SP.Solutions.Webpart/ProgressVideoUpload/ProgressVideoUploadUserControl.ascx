﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgressVideoUploadUserControl.ascx.cs" Inherits="SP.Solutions.Webpart.ProgressVideoUpload.ProgressVideoUploadUserControl" %>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div>
    <div class="col-sm-6">
        <div class="row form-group">
            <div class="col-sm-4 form-Label">Title : </div>
            <div class="col-sm-8">
                <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4 form-Label">Category : </div>
            <div class="col-sm-8">
                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4 form-Label">Location : </div>
            <div class="col-sm-8">
                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control">
                    <asp:ListItem Value="NB – NB 405">NB – NB 405</asp:ListItem>
                    <asp:ListItem Value="SB – SB 405">SB – SB 405</asp:ListItem>
                    <asp:ListItem Value="73N – 73 NB">73N – 73 NB</asp:ListItem>
                    <asp:ListItem Value="73S – 73 SB">73S – 73 SB</asp:ListItem>
                    <asp:ListItem Value="605N – 605 NB">605N – 605 NB</asp:ListItem>
                    <asp:ListItem Value="605S – 605 SB">605S – 605 SB</asp:ListItem>
                    <asp:ListItem Value="22E – 22 EB">22E – 22 EB</asp:ListItem>
                    <asp:ListItem Value="22W – 22 WB">22W – 22 WB</asp:ListItem>
                    <asp:ListItem Value="SB – Seal Beach Blvd">SB – Seal Beach Blvd</asp:ListItem>
                    <asp:ListItem Value="BC – Bolsa Chica Rd">BC – Bolsa Chica Rd</asp:ListItem>
                    <asp:ListItem Value="SD – Sprindgdale St">SD – Sprindgdale St</asp:ListItem>
                    <asp:ListItem Value="WM – Westminster Blvd">WM – Westminster Blvd</asp:ListItem>
                    <asp:ListItem Value="EW – Edwards St">EW – Edwards St</asp:ListItem>
                    <asp:ListItem Value="GW – Goldenwest St">GW – Goldenwest St</asp:ListItem>
                    <asp:ListItem Value="BS – Bolsa Ave">BS – Bolsa Ave</asp:ListItem>
                    <asp:ListItem Value="MF – Mcfadden Ave">MF – Mcfadden Ave</asp:ListItem>
                    <asp:ListItem Value="B – Beach Blvd">B – Beach Blvd</asp:ListItem>
                    <asp:ListItem Value="ED – Edinger Ave">ED – Edinger Ave</asp:ListItem>
                    <asp:ListItem Value="NL – Newland St">NL – Newland St</asp:ListItem>
                    <asp:ListItem Value="HL – Heil Ped OC">HL – Heil Ped OC</asp:ListItem>
                    <asp:ListItem Value="M – Magnolia St">M – Magnolia St</asp:ListItem>
                    <asp:ListItem Value="WN – Warner Ave">WN – Warner Ave</asp:ListItem>
                    <asp:ListItem Value="BD – Bushard St">BD – Bushard St</asp:ListItem>
                    <asp:ListItem Value="S – Slater Ave">S – Slater Ave</asp:ListItem>
                    <asp:ListItem Value="BH – Brookhurst St">BH – Brookhurst St</asp:ListItem>
                    <asp:ListItem Value="T – Talbert Ave">T – Talbert Ave</asp:ListItem>
                    <asp:ListItem Value="W – Ward St">W – Ward St</asp:ListItem>
                    <asp:ListItem Value="EC – Euclid St">EC – Euclid St</asp:ListItem>
                    <asp:ListItem Value="HB – Harbor Blvd">HB – Harbor Blvd</asp:ListItem>
                    <asp:ListItem Value="FV – Fairview St">FV – Fairview St</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4 form-Label">Browse File(s) : </div>
            <div class="col-sm-8">
                <asp:FileUpload ID="brwsFile" runat="server" AllowMultiple="true" CssClass="form-control" />
            </div>
        </div>
        <div>
            <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn btn-default pull-right" />
            <asp:Button runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn btn-default pull-right" />     
        </div>
        <div>
            <asp:Label ID="lblMessage" runat="server" ForeColor="Green"></asp:Label>
        </div>
    </div>
</div>