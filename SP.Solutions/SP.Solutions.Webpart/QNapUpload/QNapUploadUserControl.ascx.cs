﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace SP.Solutions.Webpart.QNapUpload
{
    public partial class QNapUploadUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {                
                SPSecurity.RunWithElevatedPrivileges(delegate
                {
                    if (brwsFile.HasFile)
                    {
                        string title = txtTitle.Text;
                        string category = txtCategory.Text;
                        List<string> files = new List<string>();

                        SPSite site = SPContext.Current.Site;
                        SPWeb web = site.OpenWeb();
                        SPList oList = web.Lists["QNapVideo"];
                        SPListItem oSPListItem = oList.Items.Add();
                        oSPListItem["Title"] = title;
                        oSPListItem["Category"] = category;
                        oSPListItem["Location"] = ddlLocation.SelectedItem.Value;
                        oSPListItem.Update();
                        Impersonate.ImpersonateUser("oc405", "patrick.park", "OC405pat");
                        foreach (HttpPostedFile uploadedFile in brwsFile.PostedFiles)
                        {
                            string dir = @"\\nas1\media\QNAP\" + oSPListItem.ID.ToString();
                            
                            Directory.CreateDirectory(dir);
                            uploadedFile.SaveAs(dir + "\\" + new FileInfo(uploadedFile.FileName).Name);
                            files.Add(uploadedFile.FileName);
                            
                            
                        }
                        Impersonate.UndoImpersonation();
                        oSPListItem["Files"] = string.Join("|", files);
                        oSPListItem.Update();

                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Video uploaded successfully.";
                        txtTitle.Text = txtCategory.Text = string.Empty;
                        Response.Redirect("https://sharepoint.oc405.com/quality/mot/Pages/MOTVideos.aspx");
                    }
                });
            }
            catch (Exception ex)
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = ex.Message + "<br><br>" + ex.StackTrace;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://sharepoint.oc405.com/quality/mot/Pages/MOTVideos.aspx");
        }
    }
}
