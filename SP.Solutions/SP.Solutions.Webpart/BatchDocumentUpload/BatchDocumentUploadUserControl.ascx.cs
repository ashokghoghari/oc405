﻿using Microsoft.Office.DocumentManagement.DocumentSets;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI;

namespace SP.Solutions.Webpart.BatchDocumentUpload
{
    enum SPColumnTypes
    {
        Date,
        ManagedMetaData,
        String
    }

    class SPColumnInfo
    {
        public SPColumnInfo()
        {
            SPColumnType = SPColumnTypes.String;
        }

        public string SPColumnName { get; set; }

        public SPColumnTypes SPColumnType { get; set; }

        public string TermStoreGroupName { get; set; }

        public bool IsAlloedMultipleTermValues { get; set; }
    }

    public partial class BatchDocumentUploadUserControl : UserControl
    {
        static Dictionary<string, SPColumnInfo> _mappingInfo = null;
        static Group _groups = null;
        public static string folderPath = "C:\\UploadFiles\\";
        public static string completeFolderName = "Completed\\";
        public static string logFolderName = "Logs\\";
        private static bool runAsync = true;
        private static string outLogPath;
        private static string siteURL = "https://sharepoint.oc405.com/DControl/ProjDocs";
        public void ExecuteFromProgram()
        {
            runAsync = false;
            this.Page_Load(null, null);
            this.btnSync_Click(null, null);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _mappingInfo = new Dictionary<string, SPColumnInfo>();
            _mappingInfo.Add("3rd Party", new SPColumnInfo() { SPColumnName = "_x0033_rd_x0020_Party_x0020_Contractor", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Sub Contractors" });
            _mappingInfo.Add("3rd Party Document #", new SPColumnInfo() { SPColumnName = "_x0033_rdPartyDocumentNo" });
            _mappingInfo.Add("AconexReferenceNo", new SPColumnInfo() { SPColumnName = "AconexReferenceNo" });
            _mappingInfo.Add("Buy America", new SPColumnInfo() { SPColumnName = "BuyAmerica" });
            _mappingInfo.Add("Change Order", new SPColumnInfo() { SPColumnName = "ChangeOrder" });
            _mappingInfo.Add("Conflict ID Number", new SPColumnInfo() { SPColumnName = "ConflictIDNumber" });
            _mappingInfo.Add("Contract Section", new SPColumnInfo() { SPColumnName = "ContractSection", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "TP Section" });
            _mappingInfo.Add("Correspondence", new SPColumnInfo() { SPColumnName = "Correspondence", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Letters" });
            _mappingInfo.Add("CT Categories", new SPColumnInfo() { SPColumnName = "CTCategories", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "CT Categories" });
            _mappingInfo.Add("Date Due Back", new SPColumnInfo() { SPColumnName = "DateDueBack", SPColumnType = SPColumnTypes.Date });
            _mappingInfo.Add("Date Received Back", new SPColumnInfo() { SPColumnName = "DateReceivedBack", SPColumnType = SPColumnTypes.Date });
            _mappingInfo.Add("Date Sent", new SPColumnInfo() { SPColumnName = "DateSent", SPColumnType = SPColumnTypes.Date });
            _mappingInfo.Add("DBE", new SPColumnInfo() { SPColumnName = "DBE" });
            _mappingInfo.Add("Departments", new SPColumnInfo() { SPColumnName = "Departments", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Departments" });
            _mappingInfo.Add("Deviation #", new SPColumnInfo() { SPColumnName = "DeviationNo" });
            _mappingInfo.Add("Directive Letter #", new SPColumnInfo() { SPColumnName = "DirectiveLetterNo" });
            _mappingInfo.Add("Discipline", new SPColumnInfo() { SPColumnName = "Discipline", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Discipline", IsAlloedMultipleTermValues = true });
            _mappingInfo.Add("Doc Type", new SPColumnInfo() { SPColumnName = "DocType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Document Type" });
            _mappingInfo.Add("DocumentAttribute", new SPColumnInfo() { SPColumnName = "DocumentAttribute", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "DocumentAttribute" });
            _mappingInfo.Add("Document #", new SPColumnInfo() { SPColumnName = "DocumentNo" });
            _mappingInfo.Add("DocumentSet", new SPColumnInfo() { SPColumnName = "DocumentSet" });
            _mappingInfo.Add("DocumentSetLabel", new SPColumnInfo() { SPColumnName = "DocumentSetLabel" });
            _mappingInfo.Add("Document Set Type", new SPColumnInfo() { SPColumnName = "DocumentSetType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Document Set Type" });
            _mappingInfo.Add("Document Status", new SPColumnInfo() { SPColumnName = "DocumentStatus", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Document Status" });
            _mappingInfo.Add("Document Title", new SPColumnInfo() { SPColumnName = "DocumentTitle" });
            _mappingInfo.Add("Durations", new SPColumnInfo() { SPColumnName = "Durations" });
            _mappingInfo.Add("ECR", new SPColumnInfo() { SPColumnName = "ECR", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "ECR" });
            _mappingInfo.Add("Inspector Name", new SPColumnInfo() { SPColumnName = "InspectorName", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Inspector Name" });
            _mappingInfo.Add("Material Type", new SPColumnInfo() { SPColumnName = "MaterialType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Material Type" });
            _mappingInfo.Add("Material Testing", new SPColumnInfo() { SPColumnName = "MaterialTesting", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Material Testing" });
            _mappingInfo.Add("NCR Disposition", new SPColumnInfo() { SPColumnName = "NCRDisposition", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "NCR Disposition" });
            _mappingInfo.Add("NCR #", new SPColumnInfo() { SPColumnName = "NCRNo" });
            _mappingInfo.Add("NCR Type", new SPColumnInfo() { SPColumnName = "NCRType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "NCR Type" });
            _mappingInfo.Add("NDC #", new SPColumnInfo() { SPColumnName = "NDCNo" });
            _mappingInfo.Add("Permissions", new SPColumnInfo() { SPColumnName = "Permissions", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Permissions" });
            _mappingInfo.Add("Permits", new SPColumnInfo() { SPColumnName = "Permits" });
            _mappingInfo.Add("Permit Type", new SPColumnInfo() { SPColumnName = "PermitType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Permit Type" });
            _mappingInfo.Add("Potential Change Order", new SPColumnInfo() { SPColumnName = "PotentialChangeOrder" });
            _mappingInfo.Add("Quality Doc #", new SPColumnInfo() { SPColumnName = "QualityDocNo" });
            _mappingInfo.Add("Quality Docs Type", new SPColumnInfo() { SPColumnName = "QualityDocsType", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Quality Document" });
            _mappingInfo.Add("Responsible Person", new SPColumnInfo() { SPColumnName = "ResponsiblePerson" });
            _mappingInfo.Add("Revision #", new SPColumnInfo() { SPColumnName = "RevisionNo" });
            _mappingInfo.Add("RFI #", new SPColumnInfo() { SPColumnName = "RFINo" });
            _mappingInfo.Add("RFI Response", new SPColumnInfo() { SPColumnName = "RFIResponse" });
            _mappingInfo.Add("Safety", new SPColumnInfo() { SPColumnName = "Safety" });
            _mappingInfo.Add("Specification Section", new SPColumnInfo() { SPColumnName = "SpecificationSection", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "CT Specifications" });
            _mappingInfo.Add("Task Order", new SPColumnInfo() { SPColumnName = "TaskOrder" });
            _mappingInfo.Add("TP Section", new SPColumnInfo() { SPColumnName = "TPSection", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "TP Section" });
            _mappingInfo.Add("Utility", new SPColumnInfo() { SPColumnName = "StrUtility"/*, SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "Utility" */});
            _mappingInfo.Add("Unit ID", new SPColumnInfo() { SPColumnName = "UnitID", SPColumnType = SPColumnTypes.ManagedMetaData, TermStoreGroupName = "UNIT ID" });
            if (runAsync)
            {
                siteURL = SPContext.Current.Web.Url;
            }
            using (SPSite spSite = new SPSite(siteURL))
            {
                using (SPWeb web = spSite.OpenWeb())
                {
                    TaxonomySession taxonomySession = new TaxonomySession(spSite);
                    TermStore termStore = taxonomySession.TermStores["Managed Metadata Service"];
                    _groups = termStore.Groups["OC405 Codes"];
                }
            }
        }

        public static DataTable ReadCSV(string csvPath, string outCSVPath, string outLogPath)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[50] {
                new DataColumn("DocumentAttribute", typeof(string)),
                 new DataColumn("Document Set Type", typeof(string)),
                 new DataColumn("Discipline", typeof(string)),
                 new DataColumn("DocumentSet", typeof(string)),
                 new DataColumn("DocumentSetLabel", typeof(string)),
                 new DataColumn("AconexReferenceNo", typeof(string)),
                 new DataColumn("Document #", typeof(string)),
                 new DataColumn("Document Title", typeof(string)),
                 new DataColumn("Doc Type", typeof(string)),
                 new DataColumn("Revision #", typeof(string)),
                 new DataColumn("Durations", typeof(string)),
                 new DataColumn("Date Sent", typeof(string)),
                 new DataColumn("Date Due Back", typeof(string)),
                 new DataColumn("Date Received Back", typeof(string)),
                 new DataColumn("Document Status", typeof(string)),
                 new DataColumn("Correspondence", typeof(string)),
                 new DataColumn("Contract Section", typeof(string)),
                 new DataColumn("TP Section", typeof(string)),
                 new DataColumn("Change Order", typeof(string)),
                 new DataColumn("Deviation #", typeof(string)),
                 new DataColumn("Potential Change Order", typeof(string)),
                 new DataColumn("Directive Letter #", typeof(string)),
                 new DataColumn("Task Order", typeof(string)),
                 new DataColumn("DBE", typeof(string)),
                 new DataColumn("Utility", typeof(string)),
                 new DataColumn("Conflict ID Number", typeof(string)),
                 new DataColumn("Unit ID", typeof(string)),
                 new DataColumn("Departments", typeof(string)),
                 new DataColumn("Responsible Person", typeof(string)),
                 new DataColumn("Inspector Name", typeof(string)),
                 new DataColumn("Specification Section", typeof(string)),
                 new DataColumn("3rd Party Document #", typeof(string)),
                 new DataColumn("3rd Party", typeof(string)),
                 new DataColumn("CT Categories", typeof(string)),
                 new DataColumn("ECR", typeof(string)),
                 new DataColumn("Material Type", typeof(string)),
                 new DataColumn("Material Testing", typeof(string)),
                 new DataColumn("Buy America", typeof(string)),
                 new DataColumn("Permits", typeof(string)),
                 new DataColumn("Permit Type", typeof(string)),
                 new DataColumn("Quality Docs Type", typeof(string)),
                 new DataColumn("Quality Doc #", typeof(string)),
                 new DataColumn("Safety", typeof(string)),
                 new DataColumn("RFI #", typeof(string)),
                 new DataColumn("RFI Response", typeof(string)),
                 new DataColumn("NCR #", typeof(string)),
                 new DataColumn("NCR Disposition", typeof(string)),
                 new DataColumn("NCR Type", typeof(string)),
                 new DataColumn("NDC #", typeof(string)),
                 new DataColumn("Permissions", typeof(string))
            });

            //Read the contents of CSV file.  
            string csvData = File.ReadAllText(csvPath);

            //Execute a loop over the rows.  
            string[] rows = csvData.Split('\n');
            bool isHeader = true;
            foreach (string row in rows)
            {
                if (!string.IsNullOrEmpty(row) && !isHeader)
                {
                    dt.Rows.Add();
                    int i = 0;

                    //Execute a loop over the columns.  
                    foreach (string cell in row.Split(','))
                    {
                        if (i < dt.Columns.Count)
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    isHeader = false;
                }
            }
            //File.Move(csvPath, outCSVPath);
            return dt;
        }

        static SPFolder VerifyDocumentSetOld(SPWeb web, DataRow row, string outLogPath)
        {
            string documentSetName = Convert.ToString(row["DocumentSetLabel"]);
            File.AppendAllText(outLogPath, string.Format("===================================================={0}==========================================================", documentSetName));
            SPFolder folder = null;
            try
            {
                SPList sharedDocs = web.Lists["Project Documents"];
                if (sharedDocs != null)
                {

                    SPQuery objQuery = new SPQuery();
                    objQuery.Query = @"<Where><And>
                                <Eq><FieldRef Name='FSObjType'/><Value Type='Integer'>1</Value></Eq>
                                <Eq><FieldRef Name='FileLeafRef' /><Value Type='Text'>" + documentSetName + "</Value></Eq></And></Where>";
                    SPListItemCollection lst = sharedDocs.GetItems(objQuery);
                    if (lst.Count > 0)
                    {
                        folder = lst[0].Folder;
                    }
                    else
                    {
                        SPContentType docSetCT = sharedDocs.ContentTypes["Master Document Set"];
                        if (docSetCT != null)
                        {
                            Hashtable props = GetColumnProps(row, out Dictionary<string, SPColumnInfo> propsManagedData);
                            DocumentSet docSet = DocumentSet.Create(sharedDocs.RootFolder, documentSetName, docSetCT.Id, props, true);
                            DocumentSet MyDocSet = DocumentSet.GetDocumentSet(sharedDocs.GetItemById(docSet.Item.ID).Folder);
                            foreach (var metaDataEntry in propsManagedData.Where(x => row[x.Key] != null && !string.IsNullOrEmpty(row[x.Key].ToString())))
                            {
                                try
                                {
                                    TermSet termSet = _groups.TermSets[metaDataEntry.Value.TermStoreGroupName];
                                    Term term = termSet.Terms[Convert.ToString(row[metaDataEntry.Key]).Trim()];

                                    //DocumentLibrary.Fields
                                    TaxonomyField taxonomyField = MyDocSet.ParentList.Fields[metaDataEntry.Value.SPColumnName] as TaxonomyField;
                                    TaxonomyFieldValue taxonomyFieldValue = new TaxonomyFieldValue(taxonomyField);
                                    taxonomyFieldValue.TermGuid = term.Id.ToString();
                                    taxonomyFieldValue.Label = term.Name;

                                    if (metaDataEntry.Value.IsAlloedMultipleTermValues)
                                    {
                                        TaxonomyFieldValueCollection values = new TaxonomyFieldValueCollection(taxonomyField);
                                        values.Add(taxonomyFieldValue);
                                        MyDocSet.Item[metaDataEntry.Value.SPColumnName] = values;
                                    }
                                    else
                                        MyDocSet.Item[metaDataEntry.Value.SPColumnName] = taxonomyFieldValue;
                                }
                                catch (Exception ex)
                                {

                                }

                            }
                            MyDocSet.Item.Update();
                            folder = MyDocSet.Folder;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(outLogPath, string.Format("Not able to create DocumentSet : {0}", documentSetName));
                File.AppendAllText(outLogPath, ex.Message);
                File.AppendAllText(outLogPath, ex.StackTrace);

            }
            return folder;
        }

        private static void Updateprops(DataRow row, SPListItem item)
        {

            foreach (KeyValuePair<string, SPColumnInfo> entry in _mappingInfo)
            {
                switch (entry.Value.SPColumnType)
                {
                    case SPColumnTypes.Date:
                        if (row[entry.Key] != null && !string.IsNullOrEmpty(row[entry.Key].ToString()))
                        {
                            try
                            {
                                //DateTime dt;
                                bool success = DateTime.TryParse(row[entry.Key].ToString(), out DateTime dt);
                                if (success)
                                    item[entry.Value.SPColumnName] = dt;
                            }
                            catch (Exception ex)
                            { }

                        }
                        break;
                    case SPColumnTypes.ManagedMetaData:
                        break;
                    case SPColumnTypes.String:
                    default:
                        item[entry.Value.SPColumnName] = Convert.ToString(row[entry.Key]);
                        break;
                }

            }
        }

        static SPFolder VerifyDocumentSet(SPWeb web, DataRow row)
        {
            SPFolder folder = null;
            SPList sharedDocs = web.Lists["Project Documents"];
            if (sharedDocs != null)
            {
                folder = sharedDocs.RootFolder;
            }
            return folder;
        }

        private static Hashtable GetColumnProps(DataRow row, out Dictionary<string, SPColumnInfo> propsManagedMetaData)
        {
            Hashtable props = new Hashtable();
            propsManagedMetaData = new Dictionary<string, SPColumnInfo>();

            foreach (KeyValuePair<string, SPColumnInfo> entry in _mappingInfo)
            {
                switch (entry.Value.SPColumnType)
                {
                    case SPColumnTypes.Date:
                        if (row[entry.Key] != null && !string.IsNullOrEmpty(row[entry.Key].ToString()))
                        {
                            try
                            {
                                //DateTime dt;
                                bool success = DateTime.TryParse(row[entry.Key].ToString(), out DateTime dt);
                                if (success)
                                    props.Add(entry.Value.SPColumnName, dt);
                            }
                            catch (Exception ex)
                            { }

                        }
                        break;
                    case SPColumnTypes.ManagedMetaData:
                        propsManagedMetaData.Add(entry.Key, entry.Value);

                        /*if (row[entry.Key] != null && !string.IsNullOrEmpty(row[entry.Key].ToString()))
                        {
                            TermSet termSet = _groups.TermSets[entry.Value.TermStoreGroupName];
                            Term term = termSet.Terms[Convert.ToString(row[entry.Key])];
                            TaxonomyFieldValue taxonomyFieldValue = new TaxonomyFieldValue(term.Id.ToString());
                            taxonomyFieldValue.TermGuid = term.Id.ToString();
                            taxonomyFieldValue.Label = term.Name;

                            props.Add(entry.Value.SPColumnName, Convert.ToString(row[entry.Key]));
                        }*/
                        break;
                    case SPColumnTypes.String:
                    default:
                        props.Add(entry.Value.SPColumnName, Convert.ToString(row[entry.Key]));
                        break;
                }

            }

            return props;
        }

        static void AsyncMethod(string spSiteURL, string csvFilePath, string outCSVPath, string outLogPath)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                DateTime StartTime = DateTime.Now;
                using (SPSite spSite = new SPSite(spSiteURL))
                {
                    using (SPWeb web = spSite.OpenWeb())
                    {
                        var outLog = new StringBuilder();
                        DataTable dt = ReadCSV(csvFilePath, outCSVPath, outCSVPath);
                        var grouped = from table in dt.AsEnumerable()
                                      group table by new { documentSetName = table["DocumentSet"] } into groupby
                                      select new
                                      {
                                          Value = groupby.Key,
                                          ColumnValues = groupby
                                      };
                        foreach (var key in grouped)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(key.Value.documentSetName)))
                            {
                                SPFolder docSetFolder = null;
                                if (key.ColumnValues.Count() > 0)
                                {
                                    //docSetFolder = VerifyDocumentSet(web, key.ColumnValues.FirstOrDefault());
                                    docSetFolder = VerifyDocumentSetOld(web, key.ColumnValues.FirstOrDefault(), outLogPath);
                                }
                                if (docSetFolder != null)
                                {
                                    foreach (DataRow row in key.ColumnValues)
                                    {
                                        string filestatus = "File Not Found";
                                        string msg = "Success";
                                        string overWriteMessage = string.Empty;
                                        try
                                        {
                                            string[] dirs = Directory.GetFiles(folderPath, row["Document #"] + "*");
                                            if (dirs.Length > 0)
                                            {
                                                string fileURL = dirs[0];
                                                string fileName = fileURL.Substring(fileURL.LastIndexOf("\\") + 1);

                                                Hashtable props = GetColumnProps(row, out Dictionary<string, SPColumnInfo> propsManagedData);
                                                SPFile newFile = null;

                                                try
                                                {
                                                    newFile = docSetFolder.Files.Add(fileName, File.ReadAllBytes(fileURL), props, false);
                                                }
                                                catch (Exception ex)
                                                {
                                                    newFile = docSetFolder.Files.Add(fileName, File.ReadAllBytes(fileURL), props, true);
                                                    if (newFile != null)
                                                    {
                                                        overWriteMessage = "\n File is overWritten...";
                                                    }
                                                }


                                                foreach (var metaDataEntry in propsManagedData.Where(x => row[x.Key] != null && !string.IsNullOrEmpty(row[x.Key].ToString())))
                                                {
                                                    try
                                                    {
                                                        TermSet termSet = _groups.TermSets[metaDataEntry.Value.TermStoreGroupName];
                                                        Term term = termSet.Terms[Convert.ToString(row[metaDataEntry.Key]).Trim()];

                                                        TaxonomyField taxonomyField = newFile.DocumentLibrary.Fields[metaDataEntry.Value.SPColumnName] as TaxonomyField;
                                                        TaxonomyFieldValue taxonomyFieldValue = new TaxonomyFieldValue(taxonomyField);
                                                        taxonomyFieldValue.TermGuid = term.Id.ToString();
                                                        taxonomyFieldValue.Label = term.Name;

                                                        if (metaDataEntry.Value.IsAlloedMultipleTermValues)
                                                        {
                                                            TaxonomyFieldValueCollection values = new TaxonomyFieldValueCollection(taxonomyField);
                                                            values.Add(taxonomyFieldValue);
                                                            newFile.Item[metaDataEntry.Value.SPColumnName] = values;
                                                        }
                                                        else
                                                            newFile.Item[metaDataEntry.Value.SPColumnName] = taxonomyFieldValue;
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                }
                                                newFile.Item.Update();


                                                filestatus = "File Found";
                                                if (File.Exists(folderPath + completeFolderName + fileName))
                                                    File.Delete(folderPath + completeFolderName + fileName);
                                                File.Move(fileURL, folderPath + completeFolderName + fileName);
                                            }
                                            File.AppendAllText(outLogPath, string.Format("{0} ( {1} ) {2}\n",
                                                    Convert.ToString(row["Document #"]),
                                                    filestatus,
                                                    overWriteMessage));
                                        }
                                        catch (Exception ex)
                                        {
                                            File.AppendAllText(outLogPath, string.Format("{0} ( Error ) {1}\n",
                                                     Convert.ToString(row["Document #"]),
                                                     ex.Message));
                                        }
                                    }
                                }

                            }

                        }
                        DateTime EndTime = DateTime.Now;
                        TimeSpan span = EndTime - StartTime;
                        double totalMinutes = span.TotalMinutes;
                        outLog.AppendLine("Time Taken (In Minutes) : " + totalMinutes);
                        File.AppendAllText(outLogPath, "Time Taken (In Minutes) : " + totalMinutes);
                        /* DateTime EndTime = DateTime.Now;
                         TimeSpan span = EndTime - StartTime;
                         double totalMinutes = span.TotalMinutes;
                         outLog.AppendLine("Time Taken (In Minutes) : " + totalMinutes);
                         File.WriteAllText(outLogPath, outLog.ToString());
                         SPList list = web.Lists.TryGetList("Aconex Import Logs");
                         if (list != null)
                         {
                             list.RootFolder.Files.Add(outLogPath.Substring(outLogPath.LastIndexOf("\\") + 1), File.ReadAllBytes(outLogPath));
                         }*/
                    }
                }
            });
        }

        static void ProcessCSV(string csvFilePath, string outCSVPath, string outLogPath)
        {
            string siteURL = SPContext.Current.Web.Url;
            if (runAsync)
            {
                Task.Run(() =>
                {
                    AsyncMethod(siteURL, csvFilePath, outCSVPath, outLogPath);
                }).ConfigureAwait(false);
            }
            else
            {
                siteURL = "https://sharepoint.oc405.com/DControl/ProjDocs";
                AsyncMethod(siteURL, csvFilePath, outCSVPath, outLogPath);
            }
        }

        protected void btnSync_Click(object sender, EventArgs e)
        {
            string[] csvFiles = Directory.GetFiles(folderPath, "*.csv");
            if (csvFiles.Length > 0)
            {
                string csvFilePath = csvFiles[0];
                string csvFileName = csvFilePath.Substring(csvFilePath.LastIndexOf("\\") + 1);
                string outCSVFileName = csvFileName.Substring(0, csvFileName.LastIndexOf(".")) + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";
                string outLogFileName = csvFileName.Substring(0, csvFileName.LastIndexOf(".")) + DateTime.Now.ToString("yyyyMMddhhmmss") + ".log";
                if (!Directory.Exists(folderPath + completeFolderName))
                {
                    Directory.CreateDirectory(folderPath + completeFolderName);
                }
                if (!Directory.Exists(folderPath + logFolderName))
                {
                    Directory.CreateDirectory(folderPath + logFolderName);
                }
                ProcessCSV(csvFilePath, folderPath + completeFolderName + outCSVFileName, folderPath + logFolderName + outLogFileName);
            }
        }

        //static Dictionary<string, string> _mappingInfo = null;
        //public static string folderPath = "C:\\UploadFiles\\";
        //public static string completeFolderName = "Completed\\";
        //public static string logFolderName = "Logs\\";

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    _mappingInfo = new Dictionary<string, string>();
        //    //_mappingInfo.Add("FileColumn", "SPColumn");
        //    _mappingInfo.Add("DocumentAttribute", "DocumentAttribute");
        //    _mappingInfo.Add("Document Set Type", "DocumentSetType0");
        //    _mappingInfo.Add("Discipline", "Disipline");
        //    _mappingInfo.Add("DocumentSet", "DocumentSet");
        //    _mappingInfo.Add("DocumentSetLabel", "DocumentSetLabel");
        //    _mappingInfo.Add("AconexReferenceNo", "AconexReferenceNo");
        //    _mappingInfo.Add("Document #", "DocumentNo");
        //    _mappingInfo.Add("Document Title", "DocumentTitle");
        //    _mappingInfo.Add("Doc Type", "DocType");
        //    _mappingInfo.Add("Revision #", "RevisionNo");
        //    _mappingInfo.Add("Durations", "Durations");
        //    _mappingInfo.Add("Date Sent", "DateSent");
        //    _mappingInfo.Add("Date Due Back", "DateDueBack");
        //    _mappingInfo.Add("Date Received Back", "DateReceivedBack");
        //    _mappingInfo.Add("Document Status", "DocumentStatus");
        //    _mappingInfo.Add("Correspondence", "Correspondence");
        //    _mappingInfo.Add("Contract Section", "ContractSection");
        //    _mappingInfo.Add("TP Section", "TPSection");
        //    _mappingInfo.Add("Change Order", "ChangeOrder");
        //    _mappingInfo.Add("Deviation #", "DeviationNo");
        //    _mappingInfo.Add("Potential Change Order", "PotentialChangeOrder");
        //    _mappingInfo.Add("Directive Letter #", "DirectiveLetterNo");
        //    _mappingInfo.Add("Task Order", "TaskOrder");
        //    _mappingInfo.Add("DBE", "DBE");
        //    _mappingInfo.Add("Utility", "Utility");
        //    _mappingInfo.Add("Conflict ID Number", "ConflictIDNumber");
        //    _mappingInfo.Add("Unit ID", "UnitID");
        //    _mappingInfo.Add("Departments", "Departments");
        //    _mappingInfo.Add("Responsible Person", "ResponsiblePerson");
        //    _mappingInfo.Add("Inspector Name", "InspectorName");
        //    _mappingInfo.Add("Specification Section", "SpecificationSection");
        //    _mappingInfo.Add("3rd Party Document #", "3rdPartyDocumentNo");
        //    _mappingInfo.Add("3rd Party", "3rdParty");
        //    _mappingInfo.Add("CT Categories", "CTCategories");
        //    _mappingInfo.Add("ECR", "ECR");
        //    _mappingInfo.Add("Material Type", "MaterialType");
        //    _mappingInfo.Add("Material Testing", "MaterialTesting");
        //    _mappingInfo.Add("Buy America", "BuyAmerica");
        //    _mappingInfo.Add("Permits", "Permits");
        //    _mappingInfo.Add("Permit Type", "PermitType");
        //    _mappingInfo.Add("Quality Docs Type", "QualityDocsType");
        //    _mappingInfo.Add("Quality Doc #", "QualityDocNo");
        //    _mappingInfo.Add("Safety", "Safety");
        //    _mappingInfo.Add("RFI #", "RFINo");
        //    _mappingInfo.Add("RFI Response", "RFIResponse");
        //    _mappingInfo.Add("NCR #", "NCRNo");
        //    _mappingInfo.Add("NCR Disposition", "NCRDisposition");
        //    _mappingInfo.Add("NCR Type", "NCRType");
        //    _mappingInfo.Add("NDC #", "NDCNo");
        //    _mappingInfo.Add("Permissions", "Permissions");
        //}

        //public static DataTable ReadCSV(string csvPath, string outCSVPath)
        //{
        //    DataTable dt = new DataTable();
        //    /*
        //    dt.Columns.AddRange(new DataColumn[50] {
        //        new DataColumn("DocumentAttribute", typeof(string)),
        //    new DataColumn("DocumentSetType", typeof(string)),
        //    new DataColumn("Discipline", typeof(string)),
        //    new DataColumn("DocumentSet", typeof(string)),
        //    new DataColumn("DocumentSetLabel", typeof(string)),
        //    new DataColumn("AconexReferenceNo", typeof(string)),
        //    new DataColumn("DocumentNo", typeof(string)),
        //    new DataColumn("DocumentTitle", typeof(string)),
        //    new DataColumn("DocType", typeof(string)),
        //    new DataColumn("RevisionNo", typeof(string)),
        //    new DataColumn("Durations", typeof(string)),
        //    new DataColumn("DateSent", typeof(string)),
        //    new DataColumn("DateDueBack", typeof(string)),
        //    new DataColumn("DateReceivedBack", typeof(string)),
        //    new DataColumn("DocumentStatus", typeof(string)),
        //    new DataColumn("Correspondence", typeof(string)),
        //    new DataColumn("ContractSection", typeof(string)),
        //    new DataColumn("TPSection", typeof(string)),
        //    new DataColumn("ChangeOrder", typeof(string)),
        //    new DataColumn("DeviationNo", typeof(string)),
        //    new DataColumn("PotentialChangeOrder", typeof(string)),
        //    new DataColumn("DirectiveLetterNo", typeof(string)),
        //    new DataColumn("TaskOrder", typeof(string)),
        //    new DataColumn("DBE", typeof(string)),
        //    new DataColumn("Utility", typeof(string)),
        //    new DataColumn("ConflictIDNumber", typeof(string)),
        //    new DataColumn("UnitID", typeof(string)),
        //    new DataColumn("Departments", typeof(string)),
        //    new DataColumn("ResponsiblePerson", typeof(string)),
        //    new DataColumn("InspectorName", typeof(string)),
        //    new DataColumn("SpecificationSection", typeof(string)),
        //    new DataColumn("3rdPartyDocumentNo", typeof(string)),
        //    new DataColumn("3rdParty", typeof(string)),
        //    new DataColumn("CTCategories", typeof(string)),
        //    new DataColumn("ECR", typeof(string)),
        //    new DataColumn("MaterialType", typeof(string)),
        //    new DataColumn("MaterialTesting", typeof(string)),
        //    new DataColumn("BuyAmerica", typeof(string)),
        //    new DataColumn("Permits", typeof(string)),
        //    new DataColumn("PermitType", typeof(string)),
        //    new DataColumn("QualityDocsType", typeof(string)),
        //    new DataColumn("QualityDocNo", typeof(string)),
        //    new DataColumn("Safety", typeof(string)),
        //    new DataColumn("RFINo", typeof(string)),
        //    new DataColumn("RFIResponse", typeof(string)),
        //    new DataColumn("NCRNo", typeof(string)),
        //    new DataColumn("NCRDisposition", typeof(string)),
        //    new DataColumn("NCRType", typeof(string)),
        //    new DataColumn("NDNo", typeof(string)),
        //    new DataColumn("Permissions", typeof(string))
        //    });*/


        //    dt.Columns.AddRange(new DataColumn[50] {
        //        new DataColumn("DocumentAttribute", typeof(string)),
        //         new DataColumn("Document Set Type", typeof(string)),
        //         new DataColumn("Discipline", typeof(string)),
        //         new DataColumn("DocumentSet", typeof(string)),
        //         new DataColumn("DocumentSetLabel", typeof(string)),
        //         new DataColumn("AconexReferenceNo", typeof(string)),
        //         new DataColumn("Document #", typeof(string)),
        //         new DataColumn("Document Title", typeof(string)),
        //         new DataColumn("Doc Type", typeof(string)),
        //         new DataColumn("Revision #", typeof(string)),
        //         new DataColumn("Durations", typeof(string)),
        //         new DataColumn("Date Sent", typeof(string)),
        //         new DataColumn("Date Due Back", typeof(string)),
        //         new DataColumn("Date Received Back", typeof(string)),
        //         new DataColumn("Document Status", typeof(string)),
        //         new DataColumn("Correspondence", typeof(string)),
        //         new DataColumn("Contract Section", typeof(string)),
        //         new DataColumn("TP Section", typeof(string)),
        //         new DataColumn("Change Order", typeof(string)),
        //         new DataColumn("Deviation #", typeof(string)),
        //         new DataColumn("Potential Change Order", typeof(string)),
        //         new DataColumn("Directive Letter #", typeof(string)),
        //         new DataColumn("Task Order", typeof(string)),
        //         new DataColumn("DBE", typeof(string)),
        //         new DataColumn("Utility", typeof(string)),
        //         new DataColumn("Conflict ID Number", typeof(string)),
        //         new DataColumn("Unit ID", typeof(string)),
        //         new DataColumn("Departments", typeof(string)),
        //         new DataColumn("Responsible Person", typeof(string)),
        //         new DataColumn("Inspector Name", typeof(string)),
        //         new DataColumn("Specification Section", typeof(string)),
        //         new DataColumn("3rd Party Document #", typeof(string)),
        //         new DataColumn("3rd Party", typeof(string)),
        //         new DataColumn("CT Categories", typeof(string)),
        //         new DataColumn("ECR", typeof(string)),
        //         new DataColumn("Material Type", typeof(string)),
        //         new DataColumn("Material Testing", typeof(string)),
        //         new DataColumn("Buy America", typeof(string)),
        //         new DataColumn("Permits", typeof(string)),
        //         new DataColumn("Permit Type", typeof(string)),
        //         new DataColumn("Quality Docs Type", typeof(string)),
        //         new DataColumn("Quality Doc #", typeof(string)),
        //         new DataColumn("Safety", typeof(string)),
        //         new DataColumn("RFI #", typeof(string)),
        //         new DataColumn("RFI Response", typeof(string)),
        //         new DataColumn("NCR #", typeof(string)),
        //         new DataColumn("NCR Disposition", typeof(string)),
        //         new DataColumn("NCR Type", typeof(string)),
        //         new DataColumn("NDC #", typeof(string)),
        //         new DataColumn("Permissions", typeof(string))
        //    });

        //    //Read the contents of CSV file.  
        //    string csvData = File.ReadAllText(csvPath);

        //    //Execute a loop over the rows.  
        //    string[] rows = csvData.Split('\n');
        //    bool isHeader = true;
        //    foreach (string row in rows)
        //    {
        //        if (!string.IsNullOrEmpty(row) && !isHeader)
        //        {
        //            dt.Rows.Add();
        //            int i = 0;

        //            //Execute a loop over the columns.  
        //            foreach (string cell in row.Split(','))
        //            {
        //                if (i < dt.Columns.Count)
        //                {
        //                    dt.Rows[dt.Rows.Count - 1][i] = cell;
        //                    i++;
        //                }
        //                else
        //                {
        //                    break;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            isHeader = false;
        //        }
        //    }
        //    File.Move(csvPath, outCSVPath);
        //    return dt;
        //}

        //static SPFolder VerifyDocumentSetOld(SPWeb web, DataRow row)
        //{
        //    SPFolder folder = null;
        //    SPList sharedDocs = web.Lists["Project Documents"];
        //    if (sharedDocs != null)
        //    {
        //        string documentSetName = Convert.ToString(row["DocumentSet"]);
        //        SPQuery objQuery = new SPQuery();
        //        objQuery.Query = @"<Where><And>
        //                        <Eq><FieldRef Name='FSObjType'/><Value Type='Integer'>1</Value></Eq>
        //                        <Eq><FieldRef Name='FileLeafRef' /><Value Type='Text'>" + documentSetName + "</Value></Eq></And></Where>";
        //        SPListItemCollection lst = sharedDocs.GetItems(objQuery);
        //        if (lst.Count > 0)
        //        {
        //            folder = lst[0].Folder;
        //        }
        //        else
        //        {
        //            SPContentType docSetCT = sharedDocs.ContentTypes["Master Document Set"];
        //            if (docSetCT != null)
        //            {
        //                Hashtable props = new Hashtable();
        //                foreach (KeyValuePair<string, string> entry in _mappingInfo)
        //                    props.Add(entry.Value, Convert.ToString(row[entry.Key]));
        //                DocumentSet docSet = DocumentSet.Create(sharedDocs.RootFolder, documentSetName, docSetCT.Id, props, true);
        //                folder = docSet.Folder;

        //                #region old code
        //                //string[] strProperties = {
        //                //"Document #|Document_x0020__x0023_",
        //                //"Permits|Permits",
        //                //"Quality Doc #|Quality_x0020__x0020_Doc_x0020__x0023_",
        //                //"Safety|Safety0",
        //                //"Change Order|Change_x0020_Order",
        //                //"Conflict ID Number|Conflict_x0020_ID_x0020_Number",
        //                //"DBE|DBE",
        //                //"Deviation #|Deviation_x0020__x0023_",
        //                //"Directive Letter #|Directive_x0020_Letter_x0020__x0023_",
        //                //"DocumentSet|Document_x0020_Set",
        //                //"DocumentSetLabel|DocumentSetLabel",
        //                //"Durations|Durations",
        //                //"NCR #|NCR_x0020__x0023_",
        //                //"NDC #|NDC",
        //                //"Potential Change Order|Potential_x0020_Change_x0020_Order",
        //                //"Revision #|Revision_x0020__x0023_",
        //                //"RFI #|RFI_x0020__x0023_",
        //                //"RFI Response|RFI_x0020_Response",
        //                //"3rd Party Document #|Subcontractor_x0020_Doc_x0020__x0023_",
        //                //"AconexReferenceNo|Aconex_x0020_Reference_x0020__x0023_",
        //                //"Task Order|Task_x0020_Order",
        //                //"Document Title|Document_x0020_Description",
        //                //"Document Set Type|Document_x0020_Set_x0020_Type"};
        //                //string[] metaProperties = {
        //                //"Contract Section|Contract_x0020_Section",
        //                //"Permissions|Firm_x0020__x0023_",
        //                //"Correspondence|Letters",
        //                //"CT Categories|CT_x0020_Chapter_x0020_5",
        //                //"Discipline|Disipline",
        //                //"Document Set Type|Document_x0020_Set_x0020_Type",
        //                //"Document Status|Document_x0020_Status",
        //                //"DocumentAttribute|DocumentAttribute",
        //                //"ECR|ECR0",
        //                //"Inspector Name|Inspector_x0020_Name",
        //                //"Material Testing|Material_x0020_Testing",
        //                //"Material Type|Material_x0020_Type",
        //                //"NCR Type|NCR_x0020_Type",
        //                //"NCR Disposition|NCR_x0020_Disposition",
        //                //"Permit Type|Permit_x0020_Type",
        //                //"Quality Docs Type|",
        //                //"Specification Section|Specification_x0020_Section",
        //                //"3rd Party|",
        //                //"TP Section|",
        //                //"Unit ID|",
        //                //"Utility|",
        //                //"Doc Type|",
        //                //"Departments|"
        //                //};
        //                //foreach (string propName in strProperties)
        //                //{
        //                //    string[] propNames = propName.Split('|');
        //                //    props.Add(propNames[1], Convert.ToString(row[propNames[0]]));
        //                //}
        //                #endregion
        //            }
        //        }
        //    }
        //    return folder;
        //}

        //static SPFolder VerifyDocumentSet(SPWeb web, DataRow row)
        //{
        //    SPFolder folder = null;
        //    SPList sharedDocs = web.Lists["Project Documents"];
        //    if (sharedDocs != null)
        //    {
        //        folder = sharedDocs.RootFolder;
        //    }
        //    return folder;
        //}

        //static void AsyncMethod(string spSiteURL, string csvFilePath, string outCSVPath, string outLogPath)
        //{
        //    SPSecurity.RunWithElevatedPrivileges(delegate ()
        //    {
        //        DateTime StartTime = DateTime.Now;
        //        using (SPSite spSite = new SPSite(spSiteURL))
        //        {
        //            using (SPWeb web = spSite.OpenWeb())
        //            {
        //                var outLog = new StringBuilder();
        //                DataTable dt = ReadCSV(csvFilePath, outCSVPath);
        //                var grouped = from table in dt.AsEnumerable()
        //                              group table by new { documentSetName = table["DocumentSet"] } into groupby
        //                              select new
        //                              {
        //                                  Value = groupby.Key,
        //                                  ColumnValues = groupby
        //                              };
        //                foreach (var key in grouped)
        //                {
        //                    if (!string.IsNullOrEmpty(Convert.ToString(key.Value.documentSetName)))
        //                    {
        //                        SPFolder docSetFolder = null;
        //                        if (key.ColumnValues.Count() > 0)
        //                        {
        //                            //docSetFolder = VerifyDocumentSet(web, key.ColumnValues.FirstOrDefault());
        //                            docSetFolder = VerifyDocumentSetOld(web, key.ColumnValues.FirstOrDefault());
        //                        }

        //                        foreach (DataRow row in key.ColumnValues)
        //                        {
        //                            string filestatus = "File Not Found";
        //                            string msg = "Success";

        //                            try
        //                            {
        //                                string[] dirs = Directory.GetFiles(folderPath, row["Document #"] + "*");
        //                                if (dirs.Length > 0)
        //                                {
        //                                    string fileURL = dirs[0];
        //                                    string fileName = fileURL.Substring(fileURL.LastIndexOf("\\") + 1);

        //                                    Hashtable props = new Hashtable();
        //                                    foreach (KeyValuePair<string, string> entry in _mappingInfo)
        //                                        props.Add(entry.Value, Convert.ToString(row[entry.Key]));
        //                                    docSetFolder.Files.Add(fileName, File.ReadAllBytes(fileURL), props, true);

        //                                    filestatus = "File Found";
        //                                    if (File.Exists(folderPath + completeFolderName + fileName))
        //                                        File.Delete(folderPath + completeFolderName + fileName);
        //                                    File.Move(fileURL, folderPath + completeFolderName + fileName);

        //                                    #region Old code
        //                                    /*props.Add("DocumentNo", Convert.ToString(row["Document #"]));
        //                                    props.Add("DocumentTitle", Convert.ToString(row["Document Title"]));
        //                                    props.Add("AconexReferenceNo", Convert.ToString(row["AconexReferenceNo"]));
        //                                    props.Add("ChangeOrder", Convert.ToString(row["ChangeOrder"]));
        //                                    props.Add("ConflictIDNo", Convert.ToString(row["ConflictIDNo"]));
        //                                    props.Add("ConsultantDocumentNo", Convert.ToString(row["ConsultantDocumentNo"]));
        //                                    props.Add("ConsultantFirmNo", Convert.ToString(row["ConsultantFirmNo"]));
        //                                    props.Add("ConsultantName", Convert.ToString(row["ConsultantName"]));
        //                                    props.Add("ContractSection", Convert.ToString(row["ContractSection"]));
        //                                    props.Add("Correspondence", Convert.ToString(row["Correspondence"]));
        //                                    props.Add("CTCategories", Convert.ToString(row["CTCategories"]));
        //                                    props.Add("DocumentSet", Convert.ToString(row["DocumentSet"]));
        //                                    props.Add("DocumentSetType", Convert.ToString(row["DocumentSetType"]));
        //                                    props.Add("Discipline", Convert.ToString(row["Discipline"]));
        //                                    props.Add("DocumentStatus", Convert.ToString(row["DocumentStatus"]));
        //                                    props.Add("DocumentNo", Convert.ToString(row["DocumentNo"]));
        //                                    props.Add("DocumentTitle", Convert.ToString(row["DocumentTitle"]));
        //                                    props.Add("DocumentType", Convert.ToString(row["DocumentType"]));
        //                                    props.Add("Revision", Convert.ToString(row["Revision"]));
        //                                    props.Add("SubmittalNo", Convert.ToString(row["SubmittalNo"]));
        //                                    props.Add("PotentialChangeOrder", Convert.ToString(row["PotentialChangeOrder"]));
        //                                    props.Add("DirectiveLetter", Convert.ToString(row["DirectiveLetter"]));
        //                                    props.Add("TaskOrder", Convert.ToString(row["TaskOrder"]));
        //                                    props.Add("Utility", Convert.ToString(row["Utility"]));
        //                                    props.Add("UnitID", Convert.ToString(row["UnitID"]));
        //                                    props.Add("ResponsiblePerson", Convert.ToString(row["ResponsiblePerson"]));
        //                                    props.Add("SpecificationSection", Convert.ToString(row["SpecificationSection"]));
        //                                    props.Add("SubDocumentNo", Convert.ToString(row["SubDocumentNo"]));
        //                                    props.Add("SubFirmNo", Convert.ToString(row["SubFirmNo"]));
        //                                    props.Add("SubName", Convert.ToString(row["SubName"]));
        //                                    props.Add("DBEFirm", Convert.ToString(row["DBEFirm"]));
        //                                    props.Add("ECR", Convert.ToString(row["ECR"]));
        //                                    props.Add("MaterialType", Convert.ToString(row["MaterialType"]));
        //                                    props.Add("Permits", Convert.ToString(row["Permits"]));
        //                                    props.Add("QualityDocsType", Convert.ToString(row["QualityDocsType"]));
        //                                    props.Add("QualityDocNo", Convert.ToString(row["QualityDocNo"]));
        //                                    props.Add("Safety", Convert.ToString(row["Safety"]));
        //                                    props.Add("ReportType", Convert.ToString(row["ReportType"]));
        //                                    props.Add("RFI", Convert.ToString(row["RFI"]));*/
        //                                    #endregion Old code
        //                                }
        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                msg = ex.Message;
        //                            }
        //                            var newLine = string.Format("{0} : {1} : {2} \n {3}==============================================================================================================",
        //                                        Convert.ToString(row["Document #"]),
        //                                        Convert.ToString(row["AconexReferenceNo"]),
        //                                        filestatus,
        //                                        msg);
        //                            outLog.AppendLine(newLine);
        //                        }
        //                    }

        //                }
        //                DateTime EndTime = DateTime.Now;
        //                TimeSpan span = EndTime - StartTime;
        //                double totalMinutes = span.TotalMinutes;
        //                outLog.AppendLine("Time Taken (In Minutes) : " + totalMinutes);
        //                File.WriteAllText(outLogPath, outLog.ToString());
        //                SPList list = web.Lists.TryGetList("Aconex Import Logs");
        //                if (list != null)
        //                {
        //                    list.RootFolder.Files.Add(outLogPath.Substring(outLogPath.LastIndexOf("\\") + 1), File.ReadAllBytes(outLogPath));
        //                }
        //            }
        //        }
        //    });
        //}

        //static void ProcessCSV(string csvFilePath, string outCSVPath, string outLogPath)
        //{

        //    /*Thread thread = new Thread(() => {

        //    });

        //    thread.Start();*/
        //    string siteURL = SPContext.Current.Web.Url;
        //    Task.Run(() =>
        //    {
        //        AsyncMethod(siteURL, csvFilePath, outCSVPath, outLogPath);
        //    }).ConfigureAwait(false);
        //}

        //protected void btnSync_Click(object sender, EventArgs e)
        //{
        //    string[] csvFiles = Directory.GetFiles(folderPath, "*.csv");
        //    if (csvFiles.Length > 0)
        //    {
        //        string csvFilePath = csvFiles[0];
        //        string csvFileName = csvFilePath.Substring(csvFilePath.LastIndexOf("\\") + 1);
        //        string outCSVFileName = csvFileName.Substring(0, csvFileName.LastIndexOf(".")) + DateTime.Now.ToString("yyyyMMddhhmmss") + ".csv";
        //        string outLogFileName = csvFileName.Substring(0, csvFileName.LastIndexOf(".")) + DateTime.Now.ToString("yyyyMMddhhmmss") + ".log";
        //        if (!Directory.Exists(folderPath + completeFolderName))
        //        {
        //            Directory.CreateDirectory(folderPath + completeFolderName);
        //        }
        //        if (!Directory.Exists(folderPath + logFolderName))
        //        {
        //            Directory.CreateDirectory(folderPath + logFolderName);
        //        }
        //        ProcessCSV(csvFilePath, folderPath + completeFolderName + outCSVFileName, folderPath + logFolderName + outLogFileName);
        //    }
        //}
    }
}
