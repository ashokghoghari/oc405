﻿<%@ Assembly Name="SP.Solutions.Webpart, Version=1.0.0.0, Culture=neutral, PublicKeyToken=872409048c1d578f" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExcelReportUserControl.ascx.cs" Inherits="SP.Solutions.Webpart.ExcelReport.ExcelReportUserControl" %>
<script type="text/javascript">                         
   //sharepoint postback to work after clicking on telerik export to pdf
   if (typeof (_spBodyOnLoadFunctionNames) != 'undefined' && _spBodyOnLoadFunctionNames != null) {
        _spBodyOnLoadFunctionNames.push("supressSubmitWraper");
     }

   function supressSubmitWraper() {
        _spSuppressFormOnSubmitWrapper = true;
     }                      
</script>
    <table>
        <tr>
            <td>From Date : </td>
            <td><SharePoint:DateTimeControl ID="dtcFromDate" DateOnly="true" runat="server" /></td>
            <td>To Date : </td>
            <td><SharePoint:DateTimeControl ID="dtcToDate" DateOnly="true" runat="server" /></td>
            <td><asp:Button runat="server" ID="btnExport" Text="Export" OnClick="btnExport_Click" /></td>
        </tr>
    </table>
    
    
    
