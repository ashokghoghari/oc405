﻿using Microsoft.Office.DocumentManagement.DocumentSets;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
namespace SP.Solutions.TimerJob
{
    public class WeeklyReport : SPJobDefinition
    {
        public WeeklyReport() : base() { }

        public WeeklyReport(string jobName, SPService service) : base(jobName, service, null, SPJobLockType.None)
        {
            this.Title = "SP.Solution Weekly Report Job";
        }

        public WeeklyReport(string jobName, SPWebApplication webapp) :

        base(jobName, webapp, null, SPJobLockType.ContentDatabase)
        {
            this.Title = "SP.Solution Weekly Report Job";
        }
        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                List<OwnerDocEntity> listDocument = new List<OwnerDocEntity>();
                GetDocumentDetails("https://sharepoint.oc405.com/DControl/ProjDocs/", "/ProjectDocuments", DateTime.Now.AddDays(-1), DateTime.Now, listDocument, "Owner", "Date_x0020_Sent", "Document_x0020_Type");
                GetDocumentDetails("https://sharepoint.oc405.com/DControl/alldocs/", "/AllOther", DateTime.Now.AddDays(-1), DateTime.Now, listDocument, "AllOther", "Date_x0020_Sent", "Document_x0020_Type");
                
                List<QualityDocEntity> listQualityDocument = new List<QualityDocEntity>();
                GetDocumentDetails("https://sharepoint.oc405.com/DControl/design", "/Shared Documents", DateTime.Now.AddDays(-1), DateTime.Now, listQualityDocument, "Quality", "Date1", "Quality_x0020_Document_x0020_Type");

                string fileName = DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".xls";
                string tempFilePath = Path.Combine(@"C:\DailyExport", fileName);// Path.Combine(Path.GetTempPath(), fileName);
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    GenerateExcel(tempFilePath, listDocument, listQualityDocument);
                    //SendFile(tempFilePath, fileName);
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private static void GetDocumentDetails(string url, string librarySiteRelativeURL, DateTime fromDate, DateTime toDate,
                                                 object destDocumentList, string docsFrom, string refFilterCol, string refOrderByCol)
        {
            using (SPSite site = new SPSite(url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    string listUrl = web.ServerRelativeUrl + librarySiteRelativeURL;
                    SPList list = web.GetList(listUrl);
                    if (list != null)
                    {
                        SPQuery query = new SPQuery();
                        string qry = @"<Or>
                                        <And>
                                            <Geq><FieldRef Name = 'Date_x0020_Sent_x002F_Received' /><Value IncludeTimeValue = 'False' Type = 'DateTime'>" + fromDate.ToString("s") + @"</Value></Geq>
                                            <Leq><FieldRef Name = 'Date_x0020_Sent_x002F_Received' /><Value IncludeTimeValue = 'False' Type = 'DateTime'>" + toDate.ToString("s") + @"</Value></Leq>
                                        </And>
                                        <Or>
                                            <And>
                                                <Geq><FieldRef Name = 'Date_x0020_Due_x0020_Back' /><Value IncludeTimeValue = 'False' Type = 'DateTime'>" + fromDate.ToString("s") + @"</Value></Geq>
                                                <Leq><FieldRef Name = 'Date_x0020_Due_x0020_Back' /><Value IncludeTimeValue = 'False' Type = 'DateTime'>" + toDate.ToString("s") + @"</Value></Leq>
                                            </And>
                                            <And>
                                                <Geq><FieldRef Name = 'Date_x0020_Received_x002F_Returned_x0020_Back' /><Value IncludeTimeValue = 'False' Type = 'DateTime'>" + fromDate.ToString("s") + @"</Value></Geq>
                                                <Leq><FieldRef Name = 'Date_x0020_Received_x002F_Returned_x0020_Back' /><Value IncludeTimeValue = 'False' Type = 'DateTime'>" + toDate.ToString("s") + @"</Value></Leq>
                                            </And>    
                                        </Or>    
                                    </Or>";

                        query.Query = "<Where>" + qry + "</Where>" + "<OrderBy><FieldRef Name = '" + refOrderByCol + "' Ascending = 'True' /></OrderBy >";
                        SPListItemCollection items = list.GetItems(query);
                        List<OwnerDocEntity> listDocument = destDocumentList as List<OwnerDocEntity>;

                        foreach (SPListItem item in items)
                            listDocument.Add(new OwnerDocEntity(item, docsFrom));

                    }
                }
            }
        }

        private static void GenerateExcel(string tempFilePath, List<OwnerDocEntity> listDocument, List<QualityDocEntity> listQualityDocument)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    Excel.Application xlApp = new Excel.Application();
                    xlApp.DisplayAlerts = false;
                    xlApp.Visible = false;
                    xlApp.ScreenUpdating = false;

                    if (xlApp == null)
                    {
                        Console.WriteLine("Excel is not properly installed!!");
                        return;
                    }

                    Excel.Workbook xlWorkBook;
                    Excel.Worksheet xlWorkSheet;
                    object misValue = System.Reflection.Missing.Value;

                    xlWorkBook = xlApp.Workbooks.Add(misValue);
                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.Item[1];
                    xlWorkSheet.Name = "Owner Documents";
                    //"Firm #,In/Out,Document Type,STG Number,Description,STG Status,Disposition,Date Sent,Date Due Back,Date Rcvd / Ret Back,CMS #,Discipl,HARTLTR,References/Comments"
                    xlWorkSheet.Cells[1, 1] = "Firm #";
                    xlWorkSheet.Cells[1, 2] = "In/Out";
                    //xlWorkSheet.Cells[1, 3] = "Document Type";
                    xlWorkSheet.Cells[1, 3] = "OC405 Number";
                    xlWorkSheet.Cells[1, 4] = "Description";
                    xlWorkSheet.Cells[1, 5] = "OC405 Status";
                    xlWorkSheet.Cells[1, 6] = "Disposition";
                    xlWorkSheet.Cells[1, 7] = "Date Sent";
                    xlWorkSheet.Cells[1, 8] = "Date Due Back";
                    xlWorkSheet.Cells[1, 9] = "Date Rcvd / Ret Back";
                    //xlWorkSheet.Cells[1, 11] = "CMS #";
                    xlWorkSheet.Cells[1, 10] = "Discipl";
                    //xlWorkSheet.Cells[1, 13] = "HARTLTR";
                    //xlWorkSheet.Cells[1, 14] = "References/Comments";
                    xlWorkSheet.Cells[1, 11] = "Potential Change Notice";

                    var columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 11]];
                    columnHeadingsRange.Interior.Color = XlRgbColor.rgbBlue;
                    columnHeadingsRange.Font.Color = XlRgbColor.rgbWhite;


                    int extraLineCounter = 1;
                    var firmGroups = listDocument.Where(x => x.DocFrom == "Owner").GroupBy(x => x.FirmNo).OrderBy(s => s.Key).ToList();
                    foreach (var nextFirmGroup in firmGroups)
                    {
                        extraLineCounter += 1;
                        xlWorkSheet.Cells[extraLineCounter, 1] = nextFirmGroup.Key;

                        columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[extraLineCounter, 1], xlWorkSheet.Cells[extraLineCounter, 11]];
                        columnHeadingsRange.Interior.Color = XlRgbColor.rgbLightGreen;
                        columnHeadingsRange.Font.Color = XlRgbColor.rgbBlack;

                        var docGroups = nextFirmGroup.ToList().GroupBy(x => x.DocumentType).OrderBy(s => s.Key).ToList();
                        foreach (var nextDocGroup in docGroups)
                        {
                            extraLineCounter += 1;
                            xlWorkSheet.Cells[extraLineCounter, 1] = nextDocGroup.Key;

                            columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[extraLineCounter, 1], xlWorkSheet.Cells[extraLineCounter, 11]];
                            columnHeadingsRange.Interior.Color = XlRgbColor.rgbLightBlue;
                            columnHeadingsRange.Font.Color = XlRgbColor.rgbBlack;

                            foreach (OwnerDocEntity entDocument in nextDocGroup.ToList())
                            {
                                extraLineCounter += 1;
                                AppendRow(entDocument, xlWorkSheet, extraLineCounter);
                            }
                        }
                    }
                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.Add();
                    xlWorkSheet.Name = "All Other Documents";
                    //"Firm #,In/Out,Document Type,STG Number,Description,STG Status,Disposition,Date Sent,Date Due Back,Date Rcvd / Ret Back,CMS #,Discipl,HARTLTR,References/Comments"
                    xlWorkSheet.Cells[1, 1] = "Firm #";
                    xlWorkSheet.Cells[1, 2] = "In/Out";
                    //xlWorkSheet.Cells[1, 3] = "Document Type";
                    xlWorkSheet.Cells[1, 3] = "OC405 Number";
                    xlWorkSheet.Cells[1, 4] = "Description";
                    xlWorkSheet.Cells[1, 5] = "OC405 Status";
                    xlWorkSheet.Cells[1, 6] = "Disposition";
                    xlWorkSheet.Cells[1, 7] = "Date Sent";
                    xlWorkSheet.Cells[1, 8] = "Date Due Back";
                    xlWorkSheet.Cells[1, 9] = "Date Rcvd / Ret Back";
                    //xlWorkSheet.Cells[1, 10] = "CMS #";
                    xlWorkSheet.Cells[1, 10] = "Discipl";
                    //xlWorkSheet.Cells[1, 12] = "HARTLTR";
                    //xlWorkSheet.Cells[1, 13] = "References/Comments";    
                    xlWorkSheet.Cells[1, 11] = "Potential Change Notice";

                    columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 11]];
                    columnHeadingsRange.Interior.Color = XlRgbColor.rgbBlue;
                    columnHeadingsRange.Font.Color = XlRgbColor.rgbWhite;

                    extraLineCounter = 1;
                    firmGroups = listDocument.Where(x => x.DocFrom == "AllOther").GroupBy(x => x.FirmNo).OrderBy(s => s.Key).ToList();
                    foreach (var nextFirmGroup in firmGroups)
                    {
                        extraLineCounter += 1;
                        xlWorkSheet.Cells[extraLineCounter, 1] = nextFirmGroup.Key;

                        columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[extraLineCounter, 1], xlWorkSheet.Cells[extraLineCounter, 11]];
                        columnHeadingsRange.Interior.Color = XlRgbColor.rgbLightGreen;
                        columnHeadingsRange.Font.Color = XlRgbColor.rgbBlack;

                        var docGroups = nextFirmGroup.ToList().GroupBy(x => x.DocumentType).OrderBy(s => s.Key).ToList();
                        foreach (var nextDocGroup in docGroups)
                        {
                            extraLineCounter += 1;
                            xlWorkSheet.Cells[extraLineCounter, 1] = nextDocGroup.Key;

                            columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[extraLineCounter, 1], xlWorkSheet.Cells[extraLineCounter, 11]];
                            columnHeadingsRange.Interior.Color = XlRgbColor.rgbLightBlue;
                            columnHeadingsRange.Font.Color = XlRgbColor.rgbBlack;

                            foreach (OwnerDocEntity entDocument in nextDocGroup.ToList())
                            {
                                extraLineCounter += 1;
                                AppendRow(entDocument, xlWorkSheet, extraLineCounter);
                            }
                        }
                    }

                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.Add();
                    xlWorkSheet.Name = "Quality Documents";
                    //"Firm #,In/Out,Document Type,STG Number,Description,STG Status,Disposition,Date Sent,Date Due Back,Date Rcvd / Ret Back,CMS #,Discipl,HARTLTR,References/Comments"
                    xlWorkSheet.Cells[1, 1] = "Firm #";
                    xlWorkSheet.Cells[1, 2] = "In/Out";
                    //xlWorkSheet.Cells[1, 3] = "Document Type";
                    xlWorkSheet.Cells[1, 3] = "OC405 Number";
                    xlWorkSheet.Cells[1, 4] = "Description";
                    xlWorkSheet.Cells[1, 5] = "OC405 Status";
                    xlWorkSheet.Cells[1, 6] = "Disposition";
                    xlWorkSheet.Cells[1, 7] = "Date Sent";
                    xlWorkSheet.Cells[1, 8] = "Date Due Back";
                    xlWorkSheet.Cells[1, 9] = "Date Rcvd / Ret Back";
                    //xlWorkSheet.Cells[1, 10] = "CMS #";
                    xlWorkSheet.Cells[1, 10] = "Discipl";
                    //xlWorkSheet.Cells[1, 12] = "HARTLTR";
                    //xlWorkSheet.Cells[1, 13] = "References/Comments";    
                    xlWorkSheet.Cells[1, 11] = "Potential Change Notice";

                    columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 11]];
                    columnHeadingsRange.Interior.Color = XlRgbColor.rgbBlue;
                    columnHeadingsRange.Font.Color = XlRgbColor.rgbWhite;

                    extraLineCounter = 1;
                    firmGroups = listDocument.Where(x => x.DocFrom == "Quality").GroupBy(x => x.FirmNo).OrderBy(s => s.Key).ToList();
                    foreach (var nextFirmGroup in firmGroups)
                    {
                        extraLineCounter += 1;
                        xlWorkSheet.Cells[extraLineCounter, 1] = nextFirmGroup.Key;

                        columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[extraLineCounter, 1], xlWorkSheet.Cells[extraLineCounter, 11]];
                        columnHeadingsRange.Interior.Color = XlRgbColor.rgbLightGreen;
                        columnHeadingsRange.Font.Color = XlRgbColor.rgbBlack;

                        var docGroups = nextFirmGroup.ToList().GroupBy(x => x.DocumentType).OrderBy(s => s.Key).ToList();
                        foreach (var nextDocGroup in docGroups)
                        {
                            extraLineCounter += 1;
                            xlWorkSheet.Cells[extraLineCounter, 1] = nextDocGroup.Key;

                            columnHeadingsRange = xlWorkSheet.Range[xlWorkSheet.Cells[extraLineCounter, 1], xlWorkSheet.Cells[extraLineCounter, 11]];
                            columnHeadingsRange.Interior.Color = XlRgbColor.rgbLightBlue;
                            columnHeadingsRange.Font.Color = XlRgbColor.rgbBlack;

                            foreach (OwnerDocEntity entDocument in nextDocGroup.ToList())
                            {
                                extraLineCounter += 1;
                                AppendRow(entDocument, xlWorkSheet, extraLineCounter);
                            }
                        }
                    }
                    
                    xlWorkBook.SaveAs(tempFilePath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.ScreenUpdating = true;
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void AppendRow(OwnerDocEntity entDocument, Excel.Worksheet xlWorkSheet, int currentRowCount)
        {
            xlWorkSheet.Cells[currentRowCount, 1] = entDocument.FirmNo;
            xlWorkSheet.Cells[currentRowCount, 2] = entDocument.InOut;
            //xlWorkSheet.Cells[currentRowCount, 3] = entDocument.DocumentType;
            xlWorkSheet.Cells[currentRowCount, 3] = entDocument.DocumentNumber;
            xlWorkSheet.Cells[currentRowCount, 4] = entDocument.Description;
            xlWorkSheet.Cells[currentRowCount, 5] = entDocument.OC405Status;
            xlWorkSheet.Cells[currentRowCount, 6] = entDocument.Disposition;
            xlWorkSheet.Cells[currentRowCount, 7] = entDocument.DateSent;
            xlWorkSheet.Cells[currentRowCount, 8] = entDocument.DateDueBack;
            xlWorkSheet.Cells[currentRowCount, 9] = entDocument.DateRcvdOrRetBack;
            //xlWorkSheet.Cells[currentRowCount, 10] = entDocument.CMSNo;
            xlWorkSheet.Cells[currentRowCount, 10] = entDocument.Discipl;
            //xlWorkSheet.Cells[currentRowCount, 12] = entDocument.HARTLTR;
            //xlWorkSheet.Cells[currentRowCount, 13] = entDocument.ReferencesOrComments;
            xlWorkSheet.Cells[currentRowCount, 11] = entDocument.PotentialChangeNotice;

            Excel.Range excelCell = (Excel.Range)xlWorkSheet.Cells[currentRowCount, 3];
            Excel.Hyperlink link = (Excel.Hyperlink)xlWorkSheet.Hyperlinks.Add(excelCell, entDocument.LinkAddress, Type.Missing, entDocument.DocumentNumber, entDocument.DocumentNumber);
        }

        private static void AppendRow(QualityDocEntity entDocument, Excel.Worksheet xlWorkSheet, int currentRowCount)
        {
            xlWorkSheet.Cells[currentRowCount, 1] = entDocument.QualityDocumentType;
            xlWorkSheet.Cells[currentRowCount, 2] = entDocument.DocumentNumber;
            xlWorkSheet.Cells[currentRowCount, 3] = entDocument.Subject;

            //Excel.Range excelCell = (Excel.Range)xlWorkSheet.Cells[currentRowCount, 3];
            //Excel.Hyperlink link = (Excel.Hyperlink)xlWorkSheet.Hyperlinks.Add(excelCell, entDocument.LinkAddress, Type.Missing, entDocument.STGNumber, entDocument.STGNumber);
        }
        
    }
    public class CommonFunctions
    {
        public static string GetString(string value)
        {
            if (value == null)
                return string.Empty;

            if (value.Contains(","))
                return "\"" + value + "\"";

            return value;
        }

        public static string GetDateValue(SPListItem item, string datefieldName)
        {
            try
            {
                if (item[datefieldName] == null)
                    return null;

                DateTime dt = (DateTime)Convert.ChangeType(item[datefieldName], typeof(DateTime));
                return dt.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static T GetValue<T>(SPListItem item, string fieldName, bool isLookUp = false)
        {
            try
            {
                if (item[fieldName] == null)
                    return default(T);

                if (isLookUp)
                    return (T)Convert.ChangeType(item[fieldName].ToString().Split(new string[] { "|" }, StringSplitOptions.None)[0], typeof(T));
                return (T)Convert.ChangeType(item[fieldName], typeof(T));
            }
            catch (Exception ex)
            {
                return (T)Convert.ChangeType(string.Empty, typeof(T));
            }
        }
    }

    public class QualityDocEntity
    {
        public QualityDocEntity(SPListItem item)
        {
            QualityDocumentType = CommonFunctions.GetValue<string>(item, "Quality_x0020_Document_x0020_Type");
            DocumentNumber = CommonFunctions.GetValue<string>(item, "LinkFilename");
            Subject = CommonFunctions.GetValue<string>(item, "Title");
        }

        public string QualityDocumentType { get; private set; }
        public string DocumentNumber { get; private set; }
        public string Subject { get; private set; }

        public override string ToString()
        {
            return CommonFunctions.GetString(QualityDocumentType) + ","
                    + CommonFunctions.GetString(DocumentNumber) + ","
                    + CommonFunctions.GetString(Subject);
        }
    }

    public class OwnerDocEntity
    {
        public OwnerDocEntity(SPListItem item, string docFrom)
        {
            try
            {
                DocFrom = docFrom;
                FirmNo = CommonFunctions.GetValue<string>(item, "Firm_x0020__x0023_", true);
                InOut = CommonFunctions.GetValue<string>(item, "In/Out");
                DocumentType = CommonFunctions.GetValue<string>(item, "Document_x0020_Type");
                DocumentNumber = CommonFunctions.GetValue<string>(item, "STG Number");
                Description = CommonFunctions.GetValue<string>(item, "Description");
                OC405Status = CommonFunctions.GetValue<string>(item, "OC405_x0020_Status");
                Disposition = CommonFunctions.GetValue<string>(item, "Disposition"); //NCR_x0020_Disposition
                DateSent = CommonFunctions.GetDateValue(item, "Date_x0020_Sent_x002F_Received");
                DateDueBack = CommonFunctions.GetDateValue(item, "Date Due Back");
                DateRcvdOrRetBack = CommonFunctions.GetDateValue(item, "Date Received/Returned Back");
                CMSNo = CommonFunctions.GetValue<string>(item, "CMS Number");
                Discipl = CommonFunctions.GetValue<string>(item, "Discipline");
                HARTLTR = CommonFunctions.GetValue<string>(item, "HART Letter Number");
                ReferencesOrComments = CommonFunctions.GetValue<string>(item, "References/Comments");
                LinkAddress = item.Web.Url + "/_layouts/15/DocSetHome.aspx?id=" + CommonFunctions.GetValue<string>(item, "Server Relative URL") + "&amp;DefaultItemOpen=1";
                PotentialChangeNotice = CommonFunctions.GetValue<string>(item, "Change_x0020_Notice", true);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override string ToString()
        {
            return CommonFunctions.GetString(FirmNo) + ","
                    + CommonFunctions.GetString(InOut) + ","
                    //+ GetString(DocumentType) + ","
                    + CommonFunctions.GetString(DocumentNumber) + ","
                    + CommonFunctions.GetString(Description) + ","
                    + CommonFunctions.GetString(OC405Status) + ","
                    + CommonFunctions.GetString(Disposition) + ","
                    + CommonFunctions.GetString(DateSent) + ","
                    + CommonFunctions.GetString(DateDueBack) + ","
                    + CommonFunctions.GetString(DateRcvdOrRetBack) + ","
                    + CommonFunctions.GetString(CMSNo) + ","
                    + CommonFunctions.GetString(Discipl) + ","
                    + CommonFunctions.GetString(HARTLTR) + ","
                    + CommonFunctions.GetString(ReferencesOrComments);
        }

        public string DocFrom { get; private set; }

        public string FirmNo { get; private set; }

        public string InOut { get; private set; }

        public string DocumentType { get; private set; }

        public string DocumentNumber { get; private set; }

        public string Description { get; private set; }

        public string OC405Status { get; private set; }

        public string Disposition { get; private set; }

        public string DateSent { get; private set; }

        public string DateDueBack { get; private set; }

        public string DateRcvdOrRetBack { get; private set; }

        public string CMSNo { get; private set; }

        public string Discipl { get; private set; }

        public string HARTLTR { get; private set; }

        public string ReferencesOrComments { get; private set; }

        public string LinkAddress { get; private set; }

        public string PotentialChangeNotice { get; private set; }
    }
}
