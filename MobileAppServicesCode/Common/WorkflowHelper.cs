﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WorkflowServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class WorkflowHelper
    {
        public static  bool StartWorkflow(string url, Guid listId, int itemId)
        {
            try
            {
                using (SPSite site = new SPSite(url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        var workflowServiceManager = new WorkflowServicesManager(web);
                        var workflowSubscriptionService = workflowServiceManager.GetWorkflowSubscriptionService();
                        var subscriptions = workflowSubscriptionService.EnumerateSubscriptionsByList(listId);
                        foreach (var workflowSubscription in subscriptions)
                        {
                            var inputParameters = new Dictionary<string, object>();
                            workflowServiceManager.GetWorkflowInstanceService().StartWorkflowOnListItem(workflowSubscription, itemId, inputParameters);
                        }
                        web.AllowUnsafeUpdates = false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
