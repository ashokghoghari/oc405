﻿using Contract;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class Helper
    {
        public static bool SaveListItems(string siteUrl, AppServiceParam param)
        {
            bool isSaved = true;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            web.AllowUnsafeUpdates = true;

                            foreach (ListInfo nextList in param.ListInfo)
                            {
                                SPList list = web.Lists.TryGetList(nextList.ListName);

                                if (list == null) continue;
                                if (nextList.ListItemInfo == null) continue;

                                foreach (var nextListItem in nextList.ListItemInfo)
                                {
                                    if (nextListItem.ListPropInfo == null) continue;
                                    int itemId = SaveListItem(list, nextListItem);
                                    if (itemId > 0)
                                    {
                                        WorkflowHelper.StartWorkflow(siteUrl, list.ID, itemId);
                                    }
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                isSaved = false;
            }
            return isSaved;
        }
        public static int SaveListItem(SPList list, ListItemInfo nextListItem)
        {
            int listItemId = 0;
            try
            {
                SPListItem newItem = null;
                ListPropertyInfo idField = nextListItem.ListPropInfo.Where(p => p.PropertyName == "ID").FirstOrDefault();
                if (Convert.ToInt32(idField.PropertyValue) > 0)
                {
                    newItem = list.Items.GetItemById(Convert.ToInt32(idField.PropertyValue));
                    newItem["Status"] = "Updated";
                }
                else
                {
                    newItem = list.Items.Add();
                }
                foreach (ListPropertyInfo nextListProperty in nextListItem.ListPropInfo)
                {
                    /*if (nextListProperty.PropertyName.Equals("PlacementTime") || nextListProperty.PropertyName.Equals("TimeRequested"))
                    {
                        nextListProperty.PropertyType = PropertyTypeEnum.DateTime;
                    }*/
                    object value = null;
                    if (nextListProperty.PropertyValue != null && !nextListProperty.PropertyName.Equals("ID") && !nextListProperty.PropertyName.Equals("Author"))
                    {
                        switch (nextListProperty.PropertyType)
                        {
                            case PropertyTypeEnum.File:
                                FilePropertyInfo[] files = ((Newtonsoft.Json.Linq.JArray)nextListProperty.PropertyValue).ToObject<FilePropertyInfo[]>();
                                foreach (FilePropertyInfo file in files)
                                {
                                    newItem.Attachments.Add(file.Name, Convert.FromBase64String(file.FileBase64));
                                }
                                break;
                            case PropertyTypeEnum.Number:
                                value = Convert.ToInt32(nextListProperty.PropertyValue);
                                break;
                            case PropertyTypeEnum.Boolean:
                                value = Convert.ToBoolean(nextListProperty.PropertyValue);
                                break;
                            case PropertyTypeEnum.DateTime:
                                value = nextListProperty.PropertyValue;
                                break;
                            case PropertyTypeEnum.PersonOrGroup:
                                value = new SPFieldUserValue(list.ParentWeb, Convert.ToString(nextListProperty.PropertyValue));
                                break;
                            case PropertyTypeEnum.MultiPersonOrGroup:
                                break;
                            case PropertyTypeEnum.Metadata:
                                value = nextListProperty.PropertyValue;
                                break;
                            case PropertyTypeEnum.MultiMetadata:
                                break;
                            case PropertyTypeEnum.LookUp:
                                value = nextListProperty.PropertyValue;
                                break;
                            case PropertyTypeEnum.MultiLookUp:
                                break;
                            case PropertyTypeEnum.Choice:
                                value = nextListProperty.PropertyValue;
                                break;
                            case PropertyTypeEnum.MultiChoice:
                                string[] values = ((Newtonsoft.Json.Linq.JArray)nextListProperty.PropertyValue).ToObject<string[]>();
                                SPFieldMultiChoiceValue choiceFieldValue = new SPFieldMultiChoiceValue();
                                foreach (string strVal in values)
                                {
                                    choiceFieldValue.Add(strVal);
                                }
                                value = choiceFieldValue;
                                break;
                            case PropertyTypeEnum.String:
                                value = nextListProperty.PropertyValue;
                                break;
                            case PropertyTypeEnum.ReadOnly:
                            default:
                                break;
                        }

                        if (nextListProperty.PropertyType != PropertyTypeEnum.File)
                            newItem[nextListProperty.PropertyName] = value;
                    }
                    else if (nextListProperty.PropertyName.Equals("Author"))
                    {
                        newItem["Author"] = new SPFieldUserValue(list.ParentWeb, Convert.ToString(nextListProperty.PropertyValue));
                        newItem["Editor"] = new SPFieldUserValue(list.ParentWeb, Convert.ToString(nextListProperty.PropertyValue));
                    }



                }
                newItem["Title"] = "Custom Title";
                newItem.Update();
                listItemId = newItem.ID;
            }
            catch (Exception ex)
            {

            }
            return listItemId;
        }

        public static SPListItemCollection GetListItem(Uri siteUri, string listPath, string queryXML, uint rowCount, string folderName)
        {
            SPListItemCollection itemColl;

            SPList list;
            using (SPSite site = new SPSite(siteUri.ToString().TrimEnd('/')))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    list = web.Lists[listPath];
                }
            }

            SPQuery query = new SPQuery();
            query.Query = queryXML;
            query.RowLimit = rowCount;

            itemColl = list.GetItems(query);
            return itemColl;
        }
        public static GetDropdown GetAlignments(string fieldName)
        {
            string siteUrl = "https://sharepoint.oc405.com/planspecs/rfis";
            string listName = "SegmentLine";
            GetDropdown data = new GetDropdown();
            data.fieldName = fieldName;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        SPList list = oWeb.Lists[listName];
                        SPListItemCollection items = list.GetItems();
                        data.value = new List<DropDownData>();
                        foreach (SPListItem item in items)
                        {
                            DropDownData alignmentdata = new DropDownData();
                            alignmentdata.key = Convert.ToString(item["Title"]) + "-" + Convert.ToString(item["Line"]);
                            alignmentdata.value = Convert.ToString(item["Title"]) + "-" + Convert.ToString(item["Line"]);
                            data.value.Add(alignmentdata);
                        }
                        data.value = data.value.OrderBy(p => p.key).ToList();
                    }
                }
            });

            return data;
        }
        public static GetDropdown GetSuperintendents(string fieldName)
        {
            string siteUrl = "https://sharepoint.oc405.com/quality/pod";
            string listName = "Superintendents";
            GetDropdown data = new GetDropdown();
            data.fieldName = fieldName;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        SPList list = oWeb.Lists[listName];
                        SPListItemCollection items = list.GetItems();
                        data.value = new List<DropDownData>();
                        foreach (SPListItem item in items)
                        {
                            DropDownData ddData = new DropDownData();
                            string val = Convert.ToString(item["Super_x0020_Contact_x0020_Phone"]);
                            if (!string.IsNullOrEmpty(val) && val.IndexOf(";#") != -1) {
                                val = val.Substring(8);
                            }
                            ddData.key = val;
                            ddData.value = val;
                            data.value.Add(ddData);
                        }
                        data.value = data.value.OrderBy(p => p.key).ToList();
                    }
                }
            });
            return data;
        }
        public static GetDropdown GetForemans(string fieldName)
        {
            string siteUrl = "https://sharepoint.oc405.com/quality/pod";
            string listName = "Foreman";
            GetDropdown data = new GetDropdown();
            data.fieldName = fieldName;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        SPList list = oWeb.Lists[listName];
                        SPListItemCollection items = list.GetItems();
                        data.value = new List<DropDownData>();
                        foreach (SPListItem item in items)
                        {
                            DropDownData ddData = new DropDownData();
                            string val = Convert.ToString(item["Foreman_x0020_Contact_x0020_Phon"]);
                            if (!string.IsNullOrEmpty(val) && val.IndexOf(";#") != -1)
                            {
                                val = val.Substring(8);
                            }
                            ddData.key = val;
                            ddData.value = val;
                            data.value.Add(ddData);
                        }
                        data.value = data.value.OrderBy(p => p.key).ToList();
                    }
                }
            });
            return data;
        }
        public static List<PODRequest> GetPODs(SPWeb web, string listName, string timeFieldName)
        {
            List<PODRequest> pods = new List<PODRequest>();
            SPList list = web.Lists.TryGetList(listName);
            SPQuery query = new SPQuery();
            query.Query = string.Concat("<Where>",
                    "<And>",
                        "<Gt>",
                            "<FieldRef Name='" + timeFieldName + "' /><Value Type='DateTime'>" + SPUtility.CreateISO8601DateTimeFromSystemDateTime(DateTime.Today.AddDays(-2)) + "</Value>",
                        "</Gt>",
                        "<Lt>",
                            "<FieldRef Name='" + timeFieldName + "' /><Value Type='DateTime'>" + SPUtility.CreateISO8601DateTimeFromSystemDateTime(DateTime.Today.AddDays(8)) + "</Value>",
                        "</Lt>",
                    "</And>",
                "</Where>");
            SPListItemCollection items = list.GetItems(query);
            foreach (SPListItem item in items)
            {
                pods.Add(new PODRequest(Convert.ToString(item["Address"]),
                    (new SPFieldUserValue(web, Convert.ToString(item["Author"]))).LookupValue,
                    Convert.ToString(item["ID"]), Convert.ToString(item["Lat"]),
                    Convert.ToString(item["Long"]), list.ID.ToString(), Convert.ToString(item["Status"]),
                    Convert.ToString(item[timeFieldName]), listName, list.RootFolder.Name));
            }
            return pods;
        }

        public static GetDropdown GetChoiceData(string siteUrl, string listname, string fieldname)
        {
            SPList oList;
            GetDropdown data = new GetDropdown();
            data.fieldName = fieldname;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        oList = oWeb.Lists[listname];

                        data.fieldName = fieldname;
                        data.value = new List<DropDownData>();

                        SPFieldChoice MixDesignNumberChoicefield = (SPFieldChoice)oList.Fields[fieldname];
                        for (int i = 0; i < MixDesignNumberChoicefield.Choices.Count; i++)
                        {
                            DropDownData dropdowndata = new DropDownData();
                            dropdowndata.key = MixDesignNumberChoicefield.Choices[i].ToString();
                            dropdowndata.value = MixDesignNumberChoicefield.Choices[i].ToString();
                            data.value.Add(dropdowndata);
                        }
                        data.value = data.value.OrderBy(p=>p.key).ToList();
                    }
                }
            });
            return data;
        }
        public static GetDropdown GetMultiChoiceData(string siteUrl, string listname, string fieldname)
        {
            SPList oList;
            GetDropdown data = new GetDropdown();
            data.fieldName = fieldname;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        oList = oWeb.Lists[listname];

                        data.fieldName = fieldname;
                        data.value = new List<DropDownData>();

                        SPFieldMultiChoice MixDesignNumberChoicefield = (SPFieldMultiChoice)oList.Fields[fieldname];
                        for (int i = 0; i < MixDesignNumberChoicefield.Choices.Count; i++)
                        {
                            DropDownData dropdowndata = new DropDownData();
                            dropdowndata.key = MixDesignNumberChoicefield.Choices[i].ToString();
                            dropdowndata.value = MixDesignNumberChoicefield.Choices[i].ToString();
                            data.value.Add(dropdowndata);
                        }
                        data.value = data.value.OrderBy(p => p.key).ToList();
                    }
                }
            });
            return data;
        }
        public static GetDropdown GetTermStoreDataWithValue(string siteUrl, string termstore, string termgroup, string termset, string fieldName)
        {
            GetDropdown data = new GetDropdown();
            data.fieldName = fieldName;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        data.fieldName = termset;
                        data.value = new List<DropDownData>();
                        TaxonomySession Taxonomy;
                        Taxonomy = new TaxonomySession(oSite);
                        TermStore termStore = Taxonomy.TermStores[termstore];
                        Group group = termStore.Groups[termgroup];
                        TermSet taxTerm = group.TermSets[termset];
                        var terms = taxTerm.Terms;
                        foreach (Term term in terms)
                        {
                            DropDownData UnitIddata = new DropDownData();
                            UnitIddata.key = term.Name;// term.Id.ToString();
                            UnitIddata.value = Convert.ToString(term.Id);
                            data.value.Add(UnitIddata);
                        }
                        data.value = data.value.OrderBy(p => p.key).ToList();
                    }
                }
            });
            return data;
        }
        public static GetDropdown GetTermStoreData(string siteUrl, string termstore, string termgroup, string termset, string fieldName)
        {
            GetDropdown data = new GetDropdown();
            data.fieldName = fieldName;
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite oSite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        data.fieldName = termset;
                        data.value = new List<DropDownData>();
                        TaxonomySession Taxonomy;
                        Taxonomy = new TaxonomySession(oSite);
                        TermStore termStore = Taxonomy.TermStores[termstore];
                        Group group = termStore.Groups[termgroup];
                        TermSet taxTerm = group.TermSets[termset];
                        var terms = taxTerm.Terms;
                        foreach (Term term in terms)
                        {
                            DropDownData UnitIddata = new DropDownData();
                            UnitIddata.key = term.Name;// term.Id.ToString();
                            UnitIddata.value = term.Name;
                            data.value.Add(UnitIddata);
                        }
                        data.value = data.value.OrderBy(p => p.key).ToList();
                    }
                }
            });
            return data;
        }

        public static string GetTermString(object term)
        {
            return Convert.ToString(term);
        }

        public static string GetTermGUID(object term)
        {
            string termGUID = string.Empty;
            string strTerm = GetTermString(term);
            if (!string.IsNullOrEmpty(strTerm))
            {
                termGUID = strTerm.Substring(strTerm.LastIndexOf("|") + 1);
            }
            return termGUID;
        }

        public static string GetTermLabel(object term)
        {
            string termLable = string.Empty;
            string strTerm = GetTermString(term);
            if (!string.IsNullOrEmpty(strTerm))
            {
                termLable = strTerm.Substring(0, strTerm.LastIndexOf("|"));
            }
            return termLable;
        }

        public static ListPropertyInfo GetStringListPropertyInfo(SPListItem item, string propertyInternalName, PropertyTypeEnum propertyType)
        {
            return new ListPropertyInfo()
            {
                PropertyName = propertyInternalName,
                PropertyValue = (item == null || item[propertyInternalName] == null ? "" : Convert.ToString(item[propertyInternalName])),
                PropertyType = propertyType
            };
        }
        public static ListPropertyInfo GetMetaLableListPropertyInfo(SPListItem item, string propertyInternalName, PropertyTypeEnum propertyType)
        {
            return new ListPropertyInfo()
            {
                PropertyName = propertyInternalName,
                PropertyValue = (item == null || item[propertyInternalName] == null ? "" : GetTermLabel(item[propertyInternalName])),
                PropertyType = propertyType
            };
        }
        /*public static ListPropertyInfo GetPersonNameListPropertyInfo(SPListItem item, string propertyInternalName, PropertyTypeEnum propertyType)
        {
            return new ListPropertyInfo()
            {
                PropertyName = propertyInternalName,
                PropertyValue = (item == null || item[propertyInternalName] == null ? "" : GetPersonName(item[propertyInternalName])),
                PropertyType = propertyType
            };
        }*/


    }
}
