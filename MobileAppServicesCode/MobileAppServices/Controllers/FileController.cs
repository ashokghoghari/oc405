﻿using Contract;
using Microsoft.SharePoint;
using MobileAppServices.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace MobileAppServices.Controllers
{
    
    public class FileController : ApiController
    {
        [HttpGet]
        [Route("api/file")]
        public string Get()
        {
            return "Running up";
        }

        [HttpPost]
        [Route("api/file")]
        public HttpResponseMessage Post(InspectionParam param)
        {
            HttpResponseMessage result = null;
            try
            {
                result = Request.CreateResponse(HttpStatusCode.OK, "OK");
                string siteUrl = "https://sharepoint.oc405.com";

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            web.AllowUnsafeUpdates = true;
                            SPList list = web.Lists.TryGetList("ProjectDesignQualityProcedure");
                            SPListItem item = list.Items.Add();
                            item["Title"] = param.Title;
                            item["Lat"] = param.Lat;
                            item["Lon"] = param.Lon;
                            item["DayShift"] = param.Shift;
                            if (param.files != null && param.files.Count > 0)
                            {
                                foreach (FileInfo file in param.files)
                                {
                                    item.Attachments.Add(file.Name, Convert.FromBase64String(file.FileBase64));
                                }
                            }
                            item.Update();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                //throw ex;
                result = Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return result;

        }

        [HttpPost]
        [Route("api/file/submitinspectionform")]
        public HttpResponseMessage SubmitInspectionForm(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = true;
            response["Message"] = "Form Saved Successfully.";
            HttpResponseMessage result = null;
            try
            {
                result = Request.CreateResponse(HttpStatusCode.OK, "Form Saved Successfully.");
                string siteUrl = "https://sharepoint.oc405.com";

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            web.AllowUnsafeUpdates = true;

                            foreach (ListInfo nextList in param.ListInfo)
                            {
                                SPList list = web.Lists.TryGetList(nextList.ListName);

                                if (list == null) continue;
                                if (nextList.ListItemInfo == null) continue;

                                foreach (var nextListItem in nextList.ListItemInfo)
                                {
                                    SPListItem newItem = list.Items.Add();

                                    if (nextListItem.ListPropInfo == null) continue;

                                    foreach (ListPropertyInfo nextListProperty in nextListItem.ListPropInfo)
                                    {
                                        object value = null;
                                        if (nextListProperty.PropertyValue != null)
                                        {
                                            switch (nextListProperty.PropertyType)
                                            {
                                                case PropertyTypeEnum.File:

                                                    FilePropertyInfo[] files = ((Newtonsoft.Json.Linq.JArray)nextListProperty.PropertyValue).ToObject<FilePropertyInfo[]>();
                                                    foreach (FilePropertyInfo file in files)
                                                    {
                                                        newItem.Attachments.Add(file.Name, Convert.FromBase64String(file.FileBase64));
                                                    }
                                                    break;

                                                case PropertyTypeEnum.DateTime:
                                                    break;

                                                case PropertyTypeEnum.PersonOrGroup:
                                                    break;

                                                case PropertyTypeEnum.LookUp:
                                                    break;
                                                case PropertyTypeEnum.Choice:
                                                    /*string[] choicevalues = ((Newtonsoft.Json.Linq.JArray)nextListProperty.PropertyValue).ToObject<string[]>();
                                                    if (choicevalues.Length > 0)
                                                    {
                                                        value = choicevalues[0];
                                                    }*/
                                                    value = nextListProperty.PropertyValue;
                                                    break;
                                                case PropertyTypeEnum.MultiChoice:

                                                    string[] values = ((Newtonsoft.Json.Linq.JArray)nextListProperty.PropertyValue).ToObject<string[]>();
                                                    SPFieldMultiChoiceValue choiceFieldValue = new SPFieldMultiChoiceValue();
                                                    foreach (string strVal in values)
                                                    {
                                                        choiceFieldValue.Add(strVal);
                                                    }
                                                    value = choiceFieldValue;
                                                    break;

                                                case PropertyTypeEnum.String:
                                                default:
                                                    value = nextListProperty.PropertyValue;
                                                    break;
                                            }

                                            if (nextListProperty.PropertyType != PropertyTypeEnum.File)
                                                newItem[nextListProperty.PropertyName] = value;
                                        }

                                    }

                                    newItem.Update();
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                response["IsSaved"] = false;
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/file/getinspectionform")]
        public ListInfo GetInspectionForm()
        {
            ListInfo listDetails = new ListInfo();
            string siteUrl = "https://sharepoint.oc405.com";

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                listDetails = new ListInfo();
                listDetails.ListName = "SpecialInspectionReport";
                listDetails.ListItemInfo = new List<ListItemInfo>();

                string query = "<OrderBy><FieldRef Name='ID' Ascending='FALSE' /></OrderBy>";// "<OrderBy>    < FieldRef Name = 'ID' Ascending = 'False' />   </ OrderBy > ";

                SPListItemCollection spListItemCollection = GetListItem(new Uri(siteUrl), "SpecialInspectionReport", query, 5, string.Empty);
                foreach (SPListItem item in spListItemCollection)
                {
                    ListItemInfo newListItem = new ListItemInfo();
                    newListItem.ListPropInfo = new List<ListPropertyInfo>();
                    newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "BuildingPermitNo", PropertyValue = (item["BuildingPermitNo"] == null ? null : item["BuildingPermitNo"].ToString()) });
                    newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ProjectNo", PropertyValue = (item["ProjectNo"] == null ? null : item["ProjectNo"].ToString()) });
                    newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ReportNo", PropertyValue = (item["ReportNo"] == null ? null : item["ReportNo"].ToString()) });
                    listDetails.ListItemInfo.Add(newListItem);
                }
            });
            return listDetails;
        }

        private SPListItemCollection GetListItem(Uri siteUri, string listPath, string queryXML, uint rowCount, string folderName)
        {
            SPList list;
            using (SPSite site = new SPSite(siteUri.ToString().TrimEnd('/')))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    list = web.Lists[listPath];
                }
            }

            SPQuery query = new SPQuery();
            query.Query = queryXML; 
            query.RowLimit = rowCount; // Limit the result to increase performance

            SPListItemCollection itemColl = list.GetItems(query);
            return itemColl;
        }
    }

    public class InspectionParam
    {
        public string Title { get; set; }

        public List<FileInfo> files { get; set; }

        public string Lat { get; set; }

        public string Lon { get; set; }

        public string Shift { get; set; }
    }

    public class FileInfo
    {
        public string Name { get; set; }
        public string FileBase64 { get; set; }
    }
}
