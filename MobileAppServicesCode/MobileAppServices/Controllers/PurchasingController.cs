﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAppServices.Controllers
{
    public class PurchasingController : ApiController
    {
        [HttpGet]
        [Route("api/purchasing/GetCostCodes/{prefix}")]
        public Dictionary<string, string> GetCostCodes([FromUri] string prefix)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\\oc405dell1\Materials\Project Controls\ForPurchaseQuery\Purchase Query.accdb");
            OleDbCommand cmd = con.CreateCommand();
            con.Open();
            cmd.CommandText = "SELECT * FROM `QPURCHASE-2019-01` WHERE CostCode like '%" + prefix + "%'";
            cmd.Connection = con;
            OleDbDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                //var dt = rdr.NextResult();
                Console.WriteLine(rdr[0].ToString());
                dict[rdr[0].ToString()] = rdr[1].ToString();
            }
            //MessageBox.Show("Record Submitted", "Congrats");
            con.Close();
            return dict;
        }

        [HttpGet]
        [Route("api/purchasing/GetCostCodesFromDB/{prefix}")]
        public Dictionary<string, string> GetCostCodesFromDB([FromUri] string prefix)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            SqlConnection con = new SqlConnection(@"Data Source=OC405SQL02;Initial Catalog=EC_OC405;Integrated Security=True");
            SqlCommand cmd = con.CreateCommand();
            con.Open();
            cmd.CommandText = "SELECT CostCode,CostCodeName FROM tbl_Mstr_CostCode where CostCode like '%" + prefix + "%' or CostCodeName like '%" + prefix + "%'";
            cmd.Connection = con;
            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                string key = rdr[0].ToString() + " - " + rdr[1].ToString();
                dict[key] = key;
            }
            con.Close();
            return dict;
        }

    }
}
