﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Web.Http;

namespace MobileAppServices.Controllers
{
    public class ProgressController : ApiController
    {
        [HttpPost]
        [Route("api/progress")]
        public HttpResponseMessage Post([FromBody]VideoItem param)
        {
            HttpResponseMessage result = null;
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSuccess"] = true;
            response["Message"] = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param.Title) && !string.IsNullOrEmpty(param.Category))
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        using (SPSite site = new SPSite("https://sharepoint.oc405.com/quality/progpics"))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                web.AllowUnsafeUpdates = true;
                                SPList list = web.Lists.TryGetList("ProgressVideo");
                                SPListItem item = list.AddItem();
                                item["Title"] = param.Title;
                                item["Category"] = param.Category;
                                item["Files"] = param.Files;
                                item["Location"] = param.Location;
                                SPUser user = web.EnsureUser(param.CurrentuserEmail);
                                SPFieldUserValue userValue = new SPFieldUserValue(web, user.ID, user.LoginName);
                                item["Author"] = userValue;
                                item["Editor"] = userValue;
                                item.Update();
                                response["Message"] = "Success";
                                response["ItemId"] = item.ID;
                                web.AllowUnsafeUpdates = false;
                            }
                        }
                    });
                }
                else
                {
                    response["IsSuccess"] = false;
                    response["Message"] = "Please enter Title & Category.";
                }
            }
            catch (Exception ex)
            {
                response["IsSuccess"] = false;
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.Created, response);
            return result;
        }

        [HttpDelete]
        [Route("api/progress/{itemId}")]
        public HttpResponseMessage Delete(int itemId)
        {
            Impersonate.ImpersonateUser("oc405", "patrick.park", "OC405pat");
            string dirPath = @"\\nas1\Media\ProgressVids\" + Convert.ToString(itemId);
            if (Directory.Exists(dirPath))
                Directory.Delete(dirPath, true);
            Impersonate.UndoImpersonation();


            return new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Content = new StringContent("File(s) deleted.")
            };
        }
    }
}
