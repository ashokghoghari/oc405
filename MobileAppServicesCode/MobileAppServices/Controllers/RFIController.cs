﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace MobileAppServices.Controllers
{
    public class RFIController : ApiController
    {
        public string siteUrl = "";

        [HttpGet]
        [Route("api/rfi/_planspecs_rfis/{ItemId}")]
        public HttpResponseMessage GenerateRFIPdfProd(int ItemId)
        {
            this.siteUrl = "http://oc405sp01:2018/planspecs/rfis";
            return this.GenerateRFIPdf(ItemId);
        }

        [HttpGet]
        [Route("api/rfi/_planspecs_rfis_dev/{ItemId}")]
        public HttpResponseMessage GenerateRFIPdfDev(int ItemId)
        {
            this.siteUrl = "http://oc405sp01:2018/planspecs/rfis/dev";
            return this.GenerateRFIPdf(ItemId);
        }

        public HttpResponseMessage GenerateRFIPdf(int ItemId)
        {
            HttpResponseMessage response;
            Dictionary<string, object> data = new Dictionary<string, object>();
            string baseLocation = "D:\\Temp\\";
            string folder = ItemId + "-" + Guid.NewGuid().ToString();
            string location = baseLocation + folder + "\\";
            string outputFileName = "RFI_" + ItemId + ".pdf";
            string outFilePath = location + outputFileName;
            if (!Directory.Exists(location))
            {
                Directory.CreateDirectory(location);
            }
            List<string> files = new List<string>();
            Dictionary<string, string> rfiData = GetStaticData(ItemId);
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                Document document = new Document(PageSize.A4, 10, 10, 10, 10);
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                Image img = Image.GetInstance("D:\\logo.png");
                img.ScaleToFit(75, 75);
                img.SpacingAfter = 5;
                document.Add(img);
                var boldFont = FontFactory.GetFont("Calibri", 10f, Font.BOLD, BaseColor.BLACK);
                var normalFont = FontFactory.GetFont("Calibri", 10f, Font.NORMAL, BaseColor.BLACK);
                var whiteboldFont = FontFactory.GetFont("Calibri", 12f, Font.BOLD, BaseColor.WHITE);
                PdfPTable section1 = GetSection1(rfiData, normalFont, boldFont, whiteboldFont);
                document.Add(section1);
                PdfPTable section2 = GetSection2(rfiData, normalFont, boldFont, whiteboldFont);
                document.Add(section2);
                PdfPTable section3 = GetSection3(rfiData, normalFont, boldFont, whiteboldFont);
                document.Add(section3);
                PdfPTable section4 = GetSection4(rfiData, normalFont, boldFont, whiteboldFont);
                document.Add(section4);
                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                files.Add(location + "generated.pdf");
                System.IO.File.WriteAllBytes(files[0], bytes);
            }
            files.AddRange(DownloadAttachments(ItemId, location));
            CombineMultiplePDFs(files.ToArray(), outFilePath);
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    SPWeb web = site.OpenWeb();
                    web.AllowUnsafeUpdates = true;
                    SPList list = web.Lists.TryGetList("RFI");
                    SPListItem item = list.GetItemById(ItemId);
                    if (item != null)
                    {
                        if (item.Attachments.Count > 0)
                        {
                            item.Attachments.DeleteNow(outputFileName);

                        }
                        item.Attachments.AddNow(outputFileName, File.ReadAllBytes(outFilePath));
                        data["FilePath"] = item.Attachments[0];
                    }
                    web.AllowUnsafeUpdates = false;
                }
            });
            Directory.Delete(location, true);
            data["Success"] = true;
            data["Message"] = "File Generated Successfully.";

            response = Request.CreateResponse(HttpStatusCode.OK, data);
            return response;
        }

        private PdfPCell GetHeaderCell(string Title, Font font)
        {
            PdfPCell cell = new PdfPCell(new Phrase(Title, font));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingBottom = 10;
            cell.PaddingTop = 5;
            cell.PaddingLeft = 5;
            cell.BackgroundColor = new BaseColor(51, 122, 183);
            return cell;
        }

        private Paragraph GetUnderlinedCell(string text, Font font)
        {
            Paragraph para = new Paragraph(text, font);
            LineSeparator underline = new LineSeparator(1, 100, BaseColor.BLACK, Element.ALIGN_LEFT, -2);
            para.Add(underline);
            return para;
        }

        private string GetMetaValue(object obj)
        {
            string val = "-";
            if (obj != null)
            {
                if (obj.GetType().Name == "TaxonomyFieldValueCollection")
                {
                    val = "";
                    TaxonomyFieldValueCollection data = (TaxonomyFieldValueCollection)obj;
                    for (int i = 0; i < data.Count; i++)
                    {
                        val += data[i].Label + ",";
                    }
                    val = val.Trim(',');
                }
                else
                {
                    val = ((TaxonomyFieldValue)obj).Label;
                }
            }
            return val;
        }

        private string GetLookupValue(object obj)
        {
            string val = "-";
            if (obj != null)
            {
                val = (new SPFieldLookupValue(Convert.ToString(obj))).LookupValue;
            }
            return val;
        }

        private string GetMultiLookupValue(object obj)
        {
            string val = "-";
            if (obj != null)
            {
                val = "";
                SPFieldLookupValueCollection data = (SPFieldLookupValueCollection)obj;
                for (int i = 0; i < data.Count; i++)
                {
                    val += data[i].LookupValue + ",";
                }
                val = val.Trim(',');
                //val = (new SPFieldLookupValue(Convert.ToString(obj))).LookupValue;
            }
            return val;
        }

        private string GetDateValue(object obj)
        {
            string val = "-";
            if (obj != null)
            {
                val = ((DateTime)obj).ToString("MM-dd-yyyy");
            }
            return val;
        }

        private string GetMultiChoiceValue(object obj)
        {
            string val = "-";
            if (obj != null)
            {
                val = "";
                SPFieldMultiChoiceValue data = new SPFieldMultiChoiceValue(Convert.ToString(obj));
                for (int i = 0; i < data.Count; i++)
                {
                    val += data[i] + ",";
                }
                val = val.Trim(',');
            }
            return val;
        }

        private Dictionary<string, string> GetStaticData(int id)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["ID"] = Convert.ToString(id);
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    SPWeb web = site.OpenWeb();
                    SPList list = web.Lists.TryGetList("RFI");
                    SPListItem item = list.GetItemById(id);
                    if (item != null)
                    {
                        data["Project RFI No."] = Convert.ToString(item["ProjectRFINo"]);
                        data["Authority RFI No."] = Convert.ToString(item["AuthorityRFINo"]);

                        data["Closeout ID No."] = GetMetaValue(item["CloseOutIDNo"]);
                        data["Discipline"] = GetMetaValue(item["Discipline"]);
                        data["Segment"] = GetMetaValue(item["Segment"]);
                        data["Line"] = GetMultiLookupValue(item["Line"]);

                        data["Question from"] = GetLookupValue(item["QuestionFrom"]);
                        data["Originator"] = GetLookupValue(item["Originator"]);
                        data["Question directed"] = GetLookupValue(item["QuestionDirected"]);
                        data["Reviewer of Request"] = GetLookupValue(item["ReviewerOfRequest"]);

                        data["Date of Issuance"] = GetDateValue(item["DateOfIssuance"]);
                        data["Reply Needed By"] = GetDateValue(item["ReplyNeededBy"]);

                        data["Construction Stage"] = Convert.ToString(item["ConstructionStage"]);
                        data["Subject of Request"] = GetMultiChoiceValue(item["SubjectOfRequest"]);
                        data["Priority Level"] = Convert.ToString(item["PriorityLevel"]);
                        data["Location"] = Convert.ToString(item["Location"]);
                        data["Previous Issue"] = Convert.ToString(item["PreviousIssue"]);
                        data["Description"] = Convert.ToString(item["Description"]);
                        data["Question"] = Convert.ToString(item["Question"]);
                        data["Sheets Affected List"] = Convert.ToString(item["SheetsAffectedList"]);
                        data["Proposed Mitigation"] = Convert.ToString(item["ProposedMitigation"]);
                        data["Closed"] = Convert.ToString(item["CloserType"]);
                        data["NDC #"] = Convert.ToString(item["NDCNo"]);
                        data["Remark"] = Convert.ToString(item["CloserRemark"]);
                    }
                }
            });
            return data;
        }

        private PdfPTable GetSection1(Dictionary<string, string> data, Font normalFont, Font boldFont, Font whiteboldFont)
        {
            PdfPTable table = new PdfPTable(1);
            table.SpacingBefore = 5;
            table.WidthPercentage = 100;
            table.AddCell(GetHeaderCell("Section 1", whiteboldFont));
            PdfPTable section1 = new PdfPTable(new float[] { 20f, 25f, 20f, 35f });
            section1.DefaultCell.Border = 0;
            section1.SpacingBefore = 5; section1.SpacingAfter = 5;
            section1.AddCell(new Phrase("Project RFI No.:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Project RFI No."], normalFont));
            section1.AddCell(new Phrase("Authority RFI No.:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Authority RFI No."], normalFont));
            section1.AddCell(new Phrase("Closeout ID No.:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Closeout ID No."], normalFont));
            section1.AddCell(new Phrase("Discipline:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Discipline"], normalFont));
            section1.AddCell(new Phrase("Segment:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Segment"], normalFont));
            section1.AddCell(new Phrase("Line:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Line"], normalFont));
            section1.AddCell(new Phrase("Question from:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Question from"], normalFont));
            section1.AddCell(new Phrase("Originator:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Originator"], normalFont));
            section1.AddCell(new Phrase("Question directed:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Question directed"], normalFont));
            section1.AddCell(new Phrase("Reviewer of Request:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Reviewer of Request"], normalFont));
            section1.AddCell(new Phrase("Date of Issuance:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Date of Issuance"], normalFont));
            section1.AddCell(new Phrase("Reply Needed By:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Reply Needed By"], normalFont));
            section1.AddCell(new Phrase("Construction Stage:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Construction Stage"], normalFont));
            PdfPCell cell = new PdfPCell(new Phrase("For Construction Complete don't use this form, go to NCR", normalFont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.Colspan = 2;
            section1.AddCell(cell);
            table.AddCell(section1);
            return table;
        }

        private PdfPTable GetSection2(Dictionary<string, string> data, Font normalFont, Font boldFont, Font whiteboldFont)
        {
            PdfPTable table = new PdfPTable(1);
            table.SpacingBefore = 5;
            table.WidthPercentage = 100;
            table.AddCell(GetHeaderCell("Section 2", whiteboldFont));
            PdfPTable section1 = new PdfPTable(new float[] { 20f, 25f, 20f, 35f });
            section1.DefaultCell.Border = 0;
            section1.SpacingBefore = 5; section1.SpacingAfter = 5;
            section1.AddCell(new Phrase("Subject of Request:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Subject of Request"], normalFont));
            section1.AddCell(new Phrase("Priority Level:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Priority Level"], normalFont));
            section1.AddCell(new Phrase("Location:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Location"], normalFont));
            section1.AddCell(new Phrase("Previous Issue:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Previous Issue"], normalFont));

            section1.AddCell(new Phrase("Description:", boldFont));
            PdfPCell cell = new PdfPCell(GetUnderlinedCell(data["Description"], normalFont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.Colspan = 3;
            section1.AddCell(cell);

            section1.AddCell(new Phrase("Question:", boldFont));
            cell = new PdfPCell(GetUnderlinedCell(data["Question"], normalFont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.Colspan = 3;
            section1.AddCell(cell);

            section1.AddCell(new Phrase("Sheets Affected List:", boldFont));
            cell = new PdfPCell(GetUnderlinedCell(data["Sheets Affected List"], normalFont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.Colspan = 3;
            section1.AddCell(cell);

            section1.AddCell(new Phrase("Proposed Mitigation:", boldFont));
            cell = new PdfPCell(GetUnderlinedCell(data["Proposed Mitigation"], normalFont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.Colspan = 3;
            section1.AddCell(cell);

            cell = new PdfPCell(GetUnderlinedCell("Attachments 15b", boldFont));
            cell.Border = 0;
            cell.Colspan = 4;
            section1.AddCell(cell);

            cell = new PdfPCell();
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.Colspan = 4;
            PdfPTable attachmentTable = GetAttachmentTable(data["ID"], "15b", boldFont, normalFont);
            cell.AddElement(attachmentTable);
            section1.AddCell(cell);

            table.AddCell(section1);
            return table;
        }

        private PdfPTable GetSection3(Dictionary<string, string> data, Font normalFont, Font boldFont, Font whiteboldFont)
        {
            PdfPTable table = new PdfPTable(1);
            table.SpacingBefore = 5;
            table.WidthPercentage = 100;
            table.AddCell(GetHeaderCell("Section 3", whiteboldFont));
            PdfPTable section1 = new PdfPTable(1);
            section1.DefaultCell.Border = 0;
            section1.SpacingBefore = 5; section1.SpacingAfter = 5;
            //section1.AddCell(new Phrase("Resolved in the field?:", boldFont));

            PdfPCell cell = new PdfPCell(GetUnderlinedCell("EOR Design Managers", boldFont));
            cell.Border = 0;
            section1.AddCell(cell);
            cell = new PdfPCell();
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            PdfPTable tbl = GetDMTable(data["ID"], boldFont, normalFont);
            cell.AddElement(tbl);
            section1.AddCell(cell);
            table.AddCell(section1);

            section1 = new PdfPTable(1);
            cell = new PdfPCell(GetUnderlinedCell("EOR Summary", boldFont));
            cell.Border = 0;
            section1.AddCell(cell);
            cell = new PdfPCell();
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            tbl = GetEORSummaryTable(data["ID"], boldFont, normalFont);
            cell.AddElement(tbl);
            section1.AddCell(cell);
            table.AddCell(section1);

            section1 = new PdfPTable(1);
            cell = new PdfPCell(GetUnderlinedCell("Attachments 23a", boldFont));
            cell.Border = 0;
            section1.AddCell(cell);
            cell = new PdfPCell();
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            tbl = GetAttachmentTable(data["ID"], "23a", boldFont, normalFont);
            cell.AddElement(tbl);
            section1.AddCell(cell);
            table.AddCell(section1);

            section1 = new PdfPTable(1);
            cell = new PdfPCell(GetUnderlinedCell("Approvers", boldFont));
            cell.Border = 0;
            section1.AddCell(cell);
            cell = new PdfPCell();
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            tbl = GetApproversTable(data["ID"], boldFont, normalFont);
            cell.AddElement(tbl);
            section1.AddCell(cell);

            table.AddCell(section1);
            return table;
        }

        private PdfPTable GetSection4(Dictionary<string, string> data, Font normalFont, Font boldFont, Font whiteboldFont)
        {
            PdfPTable table = new PdfPTable(1);
            table.SpacingBefore = 5;
            table.WidthPercentage = 100;
            table.AddCell(GetHeaderCell("Section 4", whiteboldFont));
            PdfPTable section1 = new PdfPTable(new float[] { 20f, 25f, 20f, 35f });
            section1.DefaultCell.Border = 0;
            section1.SpacingBefore = 5; section1.SpacingAfter = 5;
            section1.AddCell(new Phrase("Closed:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["Closed"], normalFont));
            section1.AddCell(new Phrase("NDC #:", boldFont));
            section1.AddCell(GetUnderlinedCell(data["NDC #"], normalFont));
            section1.AddCell(new Phrase("Remark:", boldFont));
            PdfPCell cell = new PdfPCell(GetUnderlinedCell(data["Remark"], normalFont));
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.Colspan = 3;
            section1.AddCell(cell);
            table.AddCell(section1);
            return table;

        }

        private PdfPTable GetEORSummaryTable(string id, Font boldFont, Font normalFont)
        {
            PdfPTable table = new PdfPTable(new float[] { 25f, 15f, 60f });
            table.WidthPercentage = 100;
            table.SpacingBefore = 5; table.SpacingAfter = 5;
            table.AddCell(new Phrase("EOR", boldFont));
            table.AddCell(new Phrase("Date", boldFont));
            table.AddCell(new Phrase("Response", boldFont));
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    SPWeb web = site.OpenWeb();
                    SPList list = web.Lists.TryGetList("RFIDesignStaffResponse");
                    SPQuery query = new SPQuery();
                    query.Query = string.Concat("<Where>",
                            "<And>",
                                "<Eq>",
                                    "<FieldRef Name='Title' /><Value Type='Text'>" + id + "</Value>",
                                "</Eq>",
                                "<IsNotNull><FieldRef Name='Response'></FieldRef></IsNotNull>",
                            "</And>",
                        "</Where>");
                    SPListItemCollection items = list.GetItems(query);
                    foreach (SPListItem item in items)
                    {
                        table.AddCell(new Phrase((new SPFieldLookupValue(Convert.ToString(item["EOR"]))).LookupValue, normalFont));
                        table.AddCell(new Phrase(((DateTime)item["ResponseDate"]).ToString("MM-dd-yyyy"), normalFont));
                        table.AddCell(new Phrase(Convert.ToString(item["Response"]), normalFont));
                    }
                }
            });
            return table;
        }

        private PdfPTable GetDMTable(string id, Font boldFont, Font normalFont)
        {
            PdfPTable table = new PdfPTable(new float[] { 50f, 50f });
            table.WidthPercentage = 100;
            table.SpacingBefore = 5; table.SpacingAfter = 5;
            table.AddCell(new Phrase("Design Manager", boldFont));
            table.AddCell(new Phrase("Assigned On", boldFont));
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    SPWeb web = site.OpenWeb();
                    SPList list = web.Lists.TryGetList("RFIApprovalMetrix");
                    SPQuery query = new SPQuery();
                    query.Query = string.Concat("<Where>",
                            "<And>",
                                "<Eq>",
                                    "<FieldRef Name='Title' /><Value Type='Text'>" + id + "</Value>",
                                "</Eq>",
                                "<Eq>",
                                    "<FieldRef Name='Role' /><Value Type='Text'>Design Manager</Value>",
                                "</Eq>",
                            "</And>",
                        "</Where>");
                    SPListItemCollection items = list.GetItems(query);
                    foreach (SPListItem item in items)
                    {
                        table.AddCell(new Phrase((new SPFieldLookupValue(Convert.ToString(item["Approver"]))).LookupValue, normalFont));
                        table.AddCell(new Phrase(((DateTime)item["AssignedDate"]).ToString("MM-dd-yyyy"), normalFont));
                    }
                }
            });
            return table;
        }

        private PdfPTable GetApproversTable(string id, Font boldFont, Font normalFont)
        {
            PdfPTable table = new PdfPTable(new float[] { 10f, 25f, 15f, 15f, 35f });
            table.WidthPercentage = 100;
            table.SpacingBefore = 5; table.SpacingAfter = 5;
            table.AddCell(new Phrase("Role", boldFont));
            table.AddCell(new Phrase("User", boldFont));
            table.AddCell(new Phrase("Action Date", boldFont));
            table.AddCell(new Phrase("Status", boldFont));
            table.AddCell(new Phrase("Remark", boldFont));
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    SPWeb web = site.OpenWeb();
                    SPList list = web.Lists.TryGetList("RFIApprovalMetrix");
                    SPQuery query = new SPQuery();
                    query.Query = string.Concat("<Where>",
                            "<And>",
                                "<Eq>",
                                    "<FieldRef Name='Title' /><Value Type='Text'>" + id + "</Value>",
                                "</Eq>",
                                "<Neq>",
                                    "<FieldRef Name='Role' /><Value Type='Text'>Design Manager</Value>",
                                "</Neq>",
                            "</And>",
                        "</Where>");
                    SPListItemCollection items = list.GetItems(query);
                    foreach (SPListItem item in items)
                    {

                        table.AddCell(new Phrase(Convert.ToString(item["Role"]), normalFont));
                        if ((Convert.ToString(item["Role"]).ToLower().Equals("qve") || Convert.ToString(item["Role"]).ToLower().Equals("acceptance")) && Convert.ToString(item["Status"]).Equals("Pending"))
                        {
                            table.AddCell(new Phrase("", normalFont));
                            table.AddCell(new Phrase("", normalFont));
                            table.AddCell(new Phrase("", normalFont));
                            table.AddCell(new Phrase("", normalFont));
                        }
                        else
                        {
                            table.AddCell(new Phrase((new SPFieldLookupValue(Convert.ToString(item["Approver"]))).LookupValue, normalFont));
                            table.AddCell(new Phrase(((DateTime)item["AssignedDate"]).ToString("MM-dd-yyyy"), normalFont));
                            table.AddCell(new Phrase(Convert.ToString(item["Status"]), normalFont));
                            table.AddCell(new Phrase(Convert.ToString(item["Remark"]), normalFont));
                        }
                        
                    }
                }
            });
            return table;
        }

        private PdfPTable GetAttachmentTable(string id, string type, Font boldFont, Font normalFont)
        {
            PdfPTable table = new PdfPTable(new float[] { 10f, 90f });
            table.WidthPercentage = 100;
            table.SpacingBefore = 5; table.SpacingAfter = 5;
            table.AddCell(new Phrase("#", boldFont));
            table.AddCell(new Phrase("File Name", boldFont));
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    SPWeb web = site.OpenWeb();
                    SPList list = web.Lists.TryGetList("RFIAttachments");
                    SPQuery query = new SPQuery();
                    query.Query = string.Concat("<Where>",
                            "<And>",
                                "<Eq>",
                                    "<FieldRef Name='ItemId' /><Value Type='Text'>" + id + "</Value>",
                                "</Eq>",
                                "<Eq>",
                                    "<FieldRef Name='AttachmentType' /><Value Type='Text'>" + type + "</Value>",
                                "</Eq>",
                            "</And>",
                        "</Where>");
                    SPListItemCollection items = list.GetItems(query);
                    int i = 1;
                    foreach (SPListItem item in items)
                    {
                        table.AddCell(new Phrase(i.ToString(), normalFont));
                        table.AddCell(new Phrase(item.Name.Substring(item.Name.LastIndexOf("_") + 1), normalFont));
                        i++;
                    }
                }
            });
            return table;
        }

        private List<string> DownloadAttachments(int id, string location)
        {
            List<string> files = new List<string>();
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(siteUrl))
                {
                    SPWeb web = site.OpenWeb();
                    SPList list = web.Lists.TryGetList("RFIAttachments");
                    SPQuery query = new SPQuery();
                    query.Query = string.Concat("<Where><Eq><FieldRef Name='ItemId' /><Value Type='Text'>", id, "</Value></Eq></Where><OrderBy><FieldRef Name='AttachmentType' Ascending='FALSE' /></OrderBy>");
                    
                    SPListItemCollection items = list.GetItems(query);
                    foreach (SPListItem item in items)
                    {
                        string filePath = location + item.Name;
                        System.IO.File.WriteAllBytes(filePath, item.File.OpenBinary());
                        files.Add(filePath);
                    }
                }
            });
            return files;
        }

        private void CombineMultiplePDFs(string[] fileNames, string outFile)
        {
            Document document = new Document();
            using (FileStream newFileStream = new FileStream(outFile, FileMode.Create))
            {
                PdfCopy writer = new PdfCopy(document, newFileStream);
                if (writer == null)
                {
                    return;
                }
                document.Open();
                foreach (string fileName in fileNames)
                {
                    PdfReader reader = new PdfReader(fileName);
                    reader.ConsolidateNamedDestinations();
                    for (int i = 1; i <= reader.NumberOfPages; i++)
                    {
                        PdfImportedPage page = writer.GetImportedPage(reader, i);
                        writer.AddPage(page);
                    }
                    reader.Close();
                }
                writer.Close();
                document.Close();
            }
        }
    }
}
