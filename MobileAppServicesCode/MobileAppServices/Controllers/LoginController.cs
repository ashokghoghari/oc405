﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.DirectoryServices.Protocols;
using Microsoft.SharePoint;

namespace MobileAppServices.Controllers
{
    public class LoginController : ApiController
    {
        // GET: Login
        [HttpGet]
        [Route("api/login")]
        public string Get()
        {
            return "Running up";
        }
        private const int ERROR_LOGON_FAILURE = 0x31;

        public bool ValidateCredentials(string username, string password, string domain)
        {
            NetworkCredential credentials = new NetworkCredential(username, password, domain);

            LdapDirectoryIdentifier id = new LdapDirectoryIdentifier(domain);

            using (LdapConnection connection = new LdapConnection(id, credentials, AuthType.Kerberos))
            {
                connection.SessionOptions.Sealing = true;
                connection.SessionOptions.Signing = true;

                try
                {
                    connection.Bind();
                }
                catch (LdapException lEx)
                {
                    if (ERROR_LOGON_FAILURE == lEx.ErrorCode)
                    {
                        return false;
                    }
                    throw;
                }
            }
            return true;
        }
        private Dictionary<string, object> Login(Param param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsValidUser"] = false;
            response["Message"] = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param.UserName) && !string.IsNullOrEmpty(param.Password))
                {
                    response["IsValidUser"] = ValidateCredentials(param.UserName, param.Password, "OC405");
                    if ((bool)response["IsValidUser"])
                    {

                        response["CurrentUserName"] = string.Empty;
                        using (var context = new PrincipalContext(ContextType.Domain, "OC405"))
                        {
                            UserPrincipal usr = UserPrincipal.FindByIdentity(context, param.UserName);
                            if (usr != null)
                            {
                                response["CurrentUserName"] = usr.GivenName;
                                response["CurrentUserEmail"] = usr.EmailAddress ?? string.Empty;
                                response["CurrentUserPhone"] = usr.VoiceTelephoneNumber ?? string.Empty;

                            }
                        }
                        response["Message"] = "Logged in Success";
                    }
                    else
                    {
                        response["Message"] = "Invalid Username/Password.";
                    }
                }
                else
                {
                    response["Message"] = "Username/Password can't be blank.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }

            return response;
        }
        private Boolean IsUserInGroup(SPGroup oGroupToTestFor, String sUserLoginName)
        {
            Boolean bUserIsInGroup = false;
            try
            {
                SPUser x = oGroupToTestFor.Users[sUserLoginName];
                bUserIsInGroup = true;
            }
            catch (SPException)
            {
                bUserIsInGroup = false;
            }
            return bUserIsInGroup;

        }
        [HttpPost]
        [Route("api/login")]
        public HttpResponseMessage Post(Param param)
        {
            if (string.IsNullOrEmpty(param.siteUrl))
            {
                param.siteUrl = "https://sharepoint.oc405.com/quality/pod/";
            }
            Dictionary<string, object> response = this.Login(param);
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite site = new SPSite(param.siteUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        SPUser objUser = web.EnsureUser(Convert.ToString(response["CurrentUserEmail"]));
                        try
                        {
                            SPGroup group = objUser.Groups["POD"];
                            
                        }
                        catch(Exception ex)
                        {
                            response["IsValidUser"] = false;
                            response["Message"] = "You are not authorised to use this application. Please Contact Administrator";
                        }
                        response["CurrentUserId"] = objUser.ID;
                    }
                }
            });
            HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        public class Param
        {
            public string siteUrl { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
        }
    }
}