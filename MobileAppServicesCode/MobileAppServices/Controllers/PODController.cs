﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Contract;
using Common;
using Microsoft.SharePoint.Client;
using System.Linq;

namespace MobileAppServices.Controllers
{
    public class PODController : ApiController
    {
        //string siteUrl = "http://oc405sp01:2018/quality/poddev";
        string siteUrl = "http://oc405sp01:2018/quality/pod";

        #region Compaction Test Request
        string compactionTestListName = "Compaction Test";
        [HttpPost]
        [Route("api/pod/SubmitCompactionTestRequest")]
        public HttpResponseMessage SubmitCompactionTestRequest(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                if (Helper.SaveListItems(siteUrl, param))
                {
                    response["IsSaved"] = true;
                    response["Message"] = "Form Saved Successfully.";
                }
                else
                {
                    response["Message"] = "Error while save data.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/GetCompactionTestRequests/{UserId}")]
        public ListInfo GetCompactionTestRequests(int UserId)
        {
            ListInfo listDetails = new ListInfo();
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                listDetails = new ListInfo();
                listDetails.ListName = compactionTestListName;
                listDetails.ListItemInfo = new List<ListItemInfo>();
                string query = "<Where><And><Eq><FieldRef Name='Author' LookupId='TRUE' /><Value Type='Integer'>" + UserId + "</Value></Eq><Neq><FieldRef Name='Status' /><Value Type='Text'>Cancel</Value></Neq></And></Where>< OrderBy><FieldRef Name='ID' Ascending='FALSE' /></OrderBy>";
                SPListItemCollection spListItemCollection = Helper.GetListItem(new Uri(siteUrl), listDetails.ListName, query, 5, string.Empty);
                foreach (SPListItem item in spListItemCollection)
                {
                    ListItemInfo newListItem = new ListItemInfo();
                    newListItem.ListPropInfo = new List<ListPropertyInfo>();
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "UnitID", PropertyTypeEnum.String));
                    listDetails.ListItemInfo.Add(newListItem);
                }
            });
            return listDetails;
        }

        [HttpGet]
        [Route("api/pod/GetCompactionTestRequest/{ItemId}")]
        public ListInfo GetCompactionTestRequest(int ItemId)
        {
            ListInfo listDetails = new ListInfo();
            listDetails.ListName = compactionTestListName;
            if (ItemId > 0)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList list = web.Lists[listDetails.ListName];
                            SPListItem item = list.GetItemById(ItemId);
                            ListItemInfo newListItem = new ListItemInfo();
                            newListItem.ListPropInfo = new List<ListPropertyInfo>();
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ID", PropertyValue = (item["ID"] == null ? "" : item["ID"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Title", PropertyValue = (item["Title"] == null ? "" : item["Title"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "UnitID", PropertyValue = (item["UnitID"] == null ? "" : item["UnitID"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "StationNumbers", PropertyValue = (item["StationNumbers"] == null ? "" : item["StationNumbers"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "TimeRequested", PropertyValue = (item["TimeRequested"] == null ? "" : item["TimeRequested"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Alignment", PropertyValue = (item["Alignment"] == null ? "" : item["Alignment"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Elevation", PropertyValue = (item["Elevation"] == null ? "" : item["Elevation"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "BackfillPlacementClassification", PropertyValue = (item["BackfillPlacementClassification"] == null ? "" : item["BackfillPlacementClassification"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "RequiredCompactionPercentage", PropertyValue = (item["RequiredCompactionPercentage"] == null ? "" : item["RequiredCompactionPercentage"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "SpecialNotes", PropertyValue = (item["SpecialNotes"] == null ? "" : item["SpecialNotes"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Lat", PropertyValue = (item["Lat"] == null ? "" : item["Lat"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Long", PropertyValue = (item["Long"] == null ? "" : item["Long"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Address", PropertyValue = (item["Address"] == null ? "" : item["Address"].ToString()) });
                            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Superintendent", PropertyTypeEnum.Choice));
                            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Foreman", PropertyTypeEnum.Choice));
                            listDetails.ListItemInfo = new List<ListItemInfo>();
                            listDetails.ListItemInfo.Add(newListItem);
                        }
                    }
                });
            }

            listDetails.dropdown = new List<GetDropdown>();
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "BackfillPlacementClassification"));
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "RequiredCompactionPercentage"));
            listDetails.dropdown.Add(Helper.GetSuperintendents("Superintendent"));
            listDetails.dropdown.Add(Helper.GetForemans("Foreman"));
            listDetails.dropdown.Add(Helper.GetTermStoreData(siteUrl, "Managed Metadata Service", "OC405 Codes", "UNIT ID", "UnitID"));
            listDetails.dropdown.Add(Helper.GetAlignments("Alignment"));
            return listDetails;
        }

        #endregion

        #region Concrete Placement

        string concretePlacementListName = "Concrete Placement";

        [HttpPost]
        [Route("api/pod/SubmitConcretePlacement")]
        public HttpResponseMessage SubmitConcretePlacement(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                if (Helper.SaveListItems(siteUrl, param))
                {
                    response["IsSaved"] = true;
                    response["Message"] = "Form Saved Successfully.";
                }
                else
                {
                    response["Message"] = "Error while save data.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/GetConcretePlacements/{UserId}")]
        public ListInfo GetConcretePlacements(int UserId)
        {
            ListInfo listDetails = new ListInfo();
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                listDetails = new ListInfo();
                listDetails.ListName = concretePlacementListName;
                listDetails.ListItemInfo = new List<ListItemInfo>();
                string query = "<Where><And><Eq><FieldRef Name='Author' LookupId='TRUE' /><Value Type='Integer'>" + UserId + "</Value></Eq><Neq><FieldRef Name='Status' /><Value Type='Text'>Cancel</Value></Neq></And></Where>< OrderBy><FieldRef Name='ID' Ascending='FALSE' /></OrderBy>";
                SPListItemCollection spListItemCollection = Helper.GetListItem(new Uri(siteUrl), listDetails.ListName, query, 5, string.Empty);
                foreach (SPListItem item in spListItemCollection)
                {
                    ListItemInfo newListItem = new ListItemInfo();
                    newListItem.ListPropInfo = new List<ListPropertyInfo>();
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "UnitID", PropertyTypeEnum.String));
                    listDetails.ListItemInfo.Add(newListItem);
                }
            });
            return listDetails;
        }

        [HttpGet]
        [Route("api/pod/GetConcretePlacement/{ItemId}")]
        public ListInfo GetConcretePlacement(int ItemId)
        {
            ListInfo listDetails = new ListInfo();
            listDetails.ListName = concretePlacementListName;
            if (ItemId > 0)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList list = web.Lists[listDetails.ListName];
                            SPListItem item = list.GetItemById(ItemId);
                            ListItemInfo newListItem = new ListItemInfo();
                            newListItem.ListPropInfo = new List<ListPropertyInfo>();
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ID", PropertyValue = (item["ID"] == null ? "" : item["ID"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Title", PropertyValue = (item["Title"] == null ? "" : item["Title"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Stationing", PropertyValue = (item["Stationing"] == null ? "" : item["Stationing"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "PlacementTime", PropertyValue = (item["PlacementTime"] == null ? "" : item["PlacementTime"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "MixDesignNumber", PropertyValue = (item["MixDesignNumber"] == null ? "" : item["MixDesignNumber"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "BatchPlantLocation", PropertyValue = (item["BatchPlantLocation"] == null ? "" : item["BatchPlantLocation"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "TotalYards", PropertyValue = (item["TotalYards"] == null ? "" : item["TotalYards"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Supplier", PropertyValue = (item["Supplier"] == null ? "" : item["Supplier"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "UnitID", PropertyValue = (item["UnitID"] == null ? "" : item["UnitID"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "SpecialNotes", PropertyValue = (item["SpecialNotes"] == null ? "" : item["SpecialNotes"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Lat", PropertyValue = (item["Lat"] == null ? "" : item["Lat"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Long", PropertyValue = (item["Long"] == null ? "" : item["Long"].ToString()) });
                            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Address", PropertyValue = (item["Address"] == null ? "" : item["Address"].ToString()) });
                            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Superintendent", PropertyTypeEnum.Choice));
                            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Foreman", PropertyTypeEnum.Choice));
                            listDetails.ListItemInfo = new List<ListItemInfo>();
                            listDetails.ListItemInfo.Add(newListItem);
                        }
                    }
                });
            }
            listDetails.dropdown = new List<GetDropdown>();
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "MixDesignNumber"));
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "BatchPlantLocation"));
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "Supplier"));
            listDetails.dropdown.Add(Helper.GetSuperintendents("Superintendent"));
            listDetails.dropdown.Add(Helper.GetForemans("Foreman"));
            listDetails.dropdown.Add(Helper.GetTermStoreData(siteUrl, "Managed Metadata Service", "OC405 Codes", "UNIT ID", "UnitID"));
            return listDetails;
        }
        #endregion

        #region CAP Inspection
        string capListName = "CAP Inspection";

        [HttpPost]
        [Route("api/pod/cap/items/add")]
        public HttpResponseMessage SubmitCAP(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                if (Helper.SaveListItems(siteUrl, param))
                {
                    response["IsSaved"] = true;
                    response["Message"] = "Form Saved Successfully.";
                }
                else
                {
                    response["Message"] = "Error while save data.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/cap/items({UserId})")]
        public ListInfo GetCAPs(int UserId)
        {
            ListInfo listDetails = new ListInfo();
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                listDetails = new ListInfo();
                listDetails.ListName = capListName;
                listDetails.ListItemInfo = new List<ListItemInfo>();
                string query = "<Where><And><Eq><FieldRef Name='Author' LookupId='TRUE' /><Value Type='Integer'>" + UserId + "</Value></Eq><Neq><FieldRef Name='Status' /><Value Type='Text'>Cancel</Value></Neq></And></Where>< OrderBy><FieldRef Name='ID' Ascending='FALSE' /></OrderBy>";
                SPListItemCollection spListItemCollection = Helper.GetListItem(new Uri(siteUrl), listDetails.ListName, query, 5, string.Empty);
                foreach (SPListItem item in spListItemCollection)
                {
                    ListItemInfo newListItem = new ListItemInfo();
                    newListItem.ListPropInfo = new List<ListPropertyInfo>();
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
                    listDetails.ListItemInfo.Add(newListItem);
                }
            });
            return listDetails;
        }

        [HttpGet]
        [Route("api/pod/cap/item({ItemId})")]
        public ListInfo GetCAP(int ItemId)
        {
            ListInfo listDetails = new ListInfo();
            listDetails.ListName = capListName;
            SPListItem item = null;
            if (ItemId > 0)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList list = web.Lists[listDetails.ListName];
                            item = list.GetItemById(ItemId);
                        }
                    }
                });
            }
            ListItemInfo newListItem = new ListItemInfo();
            newListItem.ListPropInfo = new List<ListPropertyInfo>();
            
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "CAPForm", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "StartTime", PropertyTypeEnum.DateTime));
            newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Alignment", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Stationing", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "GeoTechRequired", PropertyTypeEnum.Boolean));
            newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "Discipline", PropertyTypeEnum.Metadata));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Description", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "SpecialNotes", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Lat", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Long", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Superintendent", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Foreman", PropertyTypeEnum.Choice));

            listDetails.ListItemInfo = new List<ListItemInfo>();
            listDetails.ListItemInfo.Add(newListItem);
            listDetails.dropdown = new List<GetDropdown>();
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "CAP Form"));
            listDetails.dropdown.Add(Helper.GetAlignments("Alignment"));
            listDetails.dropdown.Add(Helper.GetSuperintendents("Superintendent"));
            listDetails.dropdown.Add(Helper.GetForemans("Foreman"));
            listDetails.dropdown.Add(Helper.GetTermStoreDataWithValue(siteUrl, "Managed Metadata Service", "OC405 Codes", "UNIT ID", "UnitID"));
            listDetails.dropdown.Add(Helper.GetTermStoreDataWithValue(siteUrl, "Managed Metadata Service", "OC405 Codes", "Discipline", "Discipline"));
            return listDetails;
            {
                /*newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ID", PropertyValue = (item == null || item["ID"] == null ? "" : item["ID"].ToString()), PropertyType = PropertyTypeEnum.ReadOnly });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Title", PropertyValue = (item == null || item["Title"] == null ? "" : item["Title"].ToString()), PropertyType = PropertyTypeEnum.String });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "CAPForm", PropertyValue = (item == null || item["CAPForm"] == null ? "" : item["CAPForm"].ToString()), PropertyType = PropertyTypeEnum.Choice });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "StartTime", PropertyValue = (item == null || item["StartTime"] == null ? "" : item["StartTime"].ToString()), PropertyType = PropertyTypeEnum.DateTime });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "UnitID", PropertyValue = (item == null || item["UnitID"] == null ? "" : Helper.GetTermLabel(item["UnitID"])), PropertyType = PropertyTypeEnum.Metadata });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Alignment", PropertyValue = (item == null || item["Alignment"] == null ? "" : item["Alignment"].ToString()), PropertyType = PropertyTypeEnum.String });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Stationing", PropertyValue = (item == null || item["Stationing"] == null ? "" : item["Stationing"].ToString()), PropertyType = PropertyTypeEnum.String });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "GeoTechRequired", PropertyValue = (item == null || item["GeoTechRequired"] == null ? "" : item["GeoTechRequired"].ToString()), PropertyType = PropertyTypeEnum.Boolean });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Discipline", PropertyValue = (item == null || item["Discipline"] == null ? "" : Helper.GetTermLabel(item["Discipline"])), PropertyType = PropertyTypeEnum.Metadata });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Description", PropertyValue = (item == null || item["Description"] == null ? "" : item["Description"].ToString()), PropertyType = PropertyTypeEnum.String });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "SpecialNotes", PropertyValue = (item == null || item["SpecialNotes"] == null ? "" : item["SpecialNotes"].ToString()), PropertyType = PropertyTypeEnum.String });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Lat", PropertyValue = (item == null || item["Lat"] == null ? "" : item["Lat"].ToString()), PropertyType = PropertyTypeEnum.String });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Long", PropertyValue = (item == null || item["Long"] == null ? "" : item["Long"].ToString()), PropertyType = PropertyTypeEnum.String });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Address", PropertyValue = (item == null || item["Address"] == null ? "" : item["Address"].ToString()), PropertyType = PropertyTypeEnum.String });
                newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Engineer", PropertyValue = (item == null || item["Engineer"] == null ? "" : Helper.GetPeitem["Engineer"].ToString()), PropertyType = PropertyTypeEnum.PersonOrGroup });*/
            }
        }
        #endregion

        #region Asphalt Placement
        string asphaltPlacementListName = "Asphalt Placement";

        [HttpPost]
        [Route("api/pod/asphaltplacement/items/add")]
        public HttpResponseMessage SubmitAsphaltPlacement(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                if (Helper.SaveListItems(siteUrl, param))
                {
                    response["IsSaved"] = true;
                    response["Message"] = "Form Saved Successfully.";
                }
                else
                {
                    response["Message"] = "Error while save data.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/asphaltplacement/items({UserId})")]
        public ListInfo GetAsphaltPlacements(int UserId)
        {
            ListInfo listDetails = new ListInfo();
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                listDetails = new ListInfo();
                listDetails.ListName = asphaltPlacementListName;
                listDetails.ListItemInfo = new List<ListItemInfo>();
                string query = "<Where><And><Eq><FieldRef Name='Author' LookupId='TRUE' /><Value Type='Integer'>" + UserId + "</Value></Eq><Neq><FieldRef Name='Status' /><Value Type='Text'>Cancel</Value></Neq></And></Where>< OrderBy><FieldRef Name='ID' Ascending='FALSE' /></OrderBy>";
                SPListItemCollection spListItemCollection = Helper.GetListItem(new Uri(siteUrl), listDetails.ListName, query, 5, string.Empty);
                foreach (SPListItem item in spListItemCollection)
                {
                    ListItemInfo newListItem = new ListItemInfo();
                    newListItem.ListPropInfo = new List<ListPropertyInfo>();
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
                    listDetails.ListItemInfo.Add(newListItem);
                }
            });
            return listDetails;
        }

        [HttpGet]
        [Route("api/pod/asphaltplacement/item({ItemId})")]
        public ListInfo GetAsphaltPlacement(int ItemId)
        {
            ListInfo listDetails = new ListInfo();
            listDetails.ListName = asphaltPlacementListName;
            SPListItem item = null;
            if (ItemId > 0)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList list = web.Lists[listDetails.ListName];
                            item = list.GetItemById(ItemId);
                        }
                    }
                });
            }
            ListItemInfo newListItem = new ListItemInfo();
            newListItem.ListPropInfo = new List<ListPropertyInfo>();
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ID", PropertyValue = (item == null || item["ID"] == null ? "" : item["ID"].ToString()), PropertyType = PropertyTypeEnum.ReadOnly });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Title", PropertyValue = (item == null || item["Title"] == null ? "" : item["Title"].ToString()), PropertyType = PropertyTypeEnum.String });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "StartTime", PropertyValue = (item == null || item["StartTime"] == null ? "" : item["StartTime"].ToString()), PropertyType = PropertyTypeEnum.DateTime });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "UnitID", PropertyValue = (item == null || item["UnitID"] == null ? "" : Helper.GetTermLabel(item["UnitID"])), PropertyType = PropertyTypeEnum.Metadata });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Alignment", PropertyValue = (item == null || item["Alignment"] == null ? "" : item["Alignment"].ToString()), PropertyType = PropertyTypeEnum.String });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Stationing", PropertyValue = (item == null || item["Stationing"] == null ? "" : item["Stationing"].ToString()), PropertyType = PropertyTypeEnum.String });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ACSupplier", PropertyValue = (item == null || item["ACSupplier"] == null ? "" : item["ACSupplier"].ToString()), PropertyType = PropertyTypeEnum.Choice });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ACBatchPlant", PropertyValue = (item == null || item["ACBatchPlant"] == null ? "" : item["ACBatchPlant"].ToString()), PropertyType = PropertyTypeEnum.Choice });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "ACMixDesignNumber", PropertyValue = (item == null || item["ACMixDesignNumber"] == null ? "" : item["ACMixDesignNumber"].ToString()), PropertyType = PropertyTypeEnum.Choice });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "TotalTonnage", PropertyValue = (item == null || item["TotalTonnage"] == null ? "" : item["TotalTonnage"].ToString()), PropertyType = PropertyTypeEnum.String });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "SpecialNotes", PropertyValue = (item == null || item["SpecialNotes"] == null ? "" : item["SpecialNotes"].ToString()), PropertyType = PropertyTypeEnum.String });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Lat", PropertyValue = (item == null || item["Lat"] == null ? "" : item["Lat"].ToString()), PropertyType = PropertyTypeEnum.String });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Long", PropertyValue = (item == null || item["Long"] == null ? "" : item["Long"].ToString()), PropertyType = PropertyTypeEnum.String });
            newListItem.ListPropInfo.Add(new ListPropertyInfo() { PropertyName = "Address", PropertyValue = (item == null || item["Address"] == null ? "" : item["Address"].ToString()), PropertyType = PropertyTypeEnum.String });
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Superintendent", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Foreman", PropertyTypeEnum.Choice));
            listDetails.ListItemInfo = new List<ListItemInfo>();
            listDetails.ListItemInfo.Add(newListItem);
            listDetails.dropdown = new List<GetDropdown>();
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "AC Supplier"));
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "AC Batch Plant"));
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "AC Mix Design Number"));
            listDetails.dropdown.Add(Helper.GetAlignments("Alignment"));
            listDetails.dropdown.Add(Helper.GetSuperintendents("Superintendent"));
            listDetails.dropdown.Add(Helper.GetForemans("Foreman"));
            listDetails.dropdown.Add(Helper.GetTermStoreDataWithValue(siteUrl, "Managed Metadata Service", "OC405 Codes", "UNIT ID", "UnitID"));
            return listDetails;
        }
        #endregion

        #region Welding Inspection
        string weldingInspectionListName = "Welding Inspection";

        [HttpPost]
        [Route("api/pod/weldinginspection/items/add")]
        public HttpResponseMessage SubmitWeldingInspection(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                if (Helper.SaveListItems(siteUrl, param))
                {
                    response["IsSaved"] = true;
                    response["Message"] = "Form Saved Successfully.";
                }
                else
                {
                    response["Message"] = "Error while save data.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/weldinginspection/items({UserId})")]
        public ListInfo GetWeldinginspections(int UserId)
        {
            ListInfo listDetails = new ListInfo();
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                listDetails = new ListInfo();
                listDetails.ListName = weldingInspectionListName;
                listDetails.ListItemInfo = new List<ListItemInfo>();
                string query = "<Where><And><Eq><FieldRef Name='Author' LookupId='TRUE' /><Value Type='Integer'>" + UserId + "</Value></Eq><Neq><FieldRef Name='Status' /><Value Type='Text'>Cancel</Value></Neq></And></Where>< OrderBy><FieldRef Name='ID' Ascending='FALSE' /></OrderBy>";
                SPListItemCollection spListItemCollection = Helper.GetListItem(new Uri(siteUrl), listDetails.ListName, query, 5, string.Empty);
                foreach (SPListItem item in spListItemCollection)
                {
                    ListItemInfo newListItem = new ListItemInfo();
                    newListItem.ListPropInfo = new List<ListPropertyInfo>();
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
                    listDetails.ListItemInfo.Add(newListItem);
                }
            });
            return listDetails;
        }

        [HttpGet]
        [Route("api/pod/weldinginspection/item({ItemId})")]
        public ListInfo GetWeldinginspection(int ItemId)
        {
            ListInfo listDetails = new ListInfo();
            listDetails.ListName = weldingInspectionListName;
            SPListItem item = null;
            if (ItemId > 0)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList list = web.Lists[listDetails.ListName];
                            item = list.GetItemById(ItemId);
                        }
                    }
                });
            }
            ListItemInfo newListItem = new ListItemInfo();
            newListItem.ListPropInfo = new List<ListPropertyInfo>();
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ArrivalTime", PropertyTypeEnum.DateTime));
            newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Stationing", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Duration", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ContractType", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "IsPermanent", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Description", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "SpecialNotes", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Lat", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Long", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Superintendent", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Foreman", PropertyTypeEnum.Choice));
            listDetails.ListItemInfo = new List<ListItemInfo>();
            listDetails.ListItemInfo.Add(newListItem);
            listDetails.dropdown = new List<GetDropdown>();
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "Contractor/Sub-Contractor"));
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "Permanent/Temporary"));
            listDetails.dropdown.Add(Helper.GetSuperintendents("Superintendent"));
            listDetails.dropdown.Add(Helper.GetForemans("Foreman"));
            listDetails.dropdown.Add(Helper.GetTermStoreDataWithValue(siteUrl, "Managed Metadata Service", "OC405 Codes", "UNIT ID", "UnitID"));
            return listDetails;
        }
        #endregion

        #region Geotechnical Request
        string GTRListName = "Geotechnical Request";

        [HttpPost]
        [Route("api/pod/gtr/items/add")]
        public HttpResponseMessage SubmitGTR(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                if (Helper.SaveListItems(siteUrl, param))
                {
                    response["IsSaved"] = true;
                    response["Message"] = "Form Saved Successfully.";
                }
                else
                {
                    response["Message"] = "Error while save data.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/gtr/items({UserId})")]
        public ListInfo GetGTRs(int UserId)
        {
            return GetListItems(UserId, GTRListName);
        }

        [HttpGet]
        [Route("api/pod/gtr/item({ItemId})")]
        public ListInfo GetGTR(int ItemId)
        {
            ListInfo listDetails = new ListInfo();
            listDetails.ListName = GTRListName;
            SPListItem item = null;
            if (ItemId > 0)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList list = web.Lists[listDetails.ListName];
                            item = list.GetItemById(ItemId);
                        }
                    }
                });
            }
            ListItemInfo newListItem = new ListItemInfo();
            newListItem.ListPropInfo = new List<ListPropertyInfo>();

            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "StartTime", PropertyTypeEnum.DateTime));
            newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Alignment", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Stationing", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ExcavationType", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "CAPParticipation", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Description", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "SpecialNotes", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Lat", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Long", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Superintendent", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Foreman", PropertyTypeEnum.Choice));

            listDetails.ListItemInfo = new List<ListItemInfo>();
            listDetails.ListItemInfo.Add(newListItem);
            listDetails.dropdown = new List<GetDropdown>();
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "Excavation Type"));
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "CAP Participation"));
            listDetails.dropdown.Add(Helper.GetAlignments("Alignment"));
            listDetails.dropdown.Add(Helper.GetSuperintendents("Superintendent"));
            listDetails.dropdown.Add(Helper.GetForemans("Foreman"));
            listDetails.dropdown.Add(Helper.GetTermStoreDataWithValue(siteUrl, "Managed Metadata Service", "OC405 Codes", "UNIT ID", "UnitID"));
            return listDetails;
            
        }
        #endregion

        #region Environmental Request
        string ERListName = "Environmental Request";

        [HttpPost]
        [Route("api/pod/er/items/add")]
        public HttpResponseMessage SubmitER(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                if (Helper.SaveListItems(siteUrl, param))
                {
                    response["IsSaved"] = true;
                    response["Message"] = "Form Saved Successfully.";
                }
                else
                {
                    response["Message"] = "Error while save data.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/er/items({UserId})")]
        public ListInfo GetERs(int UserId)
        {
            return GetListItems(UserId, ERListName);
        }

        [HttpGet]
        [Route("api/pod/er/item({ItemId})")]
        public ListInfo GetER(int ItemId)
        {
            ListInfo listDetails = new ListInfo();
            listDetails.ListName = ERListName;
            SPListItem item = null;
            if (ItemId > 0)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList list = web.Lists[listDetails.ListName];
                            item = list.GetItemById(ItemId);
                        }
                    }
                });
            }
            ListItemInfo newListItem = new ListItemInfo();
            newListItem.ListPropInfo = new List<ListPropertyInfo>();

            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "StartTime", PropertyTypeEnum.DateTime));
            newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Alignment", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Stationing", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "EnvironmentalType", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Description", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "SpecialNotes", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Lat", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Long", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Superintendent", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Foreman", PropertyTypeEnum.Choice));

            listDetails.ListItemInfo = new List<ListItemInfo>();
            listDetails.ListItemInfo.Add(newListItem);
            listDetails.dropdown = new List<GetDropdown>();
            listDetails.dropdown.Add(Helper.GetChoiceData(siteUrl, listDetails.ListName, "Environmental Type"));
            listDetails.dropdown.Add(Helper.GetAlignments("Alignment"));
            listDetails.dropdown.Add(Helper.GetSuperintendents("Superintendent"));
            listDetails.dropdown.Add(Helper.GetForemans("Foreman"));
            listDetails.dropdown.Add(Helper.GetTermStoreDataWithValue(siteUrl, "Managed Metadata Service", "OC405 Codes", "UNIT ID", "UnitID"));
            return listDetails;

        }
        #endregion

        #region Material Delivery
        string materialDeliveryListName = "Material Delivery";

        [HttpPost]
        [Route("api/pod/materialdelivery/items/add")]
        public HttpResponseMessage SubmitMaterialDelivery(AppServiceParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                if (Helper.SaveListItems(siteUrl, param))
                {
                    response["IsSaved"] = true;
                    response["Message"] = "Form Saved Successfully.";
                }
                else
                {
                    response["Message"] = "Error while save data.";
                }
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/materialdelivery/items({UserId})")]
        public ListInfo GetMaterialDeliveries(int UserId)
        {
            ListInfo listDetails = new ListInfo();
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                listDetails = new ListInfo();
                listDetails.ListName = materialDeliveryListName;
                listDetails.ListItemInfo = new List<ListItemInfo>();
                string query = "<Where><And><Eq><FieldRef Name='Author' LookupId='TRUE' /><Value Type='Integer'>" + UserId + "</Value></Eq><Neq><FieldRef Name='Status' /><Value Type='Text'>Cancel</Value></Neq></And></Where>< OrderBy><FieldRef Name='ID' Ascending='FALSE' /></OrderBy>";
                SPListItemCollection spListItemCollection = Helper.GetListItem(new Uri(siteUrl), listDetails.ListName, query, 5, string.Empty);
                foreach (SPListItem item in spListItemCollection)
                {
                    ListItemInfo newListItem = new ListItemInfo();
                    newListItem.ListPropInfo = new List<ListPropertyInfo>();
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
                    listDetails.ListItemInfo.Add(newListItem);
                }
            });
            return listDetails;
        }

        [HttpGet]
        [Route("api/pod/materialdelivery/item({ItemId})")]
        public ListInfo GetMaterialDelivery(int ItemId)
        {
            ListInfo listDetails = new ListInfo();
            listDetails.ListName = materialDeliveryListName;
            SPListItem item = null;
            if (ItemId > 0)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList list = web.Lists[listDetails.ListName];
                            item = list.GetItemById(ItemId);
                        }
                    }
                });
            }
            ListItemInfo newListItem = new ListItemInfo();
            newListItem.ListPropInfo = new List<ListPropertyInfo>();
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ArrivalTime", PropertyTypeEnum.DateTime));
            newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Stationing", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Quantity", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Description", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "SpecialNotes", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "StorageLat", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "StorageLong", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "StorageAddress", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Lat", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Long", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Superintendent", PropertyTypeEnum.Choice));
            newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Foreman", PropertyTypeEnum.Choice));
            listDetails.ListItemInfo = new List<ListItemInfo>();
            listDetails.ListItemInfo.Add(newListItem);
            listDetails.dropdown = new List<GetDropdown>();
            listDetails.dropdown.Add(Helper.GetSuperintendents("Superintendent"));
            listDetails.dropdown.Add(Helper.GetForemans("Foreman"));
            listDetails.dropdown.Add(Helper.GetTermStoreDataWithValue(siteUrl, "Managed Metadata Service", "OC405 Codes", "UNIT ID", "UnitID"));
            return listDetails;
        }
        #endregion

        #region Other
        [HttpGet]
        [Route("api/pod")]
        public string Get()
        {
            return "Running up";
        }

        private bool SendMailNotification(string subject, string body, string listname)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            web.AllowUnsafeUpdates = true;

                            //foreach (ListInfo nextList in param.ListInfo)
                            SPList list = web.Lists.TryGetList(listname);
                            if (list != null)
                            {
                                SPListItem newItem = list.Items.Add();
                                newItem["Title"] = subject;
                                newItem["MailBody"] = body;
                                newItem.Update();
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool SendConcretePlacementMail(string url, SPListItem item)
        {
            using (SPSite site = new SPSite(url)) //Site collection URL 
            {
                using (SPWeb web = site.OpenWeb()) //Subsite URL 
                {
                    SPGroup group = web.SiteGroups.GetByID(387);
                    string toUsers = "";
                    foreach (SPUser user in group.Users)
                    {
                        toUsers = user.Email + "," + toUsers;
                    }
                    toUsers.Trim(',');
                    StringDictionary headers = new StringDictionary();
                    headers.Add("from", "sharepoint@oc405.com");
                    headers.Add("to", toUsers);
                    headers.Add("subject", "Concrete Placement request on " + Convert.ToString(item["PlacementTime"]));
                    headers.Add("fAppendHtmlTag", "True"); //To enable HTML format 
                    System.Text.StringBuilder strMessage = new System.Text.StringBuilder();
                    strMessage.Append("<span>Hello,</span>");
                    strMessage.Append("<span>Please find below are Concrete Placement request details</span>");
                    strMessage.Append("<span>Batch Plant Location: " + Convert.ToString(item["BatchPlantLocation"]) + "</span>");
                    strMessage.Append("<span>Placement Time: " + Convert.ToString(item["PlacementTime"]) + "</span>");
                    strMessage.Append("<a href='" + siteUrl + "/Lists/ConcretePlacement/DispForm.aspx?ID=" + item.ID + "'>Click here</a> to view request");
                    SPUtility.SendEmail(web, headers, strMessage.ToString());
                }
            }
            return true;
        }
        #endregion

        #region Common

        [HttpGet]
        [Route("api/pod/Key")]
        public HttpResponseMessage GetGoogleMapKey()
        {
            HttpResponseMessage response = null;
            Dictionary<string, object> data = new Dictionary<string, object>();
            data["mapApiKey"] = "AIzaSyA0Vi3f9aNKmB2j_UAe0o3zjaiydvJmyeU";
            response = Request.CreateResponse(HttpStatusCode.OK, data);
            return response;
        }

        [HttpPost]
        [Route("api/pod/Login")]
        public HttpResponseMessage Post(LoginParam param)
        {
            HttpResponseMessage response = null;
            Dictionary<string, object> data = new Dictionary<string, object>();
            data["IsValidUser"] = false;
            if (!string.IsNullOrEmpty(param.UserName) && !string.IsNullOrEmpty(param.Password))
            {
                try
                {
                    using (ClientContext context = new ClientContext(siteUrl))
                    {
                        context.Credentials = new NetworkCredential(param.UserName, param.Password, "OC405");
                        Web web = context.Web;
                        context.Load(web);
                        User currentUser = web.CurrentUser;
                        context.Load(currentUser);
                        context.ExecuteQuery();
                        try
                        {
                            Microsoft.SharePoint.Client.Group groups = currentUser.Groups.GetByName("POD");
                            context.Load(groups);
                            context.ExecuteQuery();
                            data["IsValidUser"] = true;
                            data["Message"] = "Logged in Success";
                            data["CurrentUserName"] = currentUser.Title;
                            data["CurrentUserEmail"] = currentUser.Email;
                            data["CurrentUserId"] = currentUser.Id;
                        }
                        catch (Exception ex)
                        {
                            data["Message"] = "You are not authorised to use this application. Please Contact Administrator";
                        }
                    }
                }
                catch (Exception ex)
                {
                    data["Message"] = "Invalid Username/Password.";
                }
            }
            else
            {
                data["Message"] = "Username/Password can't be blank.";
            }
            response = Request.CreateResponse(HttpStatusCode.OK, data);
            return response;
        }

        [HttpPost]
        [Route("api/pod/CancelRequest")]
        public HttpResponseMessage CancelRequest(CancelRequestParam param)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["IsSaved"] = false;
            HttpResponseMessage result = null;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            web.AllowUnsafeUpdates = true;
                            SPList list = web.Lists[param.ListName];
                            if (!string.IsNullOrEmpty(param.ItemId))
                            {
                                int itemId = Convert.ToInt32(param.ItemId);
                                SPListItem item = list.GetItemById(itemId);
                                item["Status"] = "Cancel";
                                item.SystemUpdate();
                                WorkflowHelper.StartWorkflow(siteUrl, list.ID, itemId);
                                response["IsSaved"] = true;
                                response["Message"] = "Request cancelled successfully.";
                            }
                            web.AllowUnsafeUpdates = false;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                response["Message"] = ex.Message;
            }
            result = Request.CreateResponse(HttpStatusCode.OK, response);
            return result;
        }

        [HttpGet]
        [Route("api/pod/getschedules")]
        public PODRequests GetSchedules()
        {
            PODRequests allschedules = new PODRequests();
            List<PODRequest> allPODs = new List<PODRequest>();
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        SPWeb web = site.OpenWeb();
                        List<PODRequest> pods = null;
                        pods = Helper.GetPODs(web, "Concrete Placement", "PlacementTime");
                        allPODs.AddRange(pods);
                        pods = Helper.GetPODs(web, "Compaction Test", "TimeRequested");
                        allPODs.AddRange(pods);
                        pods = Helper.GetPODs(web, "CAP Inspection", "StartTime");
                        allPODs.AddRange(pods);
                        pods = Helper.GetPODs(web, "Asphalt Placement", "StartTime");
                        allPODs.AddRange(pods);
                        pods = Helper.GetPODs(web, "Welding Inspection", "ArrivalTime");
                        allPODs.AddRange(pods);
                        pods = Helper.GetPODs(web, "Geotechnical Request", "StartTime");
                        allPODs.AddRange(pods);
                        pods = Helper.GetPODs(web, "Environmental Request", "StartTime");
                        allPODs.AddRange(pods);
                        pods = Helper.GetPODs(web, "Material Delivery", "ArrivalTime");
                        allPODs.AddRange(pods);
                        allPODs = allPODs.OrderBy(x => x.Time).ToList();
                        allschedules.data = allPODs;
                    }
                });

            }
            catch (Exception ex)
            {
            }
            return allschedules;
        }

        [HttpGet]
        [Route("api/pod/version({version})")]
        public HttpResponseMessage GetVersion(string version)
        {
            HttpResponseMessage response = null;
            Dictionary<string, object> data = new Dictionary<string, object>();
            if (!version.Equals("0"))
            {
                data["IsValidVersion"] = true;
                data["Message"] = "Please go ahead...";
            }
            else
            {
                data["IsValidVersion"] = false;
                data["Message"] = "Please update app from App Store...";
            }
            
            response = Request.CreateResponse(HttpStatusCode.OK, data);
            return response;
        }

        [HttpGet]
        [Route("api/pod/items({UserId},{ListName})")]
        public ListInfo GetListItems(int UserId, string ListName)
        {
            ListInfo listDetails = new ListInfo();
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                listDetails = new ListInfo();
                listDetails.ListName = ListName;
                listDetails.ListItemInfo = new List<ListItemInfo>();
                string query = "<Where><And><Eq><FieldRef Name='Author' LookupId='TRUE' /><Value Type='Integer'>" + UserId + "</Value></Eq><Neq><FieldRef Name='Status' /><Value Type='Text'>Cancel</Value></Neq></And></Where>< OrderBy><FieldRef Name='ID' Ascending='FALSE' /></OrderBy>";
                SPListItemCollection spListItemCollection = Helper.GetListItem(new Uri(siteUrl), listDetails.ListName, query, 5, string.Empty);
                foreach (SPListItem item in spListItemCollection)
                {
                    ListItemInfo newListItem = new ListItemInfo();
                    newListItem.ListPropInfo = new List<ListPropertyInfo>();
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "ID", PropertyTypeEnum.ReadOnly));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Title", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetStringListPropertyInfo(item, "Address", PropertyTypeEnum.String));
                    newListItem.ListPropInfo.Add(Helper.GetMetaLableListPropertyInfo(item, "UnitID", PropertyTypeEnum.Metadata));
                    listDetails.ListItemInfo.Add(newListItem);
                }
            });
            return listDetails;
        }
        #endregion

    }
}
