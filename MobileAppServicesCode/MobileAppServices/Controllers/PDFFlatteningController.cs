﻿using Microsoft.SharePoint;
using Syncfusion.Pdf.Parsing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAppServices.Controllers
{
    public class PDFFlatteningController : ApiController
    {
        [HttpGet]
        [Route("api/pdfflattening")]
        public string Get()
        {
            return "Running up";
        }
        [HttpGet]
        [Route("api/pdfflattening/{itemId}")]
        public bool PDFFlattening(int itemId)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate () {
                using (SPSite site = new SPSite("https://sharepoint.oc405.com/pp"))
                {
                    SPWeb web = site.OpenWeb();
                    web.AllowUnsafeUpdates = true;
                    SPList sourceList = web.Lists.TryGetList("AESCOSource");
                    SPList targetList = web.Lists.TryGetList("AESCOTarget");
                    SPListItem sourceItem = sourceList.GetItemById(itemId);
                    SPFile file = sourceItem.File;
                    if (file != null)
                    {

                        // retrieve the file as a byte array byte[] bArray = file.OpenBinary(); 
                        string filePath = Path.Combine("C:\\PDFFlattening", "old_" + file.Name);
                        //open the file stream and write the file 
                        using (FileStream filestream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                        {
                            byte[] bArray = file.OpenBinary();
                            filestream.Write(bArray, 0, bArray.Length);
                        }
                        PdfLoadedDocument doc = new PdfLoadedDocument(filePath);
                        PdfLoadedForm form = doc.Form;
                        doc.Form.Flatten = true;
                        //doc.Security.OwnerPassword = "owner";
                        doc.Security.Permissions = Syncfusion.Pdf.Security.PdfPermissionsFlags.Default;
                        filePath = Path.Combine("C:\\PDFFlattening", file.Name);
                        doc.Save(filePath);
                        doc.Close();

                        SPFile spfile = targetList.RootFolder.Files.Add(filePath.Substring(filePath.LastIndexOf("\\") + 1), File.ReadAllBytes(filePath),true);
                        spfile.Item["Title"] = "Uploaded";
                        spfile.Item.Update();
                    }
                }
            });
            return true;
        }
    }
}
