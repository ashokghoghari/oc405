﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAppServices
{
    public class VideoItem
    {
        public string Title { get; set; }
        public string Category { get; set; }
        public string Files { get; set; }
        public string Location { get; set; }
        public string CurrentuserEmail { get; set; }
    }
}