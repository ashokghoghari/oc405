﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contract
{
    public class AppServiceParam
    {
        public List<ListInfo> ListInfo { get; set; }
    }

    public class ListInfo
    {
        public string ListName { get; set; }

        public List<ListItemInfo> ListItemInfo { get; set; }
        public List<GetDropdown> dropdown { get; set; }
    }
    
    public class GetDropdown
    {
        public string fieldName { get; set; }
        public List<DropDownData> value { get; set; }
    }
    public class DropDownData
    {
        public string value { get; set; }
        public string key { get; set; }
    }
    public class ListItemInfo
    {
        public List<ListPropertyInfo> ListPropInfo { get; set; }
    }

    public class ListPropertyInfo
    {
        public string PropertyName { get; set; }

        public object PropertyValue { get; set; }

        public PropertyTypeEnum PropertyType { get; set; }
    }

    public class FilePropertyInfo
    {
        public string Name { get; set; }
        public string FileBase64 { get; set; }
    }

    public enum PropertyTypeEnum
    {
        File,
        Number,
        Boolean,
        DateTime,
        String,
        PersonOrGroup,
        MultiPersonOrGroup,
        LookUp,
        MultiLookUp,
        Choice,
        MultiChoice,
        Metadata,
        MultiMetadata,
        ReadOnly
    }
}