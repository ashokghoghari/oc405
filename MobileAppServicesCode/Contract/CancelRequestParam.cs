﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    public class CancelRequestParam
    {
        public string ListName { get; set; }
        public string ItemId { get; set; }
    }
}
