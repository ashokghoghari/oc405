﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    public class PODRequest
    {
        public PODRequest() { }
        public PODRequest(string address, string createdBy, string id, string latCo, string longCo, string listId, string status, string time, string type, string listInternalName)
        {
            this.Address = address;
            this.CreatedBy = createdBy;
            this.Id = id;
            this.Lat = latCo;
            this.Long = longCo;
            this.Status = status;
            this.Time = time;
            this.Type = type;
            this.ListId = listId;
            this.ListInternalName = listInternalName;
        }
        public string Type { get; set; }
        public string ListInternalName { get; set; }
        public string Address { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string ListId { get; set; }
        public string Id { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string Time { get; set; }
    }
    public class PODRequests
    {
        public List<PODRequest> data { get; set; }
    }
}
